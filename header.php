<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
define( 'SITE_ROOT', get_stylesheet_directory_uri() ."/");
define( 'MW_IMAGES', SITE_ROOT . 'framework/assets/images' );
define( 'MW_HOME', home_url() );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <!--[if lt IE 9]>
        <script src="/wp-content/themes/<?php echo get_template(); ?>/framework/assets/js/html5shiv.js"></script>
        <![endif]-->
        <title><?php bloginfo( 'name' ); ?></title>
        <link rel="stylesheet" href="/wp-content/themes/<?php echo get_template(); ?>/framework/assets/css/font-awesome.min.css">
        <?php
        $blog_id = get_current_blog_id();
        if(file_exists( get_template_directory() . '/framework/assets/css/main' . $blog_id .'.min.css')):?>
            <link rel="stylesheet" href="/wp-content/themes/<?php echo get_template(); ?>/framework/assets/css/main<?php echo $blog_id;?>.min.css">
        <?php else:?>
            <link rel="stylesheet" href="/wp-content/themes/<?php echo get_template(); ?>/framework/assets/css/main.min.css">
        <?php endif;?>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="<?php the_field('favicon', 'option');?>" type="image/x-icon">
        <?php $ga = new GoogleAnalyticsController(); $ga->viewOutput(); ?>
        <?php wp_head(); ?>
    </head>
    <body>
<?php

$topBar = new TopBarController();
$topBar->viewOutput();
// Header
$header = new HeaderController();
$header->viewOutput();

if (!is_home() && !is_front_page())
{
    include(MW_VIEW . 'components/breadCrumbTpl.php');
}
?>