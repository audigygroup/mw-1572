<?php get_header();?>
<section id="generic-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-9">
                <div class="page-content">
                    <?php if ( have_posts() ) : ?>

                        <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post(); ?>

                            <?php
                            /*
                             * Run the loop for the search to output the results.
                             * If you want to overload this in a child theme then include a file
                             * called content-search.php and that will be used instead.
                             */
                            get_template_part( 'content', 'search' );

                            // End the loop.
                        endwhile;

                        //Pagination class
                        MWPagination::getInstance();

                        // If no content, include the "No posts found" template.
                        else :
                            get_template_part( 'content', 'none' );
                        endif;
                    ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-3">
                <?php
                // SideBar
                $sideBar = new SideBarController();
                $sideBar->viewOutput();
                ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>