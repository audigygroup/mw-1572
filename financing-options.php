<?php
/**
 * Template Name: Financing Options
 */
get_header();?>
    <div class="main-section-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-lg-9">
                    <?php
                    // Financing Options
                    $financingOptions = new FinancingOptionsController();
                    $financingOptions->viewOutput();

                    //FAQ
                    $faq = new AccordionComponentController();
                    $faq->viewOutput();
                    ?>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-3">
                    <?php
                    // SideBar
                    $sideBar = new SideBarController();
                    $sideBar->viewOutput();
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>