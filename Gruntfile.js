module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                'framework/assets/js/plugins/*.js',
                'framework/assets/js/theme-customizer/theme-customizer.js',
                'framework/assets/js/theme-customizer/component-sortable.js',
                '!framework/assets/js/scripts.min.js',
                'framework/assets/js/mw-custom.js'
            ]
        },
        less: {
            style: {
                options: {
                    cleancss: true,
                    compress: true,
                    yuicompress: true,
                    optimization: 2,
                    ieCompat: true
                },
                files: {
                    'framework/assets/css/main.min.css': 'framework/assets/less/bootstrap.less',
                    'framework/assets/css/admin.min.css': 'framework/assets/less/admin/admin.less'
                }
            }
        },
        uglify: {
            dist: {
                options: {
                    preserveComments: false,
                    //compress: true
                },
                files: {
                    'framework/assets/js/main.min.js': [
                        'framework/assets/js/plugins/bootstrap/transition.js',
                        'framework/assets/js/plugins/bootstrap/carousel.js',
                        'framework/assets/js/plugins/bootstrap/collapse.js',
                        'framework/assets/js/plugins/bootstrap/dropdown.js',
                        'framework/assets/js/plugins/bootstrap/modal.js',
                        'framework/assets/js/plugins/bootstrap/tooltip.js',
                        'framework/assets/js/plugins/bootstrap/popover.js',
                        'framework/assets/js/plugins/bootstrap/tab.js',
                        //'framework/assets/js/plugins/bootstrap/button.js',
                        //'framework/assets/js/plugins/bootstrap/alert.js',
                        //'framework/assets/js/plugins/bootstrap/scrollspy.js',
                        //'framework/assets/js/plugins/bootstrap/affix.js',
                        'framework/assets/js/plugins/*.js',
                        'framework/assets/js/_*.js',
                        'framework/assets/js/mw-custom.js'
                    ],
                    'framework/assets/js/theme-customizer/theme-customizer.min.js': [
                        'framework/assets/js/theme-customizer/component-sortable.js',
                        'framework/assets/js/theme-customizer/theme-customizer.js'
                    ]
                }
            }
        },
        watch: {
            js: {
                files: [
                    '<%= jshint.all %>',
                    'framework/assets/js/theme-customizer/theme-customizer.js',
                    'framework/assets/js/theme-customizer/component-sortable.js',
                    'framework/assets/js/plugins/*.js',
                    'framework/assets/js/plugins/bootstrap/transition.js',
                    'framework/assets/js/plugins/bootstrap/carousel.js',
                    'framework/assets/js/plugins/bootstrap/collapse.js',
                    'framework/assets/js/plugins/bootstrap/dropdown.js',
                    'framework/assets/js/plugins/bootstrap/modal.js',
                    'framework/assets/js/plugins/bootstrap/tooltip.js',
                    'framework/assets/js/plugins/bootstrap/popover.js',
                    'framework/assets/js/plugins/bootstrap/tab.js',
                    'framework/assets/js/mw-custom.js'
                ],
                tasks: ['uglify']
            },
            less: {
                files: [
                    'framework/assets/less/*.less',
                    'framework/assets/less/custom-components/*.less',
                    'framework/assets/less/custom-mixins/*.less',
                    'framework/assets/less/custom-template-overrides/*.less',
                    'framework/assets/less/mixin/*.less',
                    'framework/assets/less/dynamic/*.less',
                    'framework/assets/less/custom-components/component-helpers/*.less'
                ],
                tasks: ['less']
            },
            jshint: {
                files: [
                    'Gruntfile.js',
                    'framework/assets/js/plugins/*.js',
                    'framework/assets/js/theme-customizer/theme-customizer.js',
                    'framework/assets/js/theme-customizer/component-sortable.js',
                    '!framework/assets/js/scripts.min.js'
                ],
                tasks: ['jshint']
            }
        },
        clean: {
            dist: [
                'framework/assets/css/main.min.css',
                'framework/assets/js/scripts.min.js'
            ]
        }
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');

    // Register tasks
    grunt.registerTask('default', [
        'clean',
        'less',
        'uglify',
        'jshint',
    ]);
    grunt.registerTask('dev', [
        'watch',
        'notify'
    ]);

};