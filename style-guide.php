<?php
/*
Template Name: Style Guide
*/
get_header();?>
<section id="style-guide">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h2>Headings</h2>
                </div>
                <div class="highlight">
                    <h1>h1 heading</h1>
                    <h1>h1 <small>Small Text</small></h1>
                    <h2>h2 heading</h2>
                    <h2>h2 <small>Small Text</small></h2>
                    <h3>h3 heading</h3>
                    <h3>h3 <small>Small Text</small></h3>
                    <h4>h4 heading</h4>
                    <h4>h4 <small>small text</small></h4>
                    <h5>h5 heading</h5>
                    <h5>h5 <small>small text</small></h5>
                    <h6>h6 heading</h6>
                    <h6>h6 <small>small text</small></h6>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h2>Paragraphs</h2>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h2>Buttons</h2>
                </div>
                <button class="btn btn-default">Default Button</button>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>