<?php get_header();?>
    <section id="blog-single">
        <div class="main-section-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-lg-9">
                        <?php
                        $single = new EventsAndSeminarsSingleController();
                        $single->viewOutput();
                        ?>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-3">
                        <?php
                        $sideBar = new SideBarController();
                        $sideBar->viewOutput();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>