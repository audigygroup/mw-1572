<?php
/**
 * Created by PhpStorm.
 * User: dwright
 * Date: 2/11/15
 * Time: 3:52 PM
 */


$pages_generated_option = get_option('mw-setup-page-generated');
$menu_generated_option = get_option('mw-setup-menu-generated');

//$pages_generated_option = false;
//$menu_generated_option = false;


if (empty($menu_generated_option)) {
    $term = term_exists( 'Main Menu', 'nav_menu' );

    if ($term !== 0 && $term !== null) {
        wp_delete_term( $term['term_id'], 'nav_menu' );

    }

    $rs = $wpdb->get_results( "SELECT ID FROM wp_posts WHERE post_type IN ('page', 'nav_menu_item')" );

    $pageIds = array();
    if ( ! empty( $rs )) {
        foreach ($rs as $k => $v) {
            $pageIds[] = $v->ID;
        }
    }

    if ( ! empty( $pageIds )) {
        $sql = sprintf( 'DELETE FROM wp_postmeta WHERE post_id IN (%s)', implode( ",", $pageIds ) );
        $rs  = $wpdb->get_results( $sql );

        $sql = sprintf( 'DELETE FROM wp_posts WHERE ID in (%s)', implode( ",", $pageIds ) );
        $rs  = $wpdb->get_results( $sql );
    }
}

$pages = array(
    array(
        'post_name'  => 'about-us',
        'post_title' => 'About Us',
        'post_type'  => 'page',
        'position' => 1,
        'children'   => array(
            array(
                'post_name'  => 'our-practice',
                'post_title' => 'Our Practice',
                'position' => 2,
            ),
            array(
                'post_name'  => 'our-professionals',
                'post_title' => 'Our Professionals',
                'position' => 3,
            ),
            array(
                'post_name'  => 'events-seminars',
                'post_title' => 'Event & Seminars',
                'position' => 4,
            ),
            array(
                'post_name'  => 'review-us',
                'post_title' => 'Review Us',
                'position' => 5,
            ),
            array(
                'post_name'  => 'testimonials',
                'post_title' => 'Testimonials',
                'position' => 6,
            ),
            array(
                'post_name'  => 'patient-forms',
                'post_title' => 'Patient Forms',
                'position' => 7,
            ),
            array(
                'post_name'  => 'insurance',
                'post_title' => 'Insurance',
                'position' => 8,
            )
        )
    ),
    array(
        'post_name'  => 'find-hearing-aids',
        'post_title' => 'Find Hearing Aids',
        'position' => 9,
        'children'   => array(
            array(
                'post_name'  => 'hearing-aid-types',
                'post_title' => 'Find Hearing Types',
                'position' => 10,
                'children'   => array(
                    array(
                        'post_name'  => 'made-for-iphone',
                        'post_title' => 'Made For iPhone',
                        'position' => 11,
                    ),
                    array(
                        'post_name'  => 'behind-the-ear',
                        'post_title' => 'Behind-the-Ear',
                        'position' => 12,
                    ),
                    array(
                        'post_name'  => 'completely-in-the-ear',
                        'post_title' => 'Completely-in-the-Ear',
                        'position' => 13,
                    ),
                    array(
                        'post_name'  => 'invisible-in-the-ear',
                        'post_title' => 'Invisible-in-the-Ear',
                        'position' => 14,
                    ),
                    array(
                        'post_name'  => 'receiver-in-the-ear',
                        'post_title' => 'Receiver-in-the-Ear',
                        'position' => 15,
                    ),
                    array(
                        'post_name'  => 'full-shell-in-the-ear',
                        'post_title' => 'Full Shell-in-the-Ear',
                        'position' => 16,
                    ),
                    array(
                        'post_name'  => 'half-shell-in-the-ear',
                        'post_title' => 'Half Shell-in-the-Ear',
                        'position' => 17,
                    ),
                    array(
                        'post_name'  => 'in-the-canal',
                        'post_title' => 'In-the-Canal',
                        'position' => 18,
                    ),
                    array(
                        'post_name'  => 'tinnitus',
                        'post_title' => 'Tinnitus',
                        'position' => 19,
                    )
                )
            ),
            array(
                'post_name'  => 'lifestyles',
                'post_title' => 'Lifestyles',
                'position' => 20,
                'children'   => array(
                    array(
                        'post_name'  => 'active-lifestyle',
                        'post_title' => 'Active Lifestyle',
                        'position' => 21,
                    ),
                    array(
                        'post_name'  => 'casual-lifestyle',
                        'post_title' => 'Casual Lifestyle',
                        'position' => 22,
                    ),
                    array(
                        'post_name'  => 'quite-lifestyle',
                        'post_title' => 'Quite Lifestyle',
                        'position' => 23,
                    ),
                    array(
                        'post_name'  => 'very-quite-lifestyle',
                        'post_title' => 'Very Quite Lifestyle',
                        'position' => 24,
                    )
                )
            ),
            array(
                'post_name'  => 'accessories',
                'post_title' => 'Accessories',
                'position' => 25,
                'children'   => array(
                    array(
                        'post_name'  => 'cleaning-care',
                        'post_title' => 'Cleaning & Care',
                        'position' => 26,
                    ),
                    array(
                        'post_name'  => 'captioncall',
                        'post_title' => 'CaptionCall',
                        'position' => 27,
                    ),
                    array(
                        'post_name'  => 'agx-medialink',
                        'post_title' => 'AGX MediaLink',
                        'position' => 28,
                    ),
                    array(
                        'post_name'  => 'surflink',
                        'post_title' => 'SurfLink',
                        'position' => 29,
                    ),
                    array(
                        'post_name'  => 'hearing-protection',
                        'post_title' => 'Hearing Protection',
                        'position' => 30,
                    ),
                    array(
                        'post_name'  => 'custom-earbuds-monitors',
                        'post_title' => 'Custom Earbuds & Monitors',
                        'position' => 31,
                    ),
                    array(
                        'post_name'  => 'mobile-apps',
                        'post_title' => 'Mobiles Apps',
                        'position' => 32,
                    ),
                    array(
                        'post_name'  => 'assistive-listening-devices',
                        'post_title' => 'Assistive Listening Devices',
                        'position' => 33,
                    )
                )
            ),
            array(
                'post_name'  => 'batteries',
                'post_title' => 'Batteries',
                'position' => 34
            ),
            array(
                'post_name'  => 'financing-options',
                'post_title' => 'Financing Options',
                'position' => 35
            )
        )
    ),
    array(
        'post_name'  => 'contact',
        'post_title' => 'Contact',
        'position' => 36,
    ),
    array(
        'post_name'  => 'blog',
        'post_title' => 'Blog',
        'position' => 37,
    ),
    array(
        'post_name'  => 'resources',
        'post_title' => 'Resources',
        'position' => 38,
        'children'   => array(
            array(
                'post_name'  => 'facts-about-hearing-loss',
                'post_title' => 'Facts About Hearing Loss',
                'position' => 39,
                'children' => array(
                    array(
                        'post_name'  => 'conductive-hearing-loss',
                        'post_title' => 'Conductive Hearing Loss',
                        'position' => 40,
                    ),
                    array(
                        'post_name'  => 'sensionneural-hearing-loss',
                        'post_title' => 'Sensioneural Hearing Loss',
                        'position' => 41,
                    ),
                    array(
                        'post_name'  => 'mixed-hearing-loss',
                        'post_title' => 'Mixed Hearing Loss',
                        'position' => 42,
                    ),
                )
            ),
            array(
                'post_name'  => 'signs-of-hearing-loss',
                'post_title' => 'Signs of Hearing Loss',
                'position' => 43,
            ),
            array(
                'post_name'  => 'communication-tips',
                'post_title' => 'Communication Tips',
                'position' => 44,
            ),
            array(
                'post_name'  => 'binaural-hearing',
                'post_title' => 'Binaural Hearing',
                'position' => 44,
            ),
            array(
                'post_name'  => 'what-is-an-audiologist',
                'post_title' => 'What is an audiologist?',
                'position' => 45,
            )
        )
    ),
    array(
        'post_name'  => 'services',
        'post_title' => 'Services',
        'position' => 46,
        'children'   => array(
            array(
                'post_name'  => 'tinnitus-treatment',
                'post_title' => 'Tinnitus Treatment',
                'position' => 47,
            ),
            array(
                'post_name'  => 'hearing-tests',
                'post_title' => 'Hearing Tests',
                'position' => 48,
            ),
            array(
                'post_name'  => 'hearing-aid-repairs',
                'post_title' => 'Hearing Aid Repairs',
                'position' => 49,
            ),
            array(
                'post_name'  => 'balance-treatment',
                'post_title' => 'Balance Treatment',
                'position' => 50,
            ),
            array(
                'post_name'  => 'pediatric-hearing-treatment',
                'post_title' => 'Pediatric Hearing Treatment',
                'position' => 51,
            ),
            array(
                'post_name'  => 'hearing-protection',
                'post_title' => 'Hearing Protection',
                'position' => 52,
            )
        )
    ),
    array(
        'post_name'  => 'hippa',
        'post_title' => 'HIPPA',
        'add_to_menu' => false
    ),
    array(
        'post_name'  => 'privacy-policy',
        'post_title' => 'Privacy Policy',
        'add_to_menu' => false
    ),
    array(
        'post_name'  => 'request-appointment',
        'post_title' => 'Request an Appointment',
        'add_to_menu' => false
    ),
    array(
        'post_name'  => 'form-thank-you',
        'post_title' => 'Thank You',
        'add_to_menu' => false
    ),

);

if (!$pages_generated_option) {

    global $wpdb;

    foreach ($pages as $page) {

        $page_id = mw_setup_insert_page( $page );

        if (empty( $page_id )) {
            "error: not post id";
            exit;
        }

        if (isset( $page['children'] ) && ! empty( $page['children'] )) {
            foreach ($page['children'] as $child_page) {
                $child_page['post_parent'] = $page_id;
                $child_page_id =  mw_setup_insert_page( $child_page );

                if (isset($child_page['children']) && !empty($child_page['children'])) {
                    foreach ($child_page['children'] as $child_child_page) {
                        $child_child_page['post_parent'] = $child_page_id;
                        mw_setup_insert_page($child_child_page);
                    }
                }
            }
        }
    }

    add_option( 'mw-setup-page-generated', true );
}


if (empty($menu_generated_option)) {

    $menu_name = 'Primary';

    $menu_exists = wp_get_nav_menu_object($menu_name);

    if (!$menu_exists) {
        $menu_id = wp_create_nav_menu($menu_name);
    } else {
        $menu_id = $menu_exists->term_id;
    }

    $position = 0;
    foreach ($pages as $page) {

        //echo "<pre>"; print_r($page); echo "</pre>";

        if (array_key_exists('add_to_menu', $page)) { continue; }

        if (empty($page['post_title'])) { continue; }

        $currentPage = get_page_by_title($page['post_title']);

        $itemData =  array(
            'menu-item-object-id' => $currentPage->ID,
            'menu-item-parent-id' => 0,
            'menu-item-position'  => $position++,
            'menu-item-object' => 'page',
            'menu-item-type'      => 'post_type',
            'menu-item-status'    => 'publish',
            'menu-item-title' => $page['post_title']
        );

        $parent_menu_id = wp_update_nav_menu_item($menu_id, 0, $itemData);


        if (isset( $page['children'] ) && ! empty( $page['children'] )) {

            foreach ($page['children'] as $child_page) {

                if (empty($child_page['post_title'])) { continue; }

                $currentChildPage = get_page_by_title($child_page['post_title']);

                $itemData =  array(
                    'menu-item-object-id' => $currentChildPage->ID,
                    'menu-item-parent-id' => $parent_menu_id,
                    'menu-item-position'  =>  $position++,
                    'menu-item-object' => 'page',
                    'menu-item-type'      => 'post_type',
                    'menu-item-status'    => 'publish',
                    'menu-item-title' => $child_page['post_title']
                );

                $child_parent_menu_id = wp_update_nav_menu_item($menu_id, 0, $itemData);


                if (isset( $child_page['children'] ) && ! empty( $child_page['children'] )) {

                    foreach ($child_page['children'] as $child_child_page) {

                        if (empty($child_child_page['post_title'])) { continue; }

                        $currentChildChildPage = get_page_by_title($child_child_page['post_title']);

                        $itemData =  array(
                            'menu-item-object-id' => $currentChildChildPage->ID,
                            'menu-item-parent-id' => $child_parent_menu_id,
                            'menu-item-position'  => $position++,
                            'menu-item-object' => 'page',
                            'menu-item-type'      => 'post_type',
                            'menu-item-status'    => 'publish',
                            'menu-item-title' => $child_child_page['post_title']
                        );

                        wp_update_nav_menu_item($menu_id, 0, $itemData);

                    }
                }

            }
        }
    }

    add_option( 'mw-setup-menu-generated', true );
}



function mw_setup_insert_page($page)
{
    $page['post_type'] = 'page';
    $page['post_status'] = 'publish';
    $page['comment_status'] = 'closed';

    $page_id = wp_insert_post($page);
    return $page_id;
}