<?php get_header();?>
<section id="not-found-page">
    <div class="container">
        <div class="not-found-wrap">
            <div class="row">
                <div class="col-xs-12">
                    <p class="not-found-message text-center">The page you’re looking for doesn’t seem to be here. You can try searching for it, or going back to <?php bloginfo( 'name' ); ?> home page below:</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4">
                    <div class="not-found-search">
                        <?php get_search_form() ;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p class="back-home text-center"><a href="<?php echo MW_HOME; ?>" title="Home"><i class="fa fa-chevron-left"></i> Back To Home</a></p>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>