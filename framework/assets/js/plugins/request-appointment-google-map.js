
/*
 * Function to draw map
 */
function drawGoogleMap(){
    var myLatLng;
    //Finds first location in loop to have lat and long to set initial map cordinates
    for(var i =0; i < $ra.raLocation.length; i++){
        if($ra.raLocation[i].latitude === undefined && $ra.raLocation[i].longitude === undefined) {

        }else{
            myLatLng = new google.maps.LatLng($ra.raLocation[i].latitude, $ra.raLocation[i].longitude);
            break;
        }
    }

    //set properties for initial map load
    var mapOptions = {
        center: myLatLng,
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    return new google.maps.Map(document.getElementById('map-canvas-ra'), mapOptions);
}



/*
 * Marker Call back function
 */
function setMakerCallback(location, locLatLng, contentString, map){
    var marker = new google.maps.Marker({
        position: locLatLng,
        animation: google.maps.Animation.DROP,
        infoContent: contentString,
        id: location.ID
    });
    marker.setMap(map);

    return marker;
}

/*
 * set active class for panel
 */
function setActiveState(marker){
    $(document.getElementById('loc-item-' + marker.id)).click(function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
}

/*
 * Listener on set info window on click
 */
function panelListener(map, locLatLng, marker, infoWindow) {
    return function () {
        map.setZoom(14);
        map.setCenter(locLatLng);
        infoWindow.setContent(marker.infoContent);
        infoWindow.open(map, marker);
    };
}

/*
 * Function to draw markers
 */
function drawMarkers(map){

    //sets Content for listener
    function setContent() {
        infoWindow.setContent(this.infoContent);
        infoWindow.open(map, this);
    }

    // Draw Markers
    for (var i = 0; i < $ra.raLocation.length; i++) {
        var location = $ra.raLocation[i];
        var locLatLng = new google.maps.LatLng(location.latitude, location.longitude);

        var controlPanel = document.getElementById('loc-item-' + location.ID);

        var thumbnailUrl = '';

        if(typeof location.thumbnail_url !== 'undefined'){
            thumbnailUrl = '<div class="pull-left">' +
            '<a href="' + location.permalink + '"><img src="' + location.thumbnail_url + '" alt="' + location.location_title + '"></a>' +
            '</div>';
        }
        var contentString = '<div class="map-popup">' +
            '<div class="clearfix">' +
            thumbnailUrl +
            '<div class="pull-left"">' +
            '<div class="content">' +
            '<h6><a href="' + location.permalink + '">' + location.location_title + '</a></h6>' +
            '<ul class="list-unstyled">' +
            '<li><i class="fa fa-location-arrow"></i> ' + location.address + '<br> <span>' + location.city + ', ' + location.state + ' ' + location.zip_code + '</span></li>' +
            '<li><i class="fa fa-phone"></i> ' + location.phone_number + '</li>' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</div>';


        // Checks if lat and long is set to place marker
        if(location.latitude !== '' && location.longitude !== '') {

            var marker = setMakerCallback(location, locLatLng, contentString, map);
            var infoWindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 360
            });

            //Listener for marker click
            google.maps.event.addListener(marker, 'click', setContent);

            //Listener for panel click
            google.maps.event.addDomListener(controlPanel, 'click', panelListener(map, locLatLng, marker, infoWindow));
            //active state
            setActiveState(marker);
        }
    }
}

function loadScriptRa() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA9ga_SmrhLNuNrGqTHn1WKkh24_OrCDOY' +
    '&signed_in=true&callback=initRa';
    document.body.appendChild(script);
}
if(document.getElementById('request-appointment')) {
    window.onload = loadScriptRa;
}


//Init function
function initRa() {
    //map
    var map = drawGoogleMap();

    //marker for map
    drawMarkers(map);
}

