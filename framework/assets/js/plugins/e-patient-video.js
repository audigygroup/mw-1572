+function ($) {
    "use strict";
    //Callback to add video to iframe
    function wistiaCallback(i, video){
        return function(){
            var src = 'http://fast.wistia.net/embed/iframe/' + video.wistia_video_id + '?videoFoam=true&autoPlay=true';
            $('#modal-' + i + ' iframe').attr('src', src);
        };
    }

    //Callback to remove Attr
    function removeSrcAttrCallback(){
        return function() {
            $('iframe').removeAttr('src');
        };
    }

    function setWisitaVideo(){

        for (var i = 0; i < $ePatient.length; i++) {
            var video = $ePatient[i];
            $('#video-container-'+ i).click(wistiaCallback(i, video));
            $('#video-' + i).click(removeSrcAttrCallback());
        }

    }
    if (typeof $ePatient !== 'undefined') {
        setWisitaVideo();
    }
}(jQuery);