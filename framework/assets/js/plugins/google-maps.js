
/*
@TODO Refactor Block
 */
var $map  = $('#map-canvas-0');
var $map1 = $('#map-canvas-1');

// function to bounce when page is loaded
function toggleBounce() {

    if (marker.getAnimation() != null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}
function loadScript() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA9ga_SmrhLNuNrGqTHn1WKkh24_OrCDOY' +
    '&sensor=false&signed_in=true&callback=initialize';
    document.body.appendChild(script);
}
if(document.getElementById('multi-loc') || document.getElementById('location-detail') || document.getElementById('events-and-seminar-single')) {
    window.onload = loadScript;
}

function initialize() {
    //map 1
    var myLatLng  = new google.maps.LatLng($map.data('latitude'), $map.data('longitude'));
    var mapOptions = {
        center:  myLatLng,
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('map-canvas-0'), mapOptions);
    var marker = new google.maps.Marker({
        position: myLatLng,
        animation: google.maps.Animation.DROP
    });
    marker.setMap(map);

    //Map 2
    if(document.getElementById('map-canvas-1')) {

        var myLatLng1 = new google.maps.LatLng($map1.data('latitude'), $map1.data('longitude'));
        var mapOptions1 = {
            center:  myLatLng1,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map1 = new google.maps.Map(document.getElementById('map-canvas-1'), mapOptions1);
        var marker1 = new google.maps.Marker({
            position: myLatLng1,
            animation: google.maps.Animation.DROP
        });
        marker1.setMap(map1);
    }

    google.maps.event.addListener(marker, toggleBounce);
}

