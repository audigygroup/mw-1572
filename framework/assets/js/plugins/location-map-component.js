
/*
 * Function to draw map
 */
function drawGoogleLocMap(){
    var myLatLng;
    //Finds first location in loop to have lat and long to set initial map cordinates
    for(var i =0; i < $loc.length; i++){
        if($loc[i].latitude === undefined && $loc[i].longitude === undefined) {

        }else{
            myLatLng = new google.maps.LatLng($loc[i].latitude, $loc[i].longitude);
            break;
        }
    }

    //set properties for initial map load
    var mapOptions = {
        center: myLatLng,
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    return new google.maps.Map(document.getElementById('map-canvas-loc'), mapOptions);
}

/*
 * Marker Call back function
 */
function setLocMakerCallback(location, locLatLng, mapLoc){
    var marker = new google.maps.Marker({
        position: locLatLng,
        animation: google.maps.Animation.DROP,
        id: location.ID
    });
    marker.setMap(mapLoc);

    return marker;
}

/*
 * Listener on set info window on click
 */
function panelLocListener(mapLoc, locLatLng) {
    return function () {
        mapLoc.setZoom(14);
        mapLoc.setCenter(locLatLng);
    };
}

/*
 * Function to draw markers
 */
function drawLocMarkers(mapLoc){
    // Draw Markers
    for (var i = 0; i < $loc.length; i++) {
        var location = $loc[i];
        var locLatLng = new google.maps.LatLng(location.latitude, location.longitude);

        var controlPanel = document.getElementById('loc-' + location.ID);


        // Checks if lat and long is set to place marker
        if(location.latitude !== '' && location.longitude !== '') {

            setLocMakerCallback(location, locLatLng, mapLoc);

            //Listener for panel click
            google.maps.event.addDomListener(controlPanel, 'click', panelLocListener(mapLoc, locLatLng));
        }
    }
}

function loadScriptLoc() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA9ga_SmrhLNuNrGqTHn1WKkh24_OrCDOY' +
    '&callback=initLoc';
    document.body.appendChild(script);
}
if(document.getElementById('location-map')) {
    window.onload = loadScriptLoc();
}


//Init function
function initLoc() {
    //map
    var mapLoc = drawGoogleLocMap();

    //marker for map
    drawLocMarkers(mapLoc);
}
