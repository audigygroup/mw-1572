/**
 * This file adds some LIVE to the Theme Customizer live preview.
 */
(function( $ ) {

	function blendColors(c0, c1, p) {

	    var f = parseInt( c0.slice(1), 16),
	    t  = parseInt( c1.slice(1), 16),
	    R1 = f >> 16,
	    G1 = f >> 8 & 0x00FF, 
	    B1 = f & 0x0000FF,
	    R2 = t >> 16, 
	    G2 = t >> 8 & 0x00FF,
	    B2 = t & 0x0000FF;

	    return "#" + (0x1000000 + ( Math.round( ( R2 - R1 ) * p ) + R1 ) * 0x10000 + ( Math.round( ( G2 - G1 ) * p ) + G1 ) * 0x100 + ( Math.round( ( B2-B1 ) * p ) + B1 ) ).toString(16).slice(1);
	}

    var fonts;
    $.ajax({
        url: "/wp-content/themes/merriweather/framework/includes/customizer/fonts/fonts.json",
        //force to handle it as text
        dataType: "text",
        success: function(data) {

            fonts = $.parseJSON(data);

            //console.log(fonts);
        }
    });

	/*
	* Custom controls for Live Preview start here
	*/
	wp.customize( 'mw_combo_font', function( value ) {

		value.bind( function( to ) {

            // Defaults
            var heading_font_family = fonts[to].heading_font_family;
            var heading_font_weight = fonts[to].heading_font_weight;

            var sub_headline_font_family = fonts[to].sub_headline_font_family;
            var sub_headline_font_weight = fonts[to].sub_headline_font_weight;
            var sub_headline_font_style = fonts[to].sub_headline_font_style;

            var button_font_family = fonts[to].btn_font_family;
            var button_font_weight = fonts[to].btn_font_weight;
            var button_font_style = fonts[to].btn_font_style;

            var body_font_family = fonts[to].body_font_family;

            var google_font_families = fonts[to].google_font_families;


            $( 'h1, h2, h3, h4, h5 ,h6' ).css({
                fontFamily: heading_font_family,
                fontWeight: heading_font_weight
            });

            $( '.sub-headline').css({
                fontFamily: sub_headline_font_family,
                fontWeight: sub_headline_font_weight,
                fontStyle: sub_headline_font_style
            });

			$( 'body' ).css({
				fontFamily: body_font_family,
                fontWeight: 400
			});

            $( '.btn').css({
                fontFamily: button_font_family,
                fontWeight: button_font_weight,
                fontStyle: button_font_style
            });

            WebFont.load({
                google: {
                    families: google_font_families
                }
            });
		});
	});

	//Vars for blendColors
	var white = '#FFFFFF';
	var black = '#000000';

	wp.customize( 'mw_swatch_a_setting', function( value ) {

	    value.bind( function( newval ) {
			var lighterFiftyColor       = blendColors(newval, white, 0.5);
			var lighterSeventyFiveColor = blendColors(newval, white, 0.75);
			var darkenTenColor          = blendColors(newval, black, 0.10);
			var darkenFifteenColor      = blendColors(newval, black, 0.15);

	        $('.navbar-default .navbar-nav>.active>a, .swatch-a').css( 'background-color', newval );
	        $('.navbar-default, .swatch-a-fifty').css('background-color', lighterFiftyColor );
			$('.navbar .dropdown-menu>li>a').css('background-color', lighterSeventyFiveColor );

			//BTN
			$('.btn, .btn-default, .btn-primary').css({
				'border-bottom': '3px solid' + darkenTenColor,
				'background-color': newval
			});
			$('.btn, .btn-default, .btn-primary').hover(

				function (){
					$(this).css({
						'background-color': darkenFifteenColor,
						'border-top': '3px solid' + darkenTenColor,
						'border-bottom': '3px solid transparent'

					});
				},
				function () {
					$(this).css({
						'background-color': newval,
						'border-top': '3px solid transparent',
						'border-bottom': '3px solid' + darkenTenColor
					});
				}

			);

			//Navigation
			$('.navbar-default .navbar-nav>li>a, .navbar-default .navbar-nav>.open>a').hover(

				function (){
					$(this).css('background-color', lighterSeventyFiveColor );
				},
				function () {
					$(this).css('background-color', 'transparent');
				}

			);

			$('.navbar-nav>.active>a').hover(

				function (){
					$(this).css({
						"background-color": lighterSeventyFiveColor,
						"color": "#444"
					});
				},
				function () {
					$(this).css('background-color', newval);
				}

			);

			$('.navbar .dropdown-menu>li>a ').hover(

				function (){
					$(this).css('background-color', lighterFiftyColor );
				},
				function () {
					$(this).css('background-color', lighterSeventyFiveColor);
				}

			);

	    });
	});

	wp.customize( 'mw_swatch_b_setting', function( value ) {
	    value.bind( function( newval ) {

	    	var lighterFortyColor     = blendColors(newval, white, 0.4);
	        var lighterEightyColor    = blendColors(newval, white, 0.8);
			var darkenTenColor        = blendColors(newval, black, 0.1);
			var darkenFifteenColor    = blendColors(newval, black, 0.15);
			var darkenTwentyfiveColor = blendColors(newval, black, 0.25);

	        $('#hearing-aid-carousel-v2 .carousel-header-wrap .carousel-indicators li.active, .btn-v2, .swatch-b').css('background-color', newval );
			$('#single-loc-map i, .option-wrap i, .swatch-b-border').css({
				"color": newval,
				"border-color": newval
			});
	        $('.swatch-b-forty').css('background-color', lighterFortyColor);
	        $('.swatch-b-eighty').css('background-color', lighterEightyColor);

			$('.audigy-cert-logo').hover(
				function (){
					$(this).css('background-color', newval );
				},
				function () {
					$(this).css('background-color', '');
				}
			);

			//v2 buttons
			$('.btn-v2').css({
				'border-bottom': '3px solid' + darkenTenColor
			});

			$('.btn-v2').hover(

				function (){
					$(this).css({
						'background-color': darkenFifteenColor,
						'border-top': '3px solid' + darkenTwentyfiveColor,
						'border-bottom': '3px solid transparent'

					});
				},
				function () {
					$(this).css({
						'background-color': newval,
						'border-top': '3px solid transparent',
						'border-bottom': '3px solid' + darkenTwentyfiveColor
					});
				}

			);

	    });
	});

	wp.customize( 'mw_swatch_c_setting', function( value ) {
	    value.bind( function( newval ) {

			$('.swatch-c').css('background-color', newval);

	    });
	});

	wp.customize( 'mw_swatch_d_setting', function( value ) {
    	value.bind( function( newval ) {
			$(
				'.navbar-default .navbar-nav>li>a,' +
				'.dropdown-menu>li>a,' +
				'.header-wrap .member-info h3,' +
				'.header-wrap .member-info p,' +
				'.welcome-text h1,' +
				'.welcome-text .sub-headline,' +
				'#welcome-provider .name,' +
				'#welcome-provider .caption,' +
				'#our-practice h1,' +
				'#single-loc-map .title h2,' +
				'#single-loc-map .title p,' +
				'#hearing-aid-carousel-v2 h1,' +
				'#hearing-aid-carousel-v2 .sub-headline,' +
				'#hearing-aid-carousel-v2 .hearing-aid-text,' +
				'#testimonials-v2 h1,' +
				'#testimonials-v2 .sub-headline,' +
				'#start-your-journey-v2 h2,' +
				'#search-form-v2 button'
			).css('color', newval );

			$('.search-form-v2 .search-button').css({
				'background': newval,
				'border': '1px solid' + newval
			});
	    });
	});

})( jQuery );