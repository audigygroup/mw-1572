if(jQuery.ui){
    +function($) {
        function componentSortable() {
            var compSort = $('body');

            compSort.sortable({
                items: '> section',
                axis: 'y',
                cursor: 'move',
                opacity: '.6',
                distance: 1,
                delay: 150,
                containment: 'body',
                tolerance: 'pointer',
                start: function(e, ui){
                    //console.log('start');
                    if ( typeof(inlineEditPost) !== 'undefined' ) {
                        inlineEditPost.revert();
                    }
                    ui.placeholder.height(ui.item.height());
                }
            });
        }
        componentSortable();
    }(jQuery);
}
