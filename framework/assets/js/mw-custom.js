/*
*  Needs to be refactored
*  @TODO Refactor into plugin and call back
*/
+function ($) {
    "use strict";

    //Google plus
    $('#google-vid').click(function () {
        var $googleId = $('#google-plus iframe').data('google');
        var src = 'http://fast.wistia.net/embed/iframe/'+ $googleId + '?videoFoam=true&autoPlay=true';
        $('#google-plus-modal iframe').attr('src', src);
    });
    //Facebook
    $('#facebook-vid').click(function () {
        var $facebookId = $('#facebook iframe').data('facebook');
        var src = 'http://fast.wistia.net/embed/iframe/'+ $facebookId + '?videoFoam=true&autoPlay=true';
        $('#facebook-modal iframe').attr('src', src);
    });
    //Yelp
    $('#yelp-vid').click(function () {
        var $yelpId = $('#yelp iframe').data('yelp');
        var src = 'http://fast.wistia.net/embed/iframe/'+ $yelpId + '?videoFoam=true&autoPlay=true';
        $('#yelp-modal iframe').attr('src', src);
    });

    //Lifestyles
    $('#lifestyles-video-img').click(function () {
        var $lifestylesId = $('#lifestyles-video iframe').data('lifestyles-landing');
        var src = 'http://fast.wistia.net/embed/iframe/'+ $lifestylesId + '?videoFoam=true&autoPlay=true';
        $('#lifestyles-modal iframe').attr('src', src);
    });

    //Lifestyle Detail
    $('#lifestyle-img').click(function () {
        var $lifestylesDetailId = $('#lifestyle-detail-video iframe').data('lifestyles-detail');
        var src = 'http://fast.wistia.net/embed/iframe/'+ $lifestylesDetailId + '?videoFoam=true&autoPlay=true';
        $('#lifestyles-detail-modal iframe').attr('src', src);
    });

    //Remove iframe src
    $('#google-plus, #facebook, #yelp, #lifestyles-video, #lifestyle-detail-video').click(function () {
        $('iframe').removeAttr('src');
    });

}(jQuery);