<?php

class ProfessionalPostData extends GetPosts
{

    public function __construct($postIds = null)
    {
        global $post;

        $this->args = array(
            'post_type' => 'professional',
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
            'include'   => $postIds
        );

        //Set only for events and seminars single page
        if(is_singular('events-and-seminars')){
            $this->args['include'] = $postIds == null ? array('') : $postIds;
        }elseif ($post->post_type == 'professional') {
            $this->args['name'] = $post->post_name;
        }
    }
}