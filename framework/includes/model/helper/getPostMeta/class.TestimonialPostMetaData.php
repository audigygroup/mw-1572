<?php

class TestimonialPostMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $testimonialId = new TestimonialPostData();
        $this->postObject = $testimonialId->getPosts();
    }
}