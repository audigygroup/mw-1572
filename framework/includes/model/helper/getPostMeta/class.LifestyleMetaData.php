<?php

class LifestyleMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $postId = new LifestylePostData();
        $this->postObject = $postId->getPosts();
    }
}