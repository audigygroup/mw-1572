<?php

class EventsAndSeminarsMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $post = new EventsAndSeminarsPostData();
        $this->postObject = $post->getPosts();
    }
}