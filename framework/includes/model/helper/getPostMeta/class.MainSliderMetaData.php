<?php

class MainSliderMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $mainSliderId = new MainSliderPostData();
        $this->postObject = $mainSliderId->getPosts();
    }
}