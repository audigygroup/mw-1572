<?php

class LocationMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $locationId = new LocationPostData();
        $this->postObject = $locationId->getPosts();
    }
}