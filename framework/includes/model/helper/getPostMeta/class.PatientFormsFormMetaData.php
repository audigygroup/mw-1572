<?php

class PatientFormsFormMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $patientFormsId = new PatientFormsFormPostData();
        $this->postObject = $patientFormsId->getPosts();
    }
}