<?php

class ProfessionalMetaData extends GetPostsMeta
{

    protected $postIds;

    public function __construct()
    {

	    // Added to address issue with getting presenters, affecting general professionals.
	    // Very bad, relying on slug to filter,
	    // TODO: make this better
	    if(is_singular('events-and-seminars')) {
		    $this->setPostIdIncludeArg();
	    }

        $id = new ProfessionalPostData($this->postIds);
        $this->postObject = $id->getPosts();
    }

    /*
     * Method to set property to post ids of presenters
     */
    protected function setPostIdIncludeArg()
    {
        $postIds = EventsAndSeminarsEventModelFilter::getInstance();
        $postInclude = $postIds->getFilteredModel();

	    if (isset($postInclude[0])) {
            $post = unserialize($postInclude[0]['presenters']);
		    $this->postIds = $post;
	    }
    }
}