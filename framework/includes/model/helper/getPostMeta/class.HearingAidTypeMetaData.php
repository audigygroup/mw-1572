<?php

class HearingAidTypeMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $hearingAidTypeId = new HearingAidTypePostData();
        $this->postObject = $hearingAidTypeId->getPosts();
    }
}