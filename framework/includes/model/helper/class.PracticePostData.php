<?php

class PracticePostData extends WpOptionDataBase
{
    private static $instance;

    private function __construct(){
        $this->wpOptionData = array(
            'options_header_logo',
            'options_footer_logo',
            'options_facebook_id',
            'options_twitter_id',
            'options_youtube_id',
            'options_google_plus_id',
            'options_linkedin_id',
            'options_google_analytics',
            'options_powered_by',
            'options_powered_by_link',
            'options_audigy_certified_desc',
            'options_welcome_subheadline',
            'options_welcome_statement',
            'options_call_to_action_title',
            'options_call_to_action_subheadline',
            'options_thank_you_message',
            'options_practice_name',
            'options_practice_name_possessive',
            'options_a_an'
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}