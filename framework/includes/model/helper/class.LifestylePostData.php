<?php

class LifestylePostData extends GetPosts
{

    public function __construct()
    {

        global $post;

        $this->args = array(
            'post_type' => 'lifestyle',
            'orderby'   => 'menu_order',
            'order'     => 'DESC'
        );

        # filter to a single professional
        if ($post->post_type == 'lifestyle') {
            $this->args['name'] = $post->post_name;
        }

    }
}