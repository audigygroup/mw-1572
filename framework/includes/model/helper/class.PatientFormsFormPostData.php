<?php

class PatientFormsFormPostData extends GetPosts
{
    public function __construct()
    {
        $this->args = array(
            'post_type'       => 'patient-forms',
            'orderby'         => 'menu_order',
            'order'           => 'ASC'
        );
    }
}