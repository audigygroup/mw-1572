<?php

class LocationPostData extends GetPosts
{
    public function __construct($single = array())
    {
        global $post;

        $this->args = array(
            'post_type' => 'location',
            'orderby'   => 'menu_order',
            'order'     => 'ASC'
        );

        //Set array with key of single to be used as dependency
        if(array_key_exists('single', $single)){
            # filter to a single professional
            if ($post->post_type == 'location') {
                $this->args['name'] = $post->post_name;
            }
        }
    }
}