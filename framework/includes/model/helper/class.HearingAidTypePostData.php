<?php

class HearingAidTypePostData extends GetPosts
{
    public function __construct()
    {
        global $post;

        $this->args = array(
            'post_type' => 'hearing-aid-type',
            'orderby'   => 'menu_order',
            'order'     => 'ASC'
        );

        # filter to a single Hearing Aid Type
        if ($post->post_type == 'hearing-aid-type') {
            $this->args['name'] = $post->post_name;
        }
    }
}