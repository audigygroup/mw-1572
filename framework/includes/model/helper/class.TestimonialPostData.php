<?php

class TestimonialPostData extends GetPosts
{
    public function __construct()
    {
        $this->args = array(
            'post_type' => 'testimonials',
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
        );
    }
}
