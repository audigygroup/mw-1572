<?php

class EventsAndSeminarsPostData extends GetPosts
{
    public function __construct()
    {
        global $post;

        $this->args = array(
            'post_type'       => 'events-and-seminars',
            'orderby'         => 'post_date',
            'order'           => 'DESC',
        );

        if ($post->post_type == 'events-and-seminars') {
            $this->args['name'] = $post->post_name;
        }
    }
}