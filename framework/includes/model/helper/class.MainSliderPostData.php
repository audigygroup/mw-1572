<?php

class MainSliderPostData extends GetPosts
{
    public function __construct()
    {
        $this->args = array(
            'post_type' => 'slider',
            'orderby'   => 'menu_order',
            'order'     => 'ASC'
        );

    }

}