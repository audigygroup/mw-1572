<?php

/*
 * Base Class for Filter Classes. Meant for one Post type at this time 2/6/15
 */

class FilterBase
{
    // set to new up Factory
    protected $dataSet;

    //Required fields Variables
    protected $postDataSet;
    protected $metaDataSet;

    protected function getFullData()
    {
        return $this->dataSet;
    }

    /*
    * Merges permalink to post data
    */
    protected function mergePostAndPermalink()
    {
        $fullDataObj = $this->getFullData();
        $postData = $fullDataObj->post;
        $mergePostAndPermalink = array();

        foreach ($postData as $key => $post) {
            $postId = $post->ID;
            $permalink['permalink'] = get_permalink( $post->ID );
            $mergePostAndPermalink[$key] = array_merge( (array) $post, (array) $permalink );
        }

        return $mergePostAndPermalink;
    }

    /*
    * gets Thumbnail Post URL by ID
    */
    protected function getThumbnailUrl()
    {
        $postData = $this->mergePostAndPermalink();

        $url = array();

        foreach($postData as $key => $imgUrl){
            $imgId = $imgUrl['ID'];
            $getImg['thumbnail_url'] = wp_get_attachment_url( get_post_thumbnail_id( $imgId ) );
            $url[$key] =  array_merge( (array) $imgUrl, (array) $getImg );
        }
        return $url;
    }

    /*
     * Merges Meta tp post with Post ID
     */
    protected function mergeMetaToPost()
    {
        $fullDataObj = $this->getFullData();
        $mergedPostAndPermalink = $this->getThumbnailUrl();
        $postType = $mergedPostAndPermalink;
        $postMeta = $fullDataObj->metaPost;
        $mergeList = array();

        $postMetaForId = array();
        foreach ($postType as $post) {
            $postId = $post['ID'];
            if(isset($postMeta[$postId])){
                $postMetaForId = $postMeta[$postId];
            }
            $mergeList[$postId] = array_merge( (array) $post, (array) $postMetaForId );
        }

        return $mergeList;
    }

    /*
     * Filters Posts
     */
    protected function filterPostDataFields()
    {
        $mergedData = $this->mergeMetaToPost();

        $tempFilterData = array();
        $count          = 0;

        foreach ($mergedData as $postArray) {
            foreach ($this->postDataSet as $dataField) {
                $fieldsRequired = $postArray[$dataField];
                if ($fieldsRequired) {
                    $tempFilterData[$count][$dataField] = $fieldsRequired;
                }
            }
            foreach ($this->metaDataSet as $dataField) {

                if (isset( $postArray[$dataField] )) {

                    $fieldsRequired = $postArray[$dataField];
                    if ($fieldsRequired) {
                        $tempFilterData[$count][$dataField] = $fieldsRequired[0];
                    }
                } else {
                    $tempFilterData[$count][$dataField] = "";
                }
            }
            $count ++;
        }

        return $tempFilterData;
    }

    /*
     * Getter
     */
    public function getFilteredModel()
    {
        return $this->filterPostDataFields();
    }
}