<?php
/*
 *Base Class for Model to be used by Factory to put post and post meta together
 */
class BaseModel {
    public $post;
    public $metaPost;

    public function __construct($postData, $metaData = null)
    {
        $this->post = $postData;
        $this->metaPost = $metaData;
    }
}