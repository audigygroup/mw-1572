<?php
/*
 * Base Class for Category
 */
class GetCatList
{
    protected $tax;
    protected $args;

    public function getCatList()
    {
        $tax  = $this->tax;
        $args = $this->args;
        $cat = get_terms($tax, $args);

        return $cat;
    }

}