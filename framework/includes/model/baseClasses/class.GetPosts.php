<?php
/*
 * Base Class Used to use get_post WordPress Function to Query for post Object
 */

class GetPosts extends MWPost
{

    protected $args;

    public function getPosts()
    {
        $args = $this->args;
        $args['posts_per_page'] = (!array_key_exists('posts_per_page', $args) ) ? -1 : $args['posts_per_page'];
        $postObjs = get_posts($args);

        return (array) $postObjs;
    }


}