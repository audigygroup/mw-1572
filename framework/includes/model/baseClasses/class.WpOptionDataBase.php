<?php
/*
 * Base Class Meant to get any field from wp_option table
 */
class WpOptionDataBase
{

    //set Array with all options to get
    protected $wpOptionData;

    /*
     * Loops through options set to use get_option function
     */
    protected function getWpOptionData()
    {
        $options = array();

        foreach($this->wpOptionData as $key => $option){
            $options[$option] = get_option($option);
        }
        return $options;
    }

    public function getModel()
    {
        $model = $this->getWpOptionData();
        return $model;
    }
}