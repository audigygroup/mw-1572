<?php
/*
 *  Base Class Used to query for Post Meta
 */
class GetPostsMeta extends MWMetaPost
{

    protected $postObject;

    public function getPostMeta()
    {
        $postMetaById = array();

        foreach($this->postObject as $post){
            $p = (array) $post;
            $postMetaId = $p['ID'];
            $postMetaObjs = get_metadata( 'post', $postMetaId, '', true );

            $postMetaById[$postMetaId] = $postMetaObjs;
        }
        return $postMetaById;
    }


}