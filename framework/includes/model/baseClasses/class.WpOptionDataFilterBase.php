<?php
/*
 * Filters wp_option Field
 */
class WpOptionDataFilterBase
{
    // Set model to be used
    protected $model;

    //Required Fields
    protected $requiredData;

    protected function getWpOptionObj()
    {
        $fullWpOptionObj = $this->model->getModel();
        return $fullWpOptionObj;
    }

    protected function filterWpOptionData()
    {
        $wpOptionDataSet =  $this->getWpOptionObj();

        $tempFilteredArray = array();

        foreach($this->requiredData as $dataField){
            if (array_key_exists($dataField, $wpOptionDataSet)) {
                $fieldsRequired = $wpOptionDataSet[$dataField];
                $tempFilteredArray[$dataField] = $fieldsRequired;
            }
        }

        return $tempFilteredArray;
    }

    public function getFilteredData()
    {
        return $this->filterWpOptionData();
    }
}