<?php

/*
 * @TODO Refactor This class
 * This Class is used to get meta Data that is Added by ACF. This is a BASE ClASS
 */
class GetCatListMeta
{
    //Taxonomy Data Object
    protected $taxObj;

    //Taxonomy Name
    protected $taxonomy;

    // Get Field for Taxonomy taxonomy image
    protected function catMetaData()
    {
        $taxMetaById = array();

        foreach($this->taxObj as $key => $obj){
            //Use Term ID
            $termId = $obj->term_id;

            // get fields
            $taxonomy_image           = get_field( 'taxonomy_image', $this->taxonomy.'_'.$termId );
            $taxonomy_subheadline     = get_field( 'sub_headline', $this->taxonomy.'_'.$termId );
            $taxonomy_subheadline_par = get_field( 'sub_headline_paragraph', $this->taxonomy.'_'.$termId );
            $taxonomy_quick_nav       = get_field('quick_nav_term_id', $this->taxonomy.'_'.$termId);
            $ePatientVideos           = get_field('e_patient_videos', $this->taxonomy.'_'.$termId);
            $compId                   = get_field('component_page_id', $this->taxonomy.'_'.$termId);

            //Hard Coded. Needs refactoring
            $metaDataArray = array(
                'taxonomy_image' => array(
                    'img' => $taxonomy_image['url'],
                    'alt' => $taxonomy_image['alt']
                ),
                'taxonomy_headline' => array(
                    'sub_headline' => $taxonomy_subheadline,
                    'sub_headline_paragraph' => $taxonomy_subheadline_par,
                ),
                'taxonomy_quick_nav' => $taxonomy_quick_nav,
                'e_patient_videos'   => $ePatientVideos,
                'component_page_id'  => $compId
            );

            $taxMetaById[$termId] = $metaDataArray;
        }
        return $taxMetaById;
    }

    public function getCatMeta(){
        return $this->catMetaData();
    }
}