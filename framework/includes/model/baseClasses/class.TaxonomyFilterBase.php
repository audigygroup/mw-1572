<?php
/*
 * This Class is used As the Base Filter class for Taxonomy Data and Custom Taxonomy Data
 */
class TaxonomyFilterBase
{
    //Set to new up Factory
    protected $dataSet;
    //Required fields Variables
    protected $filteredFields;

    /*
    *  Grab Full Data
    */
    protected function getFullData()
    {
        return $this->dataSet;
    }

    /*
     * Merges Tax Obj to Tax AFC image Obj together
     */
    protected function mergeTaxToMeta()
    {
        $fullDataObj = $this->getFullData();
        $tax = $fullDataObj->tax;
        $taxMeta = $fullDataObj->taxImg;
        $mergeList = array();

        foreach ($tax as $key => $taxonomy) {
            $taxId = $taxonomy->term_id;
            $taxMetaForId['taxonomy_metadata'] = $taxMeta[$taxId];
            $mergeList[$key] = array_merge( (array) $taxonomy, $taxMetaForId );
        }
        return $mergeList;
    }

    /*
     * Filters Fields that are set with $filteredFields property
     */
    protected function filterTaxFields()
    {
        $mergedData = $this->mergeTaxToMeta();
        $tempFilterData = array();

        $count = 0;

        foreach ($mergedData as $index => $taxArray) {
            foreach ($this->filteredFields as $dataField) {
                $fieldsRequired = $taxArray[$dataField];
                if ($fieldsRequired) {
                    $tempFilterData[$count][$dataField] = $fieldsRequired;
                }
            }
            $count ++;
        }
        return $tempFilterData;
    }

    /*
     * Getter
     */
    public function getFilteredModel()
    {
        //checks if taxonomy has any post associated with it
        if(empty($this->getFullData()->tax)){
            return;
        }else{
            return $this->filterTaxFields();
        }
    }
}