<?php

class ColorPickerSwatchAControl extends MWCustomizerControlBase
{
    public $objCtrl  = 'WP_Customize_Color_Control';
	public $id       = 'mw_swatch_a_ctrl';
    public $label    = 'Swatch A';
    public $settings = 'mw_swatch_a_setting';
    public $section  = 'mw_color_picker';

}