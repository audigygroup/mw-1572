<?php

class ColorPickerSwatchBControl extends MWCustomizerControlBase
{
    public $objCtrl  =  'WP_Customize_Color_Control';
	public $id       = 'mw_swatch_b_ctrl';
    public $label    = 'Swatch B';
    public $settings = 'mw_swatch_b_setting';
    public $section  = 'mw_color_picker';
}