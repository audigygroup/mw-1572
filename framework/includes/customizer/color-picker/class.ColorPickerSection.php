<?php 
 
class ColorPickerSection 
{
	public $id = 'mw_color_picker';
    public $title = 'Color Picker';
    public $priority = '400';
    public $description = 'There are colors for your theme. It is based on 4 base colors.';
    public $args = array();

    public $settingClasses = array(
        'ColorPickerSwatchA',
        'ColorPickerSwatchB',
        'ColorPickerSwatchC',
        'ColorPickerSwatchD'
    );

    public $settingObjects = array();
    public $controlObjects = array();


    public function __construct()
    {

        $this->sectionArgs();

        foreach ($this->settingClasses as $className) {

            $settingClassName = $className . 'setting';
           
            $settingObj = new $settingClassName( $this->id );

            $this->settingObjects[] = $settingObj;

            $controlerClassName = $className . 'Control';

            $this->controlObjects[] = new $controlerClassName( $this->id, $settingObj->id );
        }

    }

    public function sectionArgs()
    {
        $this->args['title']       = $this->title;
        $this->args['priority']    = $this->priority;
        $this->args['description'] = $this->description;

    }
	
}