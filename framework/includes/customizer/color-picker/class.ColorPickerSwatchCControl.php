<?php 

class ColorPickerSwatchCControl extends MWCustomizerControlBase
{
    public $objCtrl  =  'WP_Customize_Color_Control';
	public $id       = 'mw_swatch_c_ctrl';
    public $label    = 'Swatch C';
    public $settings = 'mw_swatch_c_setting';
    public $section  = 'mw_color_picker';
}