<?php 

class ColorPickerSwatchDControl extends MWCustomizerControlBase
{
    public $objCtrl  =  'WP_Customize_Color_Control';
	public $id       = 'mw_swatch_d_ctrl';
    public $label    = 'Swatch D';
    public $settings = 'mw_swatch_d_setting';
    public $section  = 'mw_color_picker';
}