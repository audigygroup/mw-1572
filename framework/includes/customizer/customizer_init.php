<?php
if (class_exists( 'WP_Customize_Control' )) {

    require_once( MW_INC . 'customizer/class.SectionClasses.php' );
    require_once( MW_INC . 'customizer/class.MWCustomizer.php' );

    require_once( MW_INC . 'customizer/base-classes/class.MWCustomizerSettingsBase.php' );
    require_once( MW_INC . 'customizer/base-classes/class.MWCustomizerControlBase.php' );

    // Fonts
    require_once(MW_INC . 'customizer/fonts/class.FontPickerComboSetting.php');
    require_once(MW_INC . 'customizer/fonts/class.FontPickerComboControl.php');
    require_once( MW_INC . 'customizer/fonts/class.FontPickerSection.php' );

    // Color Picker
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSection.php' );
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSwatchASetting.php' );
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSwatchAControl.php' );
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSwatchBSetting.php' );
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSwatchBControl.php' );
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSwatchCSetting.php' );
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSwatchCControl.php' );
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSwatchDSetting.php' );
    require_once( MW_INC . 'customizer/color-picker/class.ColorPickerSwatchDControl.php' );

}