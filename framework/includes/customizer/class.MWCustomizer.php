<?php

/*
* Merriweather Customizer CTRL
*/

if ( ! class_exists( 'WP_Customize_Section' ) || ! class_exists( 'WP_Customize_Setting' ) || ! class_exists( 'WP_Customize_Control' )) {
    return;
}

add_action( 'customize_register', array( 'MWCustomizer', 'GetInstance' ) );

class MWCustomizer
{
    /**/
    protected $sectionsToRender = array();

    protected function __construct( $sectionsToRender )
    {
        $this->sectionsToRender = $sectionsToRender;

        if (empty( $this->sectionsToRender )) {
            return;
        }

        $this->addSections();
        add_action( 'customize_save_after', array( $this, 'usePhpToCompileLessAndCreateMinCss' ) );
    }

    public static function GetInstance()
    {
        static $instance = null;

        if (null === $instance) {
            $sectionToRenderObj = new SectionClasses();
            $sectionsToRender   = $sectionToRenderObj->active;

            $instance = new static( $sectionsToRender );
        }

        return $instance;
    }

    public function addSections()
    {
        global $wp_customize;

        foreach ($this->sectionsToRender as $sectionClasses) {

            $obj = new $sectionClasses;

            $wp_customize->add_section(
                $obj->id,
                $obj->args
            );

            $this->addSetting( $obj->settingObjects );
            $this->addControl( $obj->controlObjects );

        }
    }

    public function addSetting( $settingObj )
    {
        global $wp_customize;
        foreach ((array) $settingObj as $obj) {

            $wp_customize->add_setting(
                $obj->id,
                $obj->args
            );
        }
    }

    public function addControl( $controlObj )
    {
        global $wp_customize;

        foreach ((array) $controlObj as $obj) {
            $wp_customize->add_control(
                new $obj->objCtrl(
                    $wp_customize,
                    $obj->id,
                    $obj->args
                )
            );
        }
    }

    /**
     * usePhpToCompileLessAndCreateMinCss
     * Read custom settings
     * Update less variables with settings
     * compile less min cssgit s
     * @throws Exception
     *
     */
    public function usePhpToCompileLessAndCreateMinCss()
    {
        $templateDirectory = get_template_directory();
        $oneLine           = "\r\n";
        $twoLines          = $oneLine . $oneLine;
        $lessLineTpl       = '@%s: %s;';
        $lessLineTpl2      = '@mw-php-%s: %s;';

        $fileDir = "$templateDirectory/framework/assets/less/dynamic/mw-php-variables.less";

        $swatchA    = get_theme_mod( 'mw_swatch_a_setting' );
        $swatchB    = get_theme_mod( 'mw_swatch_b_setting' );
        $swatchC    = get_theme_mod( 'mw_swatch_c_setting' );
        $swatchD    = get_theme_mod( 'mw_swatch_d_setting' );
        $fontOption = get_theme_mod( 'mw_combo_font' );


        //$comboHeading = $mwFonts[$mwFontCombos[$fontOptions]['comboHeading']]['fontFamily'];
        //$comboBody    = $mwFonts[$mwFontCombos[$fontOptions]['comboBody']]['fontFamily'];


        $lessLines = array(
            "/*$oneLine* If you are in the mw-php-variables.less file, DO NOT EDIT!$oneLine*/",
            sprintf( $lessLineTpl, 'mw-base-1', $swatchA ),
            sprintf( $lessLineTpl, 'mw-base-2', $swatchB ),
            sprintf( $lessLineTpl, 'mw-base-3', $swatchC ),
            sprintf( $lessLineTpl, 'mw-base-4', $swatchD )
        );

        // Fonts

        // Get values for current font option
        $MWFontOptions = MWFontOptions::getInstance();
        $fontOptions   = $MWFontOptions->getLessValues( $fontOption );

        $lessVariables = array(
            'heading_font_family',
            'heading_font_weight',
            'sub_headline_font_family',
            'sub_headline_font_weight',
            'sub_headline_font_style',
            'body_font_family',
            'btn_font_family',
            'btn_font_weight',
            'btn_font_style'
        );


        if ( ! empty( $fontOptions ) && ! empty( $lessVariables )) {
            foreach ($lessVariables as $lessVar) {
                $lessLines[] = sprintf( $lessLineTpl2, str_replace( "_", "-", $lessVar ), $fontOptions[$lessVar] );
            }
        }
        $baseVars = implode( $twoLines, $lessLines );

        //writes to Less File
        file_put_contents( $fileDir, $baseVars );


        $parser = new Less_Parser( array( 'compress' => true, 'relativeUrls' => false ) );
        $parser->parseFile( get_template_directory() . "/framework/assets/less/bootstrap.less" );
        $css = $parser->getCss();


        $blog_id = get_current_blog_id();
        file_put_contents( get_template_directory() . "/framework/assets/css/main" . $blog_id  . ".min.css", $css );
    }
}

