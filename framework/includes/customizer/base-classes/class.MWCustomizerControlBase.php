<?php 

if(class_exists('WP_Customize_Control')){
	class MWCustomizerControlBase extends WP_Customize_Control
	{
		public $objCtrl;
	    public $id;
	    public $label;
	    public $section;
	    public $settings;
	    public $type;
	    public $choices;

	    public $args = array();


	    public function __construct( $sectionID, $settingID )
	    {
	        $this->ctrlArgs();
	        $this->section  = $sectionID;
	        $this->settings = $settingID;
	    }

	    public function ctrlArgs()
	    {
	        $this->args['label']   = $this->label;
	        $this->args['type']    = $this->type;
	        $this->args['section'] = $this->section;
	        $this->args['settings'] = $this->settings;
	        $this->args['choices'] = $this->choices;

	    }
	}
}