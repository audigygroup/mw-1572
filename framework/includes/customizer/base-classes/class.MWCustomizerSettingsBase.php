<?php 
class MWCustomizerSettingsBase
{
    public $id;
    public $default = '';
    public $type = 'theme_mod';
    public $capability = 'edit_theme_options';
    public $theme_supports = '';
    public $transport = 'postMessage';
    public $sanitize_callback = '';
    public $sanitize_js_callback = '';

    public $args = array();

    public function __construct()
    {
        $this->settingArgs();
    }

    public function settingArgs()
    {
        $this->args['default']              = $this->default;
        $this->args['type']                 = $this->type;
        $this->args['capability']           = $this->capability;
        $this->args['theme_supports']       = $this->theme_supports;
        $this->args['transport']            = $this->transport;
        $this->args['sanitize_callback']    = $this->sanitize_callback;
        $this->args['sanitize_js_callback'] = $this->sanitize_js_callback;
    }
}