<?php

class MWFontCombos
{

    public $mwFontCombos = array(
        'lato combo' => array(
            'comboHeading' => 'lato',
            'comboBody'    => 'arial'
        ),
        'arial' => array(
            'comboHeading' => 'arial',
            'comboBody'    => 'lato'
        )
    );

}