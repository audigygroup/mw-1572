<?php

class MWFontOptions
{

    protected $fontOptionsFromJSON;

    protected function __construct()
    {
        $this->fontOptionsFromJSON = $this->getFontOptionsFromJSON();
    }

    public static function getInstance()
    {
        return new MWFontOptions();
    }

    public function getLessValues( $optionNumber )
    {
        if (empty( $optionNumber )) {
            throw new Exception( 'missing font option' );
        }
        return $this->fontOptionsFromJSON[$optionNumber];
    }

    public function getFontOptionsFromJSON()
    {
        $fontFilePath = get_template_directory() . "/framework/includes/customizer/fonts/fonts.json";
        $fonts = file_get_contents($fontFilePath);
        return json_decode($fonts, ARRAY_A);
    }
}