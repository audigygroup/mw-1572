<?php

class FontPickerSection
{

    public $id = 'mw_display_options';
    public $title = 'Fonts';
    public $priority = '300';
    public $description = 'These are Fonts for Merriweather';
    public $args = array();

    public $settingClasses = array(
        'FontPickerCombo'
    );

    public $settingObjects = array();
    public $controlObjects = array();


    public function __construct()
    {
        $this->sectionArgs();

        foreach ($this->settingClasses as $className) {

            $settingClassName = $className . "Setting";
            $settingObj       = new $settingClassName( $this->id );

            $this->settingObjects[] = $settingObj;

            $controlClassName       = $className . 'Control';
            $this->controlObjects[] = new $controlClassName( $this->id, $settingObj->id );
        }
    }

    public function sectionArgs()
    {
        $this->args['title']       = $this->title;
        $this->args['priority']    = $this->priority;
        $this->args['description'] = $this->description;
    }
}