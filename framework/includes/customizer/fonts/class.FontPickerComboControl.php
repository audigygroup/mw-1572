<?php

class FontPickerComboControl extends MWCustomizerControlBase
{
    public $objCtrl  = 'WP_Customize_Control';
    public $id       = 'mw_font_combo';
    public $label    = 'Font Combos';
    public $type     = 'radio';
    public $settings = 'mw_combo_font';
    public $section  = 'mw_display_options';

    public $choices = array(
        'option1'  => 'Option 1',
        'option2'  => 'Option 2',
        'option3'  => 'Option 3',
        'option4'  => 'Option 4',
        'option5'  => 'Option 5',
        'option6'  => 'Option 6',
        'option7'  => 'Option 7',
        'option8'  => 'Option 8',
        'option9'  => 'Option 9',
        'option10' => 'Option 10'
    );
}