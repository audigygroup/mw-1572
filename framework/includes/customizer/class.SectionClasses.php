<?php
/*
* This class Controls what classes are being used in customizer
*/
class SectionClasses
{
    public $active = array(
        'FontPickerSection',
        'ColorPickerSection'
    );
}