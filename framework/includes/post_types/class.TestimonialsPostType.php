<?php

class TestimonialsPostType extends CustomPostTypeBaseClass
{
    protected $slug = 'testimonials';
    protected $postTypeName = 'Testimonial';
    protected $postTypeNamePlural = 'Testimonials';
    protected $capability_type = 'page';
    protected $supports = array( 'title', 'thumbnail' );
}