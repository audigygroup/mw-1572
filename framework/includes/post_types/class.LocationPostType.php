<?php

class LocationPostType extends PageCustomPostType{
    protected $slug = 'location';
    protected $postTypeName = 'Location ';
    protected $postTypeNamePlural = 'Locations';
    protected $supports = array( 'title', 'thumbnail' );
}