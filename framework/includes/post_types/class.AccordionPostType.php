<?php

class AccordionPostType extends PostCustomPostType
{
    protected $slug = 'accordion';
    protected $postTypeName = 'Accordion ';
    protected $postTypeNamePlural = 'Accordions ';
    protected $supports = array( 'title' );
}