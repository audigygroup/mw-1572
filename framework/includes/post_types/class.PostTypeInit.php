<?php
class PostTypeInit
{
    private static $instance;

    private $baseClasses = array(
        'CustomPostTypeBaseClass',
        'CustomTaxonomyBaseClass',
        'PageCustomPostType',
        'PostCustomPostType'
    );
    private $postTypes = array(
        'TestimonialsPostType',
        'ProfessionalsPostType',
        'MainSliderPostType',
        'LocationPostType',
        'QuickNavPostType',
        'HearingAidTypesPostType',
        'AccordionPostType',
        'PatientFormsPostType',
        'AccessoriesPostType',
        'LifestylePostType',
        'LifestylePostType',
        'EPatientPostType',
        'EPatientPostType',
        'ComponentPostType',
        'EventsAndSeminarsPostType'
    );
    private $taxonomies = array(
        'QuickNavTaxonomy',
        'AccordionTaxonomy',
        'AccessoriesTaxonomy',
        'EPatientTaxonomy',
        'ComponentTaxonomy'
    );

    private function __construct()
    {
        $this->requireFiles();
        $this->registerPostTypes();
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /*
     * Require All Files related to post types
     */
    private function requireFiles()
    {
        //Base Classes
        foreach($this->baseClasses as $baseClass)
        {
            require_once( MW_POST_TYPES . 'base-classes/class.' . $baseClass . '.php' );
        }

        //Post Types
        foreach($this->postTypes as $postType)
        {
            require_once( MW_POST_TYPES . 'class.' . $postType . '.php' );
        }

        //Taxonomies
        foreach($this->taxonomies as $taxonomy)
        {
            require_once( MW_POST_TYPES . 'taxonomy/class.' . $taxonomy . '.php' );
        }
    }

    /*
     * Registers MW Post Types
     */
    private function registerPostTypes()
    {
        $objs = array(
            $this->postTypes,
            $this->taxonomies
        );
        //post Types and taxonomies
        foreach($objs as $obj)
        {
            foreach($obj as $init){
                new $init();
            }
        }
    }
}