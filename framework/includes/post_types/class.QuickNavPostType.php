<?php

class QuickNavPostType extends PageCustomPostType {

    protected $slug = 'quick-nav';
    protected $postTypeName = 'Quick Nav ';
    protected $postTypeNamePlural = 'Quick Nav ';
    protected $supports = array( 'title' );

}