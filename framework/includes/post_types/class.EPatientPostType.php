<?php

class EPatientPostType extends PostCustomPostType
{
    protected $slug = 'e-patient-video';
    protected $postTypeName = 'E-Patient Video ';
    protected $postTypeNamePlural = 'E-Patient Videos ';
    protected $supports = array( 'title', 'thumbnail' );
}