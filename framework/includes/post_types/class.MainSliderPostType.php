<?php

class MainSliderPostType extends PageCustomPostType{
    protected $slug = 'slider';
    protected $postTypeName = 'Main Slider ';
    protected $postTypeNamePlural = 'Main Slider ';
    protected $supports = array( 'title', 'thumbnail' );
}