<?php

class ProfessionalsPostType extends PageCustomPostType {

    protected $slug = 'professional';
    protected $postTypeName = 'Professional';
    protected $postTypeNamePlural = 'Professionals';
    protected $supports = array( 'title', 'thumbnail' );

}