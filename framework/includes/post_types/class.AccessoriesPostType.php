<?php

class AccessoriesPostType extends PostCustomPostType
{
    protected $slug = 'accessories';
    protected $postTypeName = 'Hearing Aid Accessories';
    protected $postTypeNamePlural = 'Hearing Aid Accessories ';
    protected $supports = array( 'title', 'thumbnail' );

    public function __construct()
    {
        new AccessoriesTaxonomy();
        $this->createsPostType();
    }
}