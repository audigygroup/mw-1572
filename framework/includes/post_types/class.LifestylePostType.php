<?php

class LifestylePostType extends PageCustomPostType{
    protected $slug = 'lifestyle';
    protected $postTypeName = 'Lifestyle ';
    protected $postTypeNamePlural = 'Lifestyles';
    protected $supports = array( 'title', 'thumbnail' );
}