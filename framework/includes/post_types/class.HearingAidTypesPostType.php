<?php

class HearingAidTypesPostType extends PostCustomPostType
{
    protected $slug = 'hearing-aid-type';
    protected $postTypeName = 'Hearing Aid Types';
    protected $postTypeNamePlural = 'Hearing Aid Types';
    protected $supports = array( 'title' );
}