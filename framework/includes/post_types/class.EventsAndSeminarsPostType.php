<?php

class EventsAndSeminarsPostType extends PostCustomPostType
{
    protected $slug = 'events-and-seminars';
    protected $postTypeName = 'Events And Seminars';
    protected $postTypeNamePlural = 'Events And Seminars';
    protected $supports = array( 'title', 'thumbnail', 'excerpt' );
}