<?php

class PatientFormsPostType extends PostCustomPostType
{
    protected $slug = 'patient-forms';
    protected $postTypeName = 'Patient Form ';
    protected $postTypeNamePlural = 'Patient Forms ';
    protected $supports = array( 'title' );
}