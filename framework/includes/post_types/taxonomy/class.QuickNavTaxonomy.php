<?php
/*
 * Quick Nav Taxonomy
 */
class QuickNavTaxonomy extends CustomTaxonomyBaseClass
{

    protected $tax_name      = 'Quick Nav Categories';
    protected $taxonomy_name = 'quick_nav_cat';
    protected $object_type   = 'quick-nav';

}