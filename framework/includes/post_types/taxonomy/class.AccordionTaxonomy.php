<?php

class AccordionTaxonomy extends CustomTaxonomyBaseClass
{

    protected $tax_name      = 'Accordion Categories';
    protected $taxonomy_name = 'accordion_cat';
    protected $object_type   = 'accordion';

}