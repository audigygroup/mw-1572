<?php

class AccessoriesTaxonomy extends CustomTaxonomyBaseClass
{

    protected $tax_name      = 'Accessories Categories';
    protected $taxonomy_name = 'accessories_cat';
    protected $object_type   = 'accessories';

}