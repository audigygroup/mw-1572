<?php

class ComponentTaxonomy extends CustomTaxonomyBaseClass
{
    protected $tax_name      = 'Pages for Components';
    protected $taxonomy_name = 'comp_cat';
    protected $object_type   = 'component';
}