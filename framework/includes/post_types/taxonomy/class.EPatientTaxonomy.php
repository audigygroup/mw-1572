<?php

class EPatientTaxonomy extends CustomTaxonomyBaseClass
{
    protected $tax_name      = 'E-Patient Categories';
    protected $taxonomy_name = 'e_patient_cat';
    protected $object_type   = 'e-patient-video';
}