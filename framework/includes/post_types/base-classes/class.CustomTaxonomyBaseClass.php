<?php

class CustomTaxonomyBaseClass
{

    protected $tax_name;
    protected $singular_name     = 'Categories';
    protected $search_items      = 'Search Types';
    protected $all_items         = 'All Types';
    protected $parent_item       = 'Parent Type';
    protected $parent_item_colon = 'Parent Type:';
    protected $edit_item         = 'Edit Type';
    protected $update_item       = 'Update Type';
    protected $add_new_item      = 'Add New Type';
    protected $new_item_name     = 'New Type Name';
    protected $menu_name;

    protected $show_ui           = true;
    protected $hierarchical      = true;
    protected $show_admin_column = true;
    protected $query_var         = true;
    protected $rewrite           = true;

    protected $taxonomy_name;
    protected $object_type;


    public function __construct()
    {
        $this->createTaxonomy();
    }


    public function createTaxonomy(){
        $labels = array(
            'name'              => $this->tax_name,
            'singular_name'     => $this->singular_name,
            'search_items'      => $this->search_items,
            'all_items'         => $this->all_items,
            'parent_item'       => $this->parent_item,
            'parent_item_colon' => $this->parent_item_colon,
            'edit_item'         => $this->edit_item,
            'update_item'       => $this->update_item,
            'add_new_item'      => $this->add_new_item,
            'new_item_name'     => $this->new_item_name,
            'menu_name'         => $this->menu_name
        );

        $args = array(
            'labels' => $labels,
            'show_ui'           =>  $this->show_ui,
            'hierarchical'      =>  $this->hierarchical,
            'show_admin_column' =>  $this->show_admin_column,
            'query_var'         =>  $this->query_var,
            'rewrite'           =>  $this->rewrite
        );
        register_taxonomy( $this->taxonomy_name, $this->object_type, $args );
    }

}