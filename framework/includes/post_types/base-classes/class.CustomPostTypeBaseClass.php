<?php

class CustomPostTypeBaseClass
{
    protected $postTypeName;
    protected $postTypeNamePlural;
    protected $slug;
    protected $public = true;

    /**
     * @var bool - Must be true to work on public side (i.e., to use single-{post-type}.php)
     */
    protected $publicly_queryable = true;
    protected $show_ui = true;
    protected $show_in_menu = true;
    protected $query_var = true;
    protected $rewrite = true;
    protected $capability_type = 'post';
    protected $has_archive = false;

    /**
     * @var bool - Must be true for Sortable Post Type to work
     */
    protected $hierarchical = true;
    protected $menu_position = null;
    protected $supports;

    /**
     * @var bool - Used to indicate if post type is sortable, used to force hierarchical flag if needed
     */
    protected $isPostTypeSortable = true;

    public function __construct()
    {
        $this->createsPostType();

    }

    public function createsPostType()
    {

        $labels = array(
            'name'               => _x( $this->postTypeNamePlural, $this->postTypeNamePlural, 'Merriweather' ),
            'singular_name'      => _x( $this->postTypeNamePlural, $this->postTypeNamePlural, 'Merriweather' ),
            'menu_name'          => _x( $this->postTypeNamePlural, 'admin menu', 'Merriweather' ),
            'name_admin_bar'     => _x( $this->postTypeNamePlural, 'add new on admin bar', 'Merriweather' ),
            'add_new'            => _x( 'Add New ', $this->postTypeName, 'Merriweather' ),
            'add_new_item'       => __( 'Add New ' . $this->postTypeName, 'Merriweather' ),
            'new_item'           => __( 'New ' . $this->postTypeName, 'Merriweather' ),
            'edit_item'          => __( 'Edit ' . $this->postTypeName, 'Merriweather' ),
            'view_item'          => __( 'View ' . $this->postTypeNamePlural, 'Merriweather' ),
            'all_items'          => __( 'All ' . $this->postTypeNamePlural, 'Merriweather' ),
            'search_items'       => __( 'Search ' . $this->postTypeNamePlural, 'Merriweather' ),
            'parent_item_colon'  => __( 'Parent ' . $this->postTypeName . ':', 'Merriweather' ),
            'not_found'          => __( 'No ' . $this->postTypeNamePlural . 'found.', 'Merriweather' ),
            'not_found_in_trash' => __( 'No ' . $this->postTypeNamePlural . ' found in Trash.', 'Merriweather' )
        );

        $args = array(
            'labels'             => $labels,
            'public'             => $this->public,
            'publicly_queryable' => $this->publicly_queryable,
            'show_ui'            => $this->show_ui,
            'show_in_menu'       => $this->show_in_menu,
            'query_var'          => $this->query_var,
            'rewrite'            => $this->rewrite,
            'capability_type'    => $this->capability_type,
            'has_archive'        => $this->has_archive,
            'hierarchical'       => $this->checkIfPostTypeSortable(),
            'menu_position'      => $this->menu_position,
            'supports'           => $this->supports
        );

        register_post_type( $this->slug, $args );
    }


    protected function checkIfPostTypeSortable() {
        if ($this->isPostTypeSortable) {
            return true;
        } else {
            return $this->hierarchical;
        }
    }
}