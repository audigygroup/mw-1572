<?php

class ComponentPostType extends PageCustomPostType
{
    protected $slug = 'component';
    protected $postTypeName = 'Component ';
    protected $postTypeNamePlural = 'Components';
    protected $supports = array('title');
}