<?php

/**
 * The our people section uses PowerPlant to obtain employee data.
 * The persons name in slug format (david-wright) is used as the key to lookup the data.
 *
 * To maintain pretty URLs, this section adds rewrite rules to send the user slug to PowerPlant and have their
 * data appear in the local site.
 *
 * There can be no pages within the local site that match any of the peoples slugs
 */
class OurProfessionalsRewrite
{
    const REWRITE_REGEX = '^about-us/our-professionals/(.+?)(/[0-9]+)?/?$';
    const REWRITE_REDIRECT = 'index.php?professional=$matches[1]&page=$matches[2]';


    function __construct()
    {
        add_filter('init', array($this, 'add_rewrite_rule'));
        //add_filter('query_vars', array($this, 'add_query_vars'));
        add_action('wp_loaded', array($this, 'flush_rules'));
    }

    /*
    function add_query_vars($vars)
    {
        array_push($vars, 'person', 'person_section');
        return $vars;
    }
    */

    function flush_rules()
    {
        $rules = get_option('rewrite_rules');

        //echo "<pre>"; print_r($rules); echo "</pre>";

        if (!isset($rules[OurProfessionalsRewrite::REWRITE_REGEX])) {
            global $wp_rewrite;
            $wp_rewrite->flush_rules();
        }
    }

    function add_rewrite_rule()
    {
        add_rewrite_rule(OurProfessionalsRewrite::REWRITE_REGEX, OurProfessionalsRewrite::REWRITE_REDIRECT, 'top');
    }
}

$ourPeopleRewrite = new OurProfessionalsRewrite();