<?php

class ControllerBase
{

    // Set to instance of Obj
    protected $model;

    //Set getter from filter base
    protected $setGetter;

    // Set View for output
    protected $view;


    protected function getModel()
    {
        $getModel = $this->setGetter;
        return $getModel;
    }

    public function viewOutput()
    {
        $output = $this->getModel();

        ob_start();

        include_once($this->view);

        $outputBuffer = ob_get_contents();

        ob_end_clean();

	    $powerPlantContentFilter = PowerPlantContentFilter::getInstance();
	    $outputBuffer = $powerPlantContentFilter->replaceContent($outputBuffer);

        if ( file_exists($this->view)  ) {
            echo $outputBuffer;
        }

    }
}