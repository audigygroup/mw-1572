<?php
if (!function_exists('MWBreadcrumbs')) {
    function MWBreadcrumbs()
    {
        global $post;

        //$separator = '<li class="separator"> - </li>';
        $separator = "";


        if (!is_home()) {

            echo vsprintf(
                '<li><a href="%s">%s</a></li>',
                array(
                    get_option('home'),
                    'Home'
                )
            );

            echo $separator;

            if (is_single()) {

                switch ($post->post_type) {

                    case 'professional':
                        echo '<li>About Us</li><li><a href="/about-us/our-professionals">Our Professionals</a></li><li class="active">' . get_the_title() . '</li>';
                        break;

                    case'request-an-appointment':
                    case 'location':
                        echo '<li>Contact</li><li class="active">' . get_the_title() . '</li>';
                        break;

                    case 'hearing-aid-type':
                        echo '<li>Find Hearing Aids</li><li><a href="/find-hearing-aids/find-hearing-types/">Find Hearing Types</a></li><li class="active">' . get_the_title() . '</li>';
                        break;

                    case'lifestyle':
                        echo '<li>Find Hearing Aids</li><li><a href="/find-hearing-aids/lifestyles">Lifestyles</a></li><li class="active">' . get_the_title() . '</li>';
                        break;
                    default:
                        echo '<li>' . get_the_title() . '</li>';
                        break;
                }
            }elseif(is_tax()){

                if(is_tax('accessories_cat')){
                    echo '<li>Find Hearing Aids</li><li><a href="/find-hearing-aids/hearing-aid-accessories/">Hearing Aid Accessories</a></li><li class="active">' . single_term_title("", false) . '</li>';
                }

            } elseif (is_single()) {

                echo '</li>' . $separator . '<li>';
                the_title();
                echo '</li>';

            } elseif (is_page()) {

                if ($post->post_parent) {
                    $anc = get_post_ancestors($post->ID);
                    $title = get_the_title();
                    $o = array();
                    foreach ($anc as $ancestor) {
                        $o[] =  '<li>' . get_the_title($ancestor) . '</li>';
                    }

                    $reversed = array_reverse($o);
                    echo implode("", $reversed);

                    echo '<li class="active">' . $title . '</li>';

                } else {

                    echo '<li class="active">' . get_the_title() . '</li>';
                }

            } elseif (is_search()) {

                echo '<li class="active">Search Results</li>';

            }elseif(is_404()){

                echo '<li class="active">404</li>';

            }
        }
    }
}
