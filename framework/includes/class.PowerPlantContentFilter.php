<?php

/**
 * Class PowerPlantContentFilter
 * This class performs the defined filters to replace hash tagged fields with the values from the database.
 * Initial focus:
 * - SEO title and description (Yoast filters)
 */
class PowerPlantContentFilter
{
    protected $searchTermArray = array();
    protected $replacementTermArray = array();

    protected $targetTpl = '###%s###';

    protected $practiceDataKeys = array(
        'practice_name' => 'options_practice_name',
        'practice_name_possessive' => 'options_practice_name_possessive',
        'a_an' => 'options_a_an'
    );

    protected $locationDataKeys = array(

    );

    protected $statesArray = array (
        'AL' => 'Alabama',
        'AK' => 'Alaska',
        'AZ' => 'Arizona',
        'AR' => 'Arkansas',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DE' => 'Delaware',
        'DC' => 'District of Colombia',
        'FL' => 'Florida',
        'GA' => 'Georgia',
        'HI' => 'Hawaii',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'IA' => 'Iowa',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'ME' => 'Maine',
        'MD' => 'Maryland',
        'MA' => 'Massachusetts',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MS' => 'Mississippi',
        'MO' => 'Missouri',
        'MT' => 'Montana',
        'NE' => 'Nebraska',
        'NV' => 'Nevada',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NM' => 'New Mexico',
        'NY' => 'New York',
        'NC' => 'North Carolina',
        'ND' => 'North Dakota',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'OR' => 'Oregon',
        'PA' => 'Pennsylvania',
        'PR' => 'Puerto Rico',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dakota',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VT' => 'Vermont',
        'VA' => 'Virginia',
        'WA' => 'Washington',
        'WV' => 'West Virginia',
        'WI' => 'Wisconsin',
        'WY' => 'Wyoming',
    );

    private function __construct()
    {

    }

    public static function getInstance()
    {
        return new PowerPlantContentFilter();
    }

    /**
     * WordPress filters
     */
    public function doFilters()
    {
        add_filter( 'the_content', array( $this, 'replaceContent' ), 10, 3 );
        add_filter( 'wpseo_title', array( $this, 'replaceContent' ), 10, 3 );
        add_filter( 'wpseo_metadesc', array( $this, 'replaceContent' ), 10, 3 );
    }

    /**
     * filter callback method
     *
     * @param $content
     *
     * @return mixed
     */
    public function replaceContent( $content )
    {
        if (empty($this->searchTermArray) && empty($this->replacementTermArray)) {
            $this->getReplacementData();
            //echo "<pre>"; print_r($this->searchTermArray); echo "</pre>";
        }
        $content = str_replace( $this->searchTermArray, $this->replacementTermArray, $content );
        return $content;
    }

    protected function getReplacementData()
    {
        $this->getPracticeData();
        $this->getLocationsData();
    }

    /**
     * Add the practice data to get swapped
     */
    protected function getPracticeData()
    {
        $practiceDataModel = PracticePostData::getInstance();
        $practice_data = $practiceDataModel->getModel();

        foreach ($this->practiceDataKeys as $key => $field) {
            $this->addKeyValue( $key, $practice_data[$field] );
        }
    }

    /**
     * Add locations data to get replaced
     */
    protected function getLocationsData()
    {
        //Using Location Model Filter from LocationModel for component
        $locationModelFilter = LocationModelFilter::getInstance();
        $locations = $locationModelFilter->getFilteredModel();

        //echo "<pre>"; print_r($locations); echo "</pre>";

        if (empty($locations)) { return; }

        $locationNumber = 1;
        foreach ($locations as $location) {

            $locationPrefix = sprintf('location%s_', $locationNumber);
            $this->addLocationKeysValues($locationPrefix, $location);

            // Check to see if this is the primary location. If so, then create another set of content maps for it
            if (!empty($location['primary_location'])) {
                $this->addLocationKeysValues('primary_location_', $location);
            }

            $locationNumber++;
        }
    }

    protected function addLocationKeysValues($locationPrefix, $location)
    {
        $this->addKeyValue($locationPrefix . 'title', $location['post_title']);
        $this->addKeyValue($locationPrefix . 'area_name', $location['location_area_name']);
        $this->addKeyValue($locationPrefix . 'full_address', $location['full_address']);
        $this->addKeyValue($locationPrefix . 'address', $location['address']);
        $this->addKeyValue($locationPrefix . 'city', $location['city']);
        $this->addKeyValue($locationPrefix . 'city_possessive', $location['city_possessive']);
        $this->addKeyValue($locationPrefix . 'state_full', $this->getFullState($location['state']));
        $this->addKeyValue($locationPrefix . 'state_possessive', $location['state_possessive']);
        $this->addKeyValue($locationPrefix . 'state_abbreviated', $location['state']);
        $this->addKeyValue($locationPrefix . 'zip_code', $location['zip_code']);
        $this->addKeyValue($locationPrefix . 'phone_number', $location['phone_number']);
        $this->addKeyValue($locationPrefix . 'email', $location['email']);
    }

    protected function getFullState($stateAbbreviation)
    {
        if (array_key_exists($stateAbbreviation, $this->statesArray)) {
            return $this->statesArray[$stateAbbreviation];
        } else {
            return "missing state";
        }
    }
    /**
     * addKeyValue
     * Adds key,value pair to array used by str_replace
     * @param $key
     * @param $value
     */
    protected function addKeyValue( $key, $value )
    {
        $hash = sprintf( $this->targetTpl, $key );

        if ( ! empty( $key ) && ! empty( $value )) {
            $this->searchTermArray[]      = $hash;
            $this->replacementTermArray[] = $value;
        }
    }

}

$powerPlantContentFilter = PowerPlantContentFilter::getInstance();
$powerPlantContentFilter->doFilters();