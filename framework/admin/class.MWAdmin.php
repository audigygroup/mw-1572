<?php

class MWAdmin
{

    private static $instance;

    private function __construct()
    {
        PostTypeSortable::getInstance();
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );
        $this->practiceTab();
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /*
     * Enqueues Reorder Plugin for admin
     */
    public function enqueue_admin_scripts()
    {
        wp_enqueue_script( 'merriweather-script',
            get_template_directory_uri() . '/framework/assets/js/plugins/reorder.js', array( 'jquery' ), '', true );
    }

    /*
     * Enqueues Admin Specific minified Style sheet
     */
    public function enqueue_admin_styles()
    {
        wp_register_style( 'enqueue_admin_styles', get_template_directory_uri() . '/framework/assets/css/admin.min.css',
            false );
        wp_enqueue_style( 'enqueue_admin_styles' );
    }

    /*
     * Practice Tab in Admin
     */
    public function practiceTab(){

        if( function_exists('acf_add_options_page') ) {

            acf_add_options_page(array(
                'page_title' 	=> 'Practice',
                'menu_title'	=> 'Practice',
                'menu_slug' 	=> 'practice-settings',
                'capability'	=> 'edit_posts',
                'redirect'		=> false,
                'position'      => 3
            ));
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Practice',
                'menu_title'	=> 'Filter',
                'parent_slug'	=> 'practice-settings',
            ));
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Practice',
                'menu_title'	=> 'Welcome Component',
                'parent_slug'	=> 'practice-settings',
            ));
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Practice',
                'menu_title'	=> 'Call to Action Component',
                'parent_slug'	=> 'practice-settings',
            ));
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Practice',
                'menu_title'	=> 'Thank You Page',
                'parent_slug'	=> 'practice-settings',
            ));
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Practice',
                'menu_title'	=> 'Footer',
                'parent_slug'	=> 'practice-settings',
            ));
        }
    }
}
