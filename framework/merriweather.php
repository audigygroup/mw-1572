<?php

// Define directories
define( 'MW_ADMIN', get_template_directory() . '/framework/admin/' );
define( 'MW_PUBLIC', get_template_directory() . '/framework/public/' );
define( 'MW_INC', get_template_directory() . '/framework/includes/' );
define( 'MW_POST_TYPES', MW_INC . 'post_types/' );
define( 'MW_VIEW', get_template_directory() . '/framework/public/views/' );
define( 'MW_CONTROLLER', get_template_directory() . '/framework/public/controller/' );
define( 'MW_MODEL', get_template_directory() . '/framework/public/model/' );
define( 'MW_VENDOR', get_template_directory() . '/framework/vendor/' );

//adds support for Featured Images
add_theme_support( 'post-thumbnails' );

// This theme uses wp_nav_menu()
register_nav_menus( array(
    'primary'   => __( 'Primary menu', 'merriweather'),
));
//Advanced Custom Fields
require_once( get_template_directory() . '/framework/includes/acfData.php' );

// Navigation
require_once( MW_INC . 'class.WpBootstrapNavwalker.php' );

/*
 * Post Types
 */
require_once( MW_INC . 'post_types/class.PostTypeInit.php' );
PostTypeInit::getInstance();

require_once( MW_INC . 'professionals-rewrite-rule.php' );
require_once( MW_INC . 'breadcrumbs.php' );

require_once( MW_INC . 'class.PowerPlantContentFilter.php' );

/*
*
* This is for the Public Side
*
*/
require_once( MW_INC . 'customizer/fonts/class.MWFontOptions.php' );
require_once( MW_INC . 'customizer/fonts/class.MWFontCombos.php' );


// Public Only Configuration/Features
require_once( MW_PUBLIC . 'class.MWPublic.php' );


// Admin Only Configuration/Features
if (is_admin()) {
    require_once( MW_ADMIN . 'class.MWAdmin.php' );
    add_action( 'init', array( 'MWAdmin', 'getInstance' ) );
    require_once( MW_INC . 'class.PostTypeSortable.php' );
}

/*
 * Customizer
 */
require_once( MW_INC . 'customizer/customizer_init.php' );
require_once( MW_VENDOR . 'less.php/Less.php' );

/*
 * Public Models
 */
require_once( MW_PUBLIC . 'model/publicModels.php' );

/*
 * Public CTRL
 */
//Controller Base Class
require_once(MW_INC . 'controller/baseClasses/class.ControllerBase.php');

//Master Controller
require_once(MW_PUBLIC . 'controller/class.MasterController.php');
new MasterController();


// does not enqueue when is_admin() needs to be here or in a class for both admin and public
function mw_customizer_live_preview()
{
    wp_enqueue_script( 'mw-theme-customizer', get_template_directory_uri() . '/framework/assets/js/theme-customizer/theme-customizer.min.js', array( 'jquery', 'customize-preview', 'jquery-ui-sortable'), '', true );
    wp_enqueue_script( 'web-font-loader', "http://ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js", array( 'mw-theme-customizer' ), '', true );
}

add_action( 'customize_preview_init', 'mw_customizer_live_preview' );

//Paginiation Class
require_once( MW_INC . 'class.MWPagination.php' );

/*
 * Load Gravity Forms scripts in Footer
 */
add_filter( 'gform_init_scripts_footer', '__return_true' );

add_action('wp_footer', 'mw_add_global_javascript_to_footer', 100);

function mw_add_global_javascript_to_footer() {
	echo get_option('options_global_javascript_code');
}