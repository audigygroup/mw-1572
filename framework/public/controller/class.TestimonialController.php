<?php

class TestimonialController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  TestimonialModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/testimonialTpl.php';
    }
}