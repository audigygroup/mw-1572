<?php

class MainSliderController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  MainSliderModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/mainSliderTpl.php';
    }
}