<?php
//This is the render controller
require_once(MW_PUBLIC . 'controller/render/class.ComponentRenderController.php');

/*
 * Requires all Controllers that are being used besides the Base Controller
 */
class MasterController
{

    /*
     * Class Name as Key and Pretty name as value
     */
    protected static $classNames = array(
        'TopBarController'                      => 'Top Bar',
        'HeaderController'                      => 'Header',
        'MainSliderController'                  => 'Main Slider',
        'TestimonialController'                 => 'Testimonial',
        'TestimonialPageController'             => 'Testimonial Page',
        'WelcomeController'                     => 'Welcome',
        'QuickNavController'                    => 'Quick Nav',
        'HearingAidTypeController'              => 'Hearing Aid Type',
        'CopyrightController'                   => 'Copyright',
        'CallToActionController'                => 'Call To Action',
        'FooterController'                      => 'Footer',
        'OurPracticeController'                 => 'Our Practice',
        'OurProfessionalsController'            => 'Our Professionals',
        'OurProfessionalDetailController'       => 'Our Professional Detail',
        'SideBarController'                     => 'Side Bar',
        'InsurancePageController'               => 'Insurance Page',
        'PatientFormsController'                => 'Patient Forms',
        'ReviewUsPageController'                => 'Review Us Page',
        'LocationDetailController'              => 'Location Detail',
        'ThankYouController'                    => 'Thank You',
        'RequestAppointmentController'          => 'Request Appointment',
        'HearingAidTypeLandingController'       => 'Hearing Aid Type Landing',
        'HearingAidTypeDetailController'        => 'Hearing Aid Type Detail',
        'HearingAidTypeDetailNav'               => 'Hearing Aid Type Detail Nav',
        'HearingAidAccessoriesController'       => 'Hearing Aid Accessories',
        'HearingAidAccessoriesDetailController' => 'Hearing Aid Accessories Detail',
        'HearingAidAccessoriesDetailNav'        => 'Hearing Aid Accessories Detail Nav',
        'FinancingOptionsController'            => 'Financing Options',
        'LifestyleLandingController'            => 'Lifestyle Landing',
        'LifestyleDetailController'             => 'Lifestyle Detail',
        'AccordionComponentController'          => 'Accordion Component',
        'EPatientComponentController'           => 'EPatient Component',
        'GoogleAnalyticsController'             => 'Google Analytics',
        'BlogLandingController'                 => 'Blog Landing Component',
        'BlogSingleController'                  => 'Blog Single Component',
        'EventsAndSeminarsLandingController'    => 'Events and Seminars Component',
        'EventsAndSeminarsSingleController'     => 'Events And Seminars Single',
        'LocationController'                    => 'Location Four Location Component',
        'LocationMapComponentController'        => 'Location Map Component',
        'LocationTwoMapComponentController'     => 'Location Two Map Component'
    );

    public function __construct()
    {
        $this->requireClasses();
    }

    /*
     * Require all Classes that are defined in protected property $classNames
     */
    protected function requireClasses()
    {
        foreach(array_keys(self::$classNames) as $class)
        {
            require_once(MW_CONTROLLER . 'class.' . $class . '.php');
        }
    }

    /*
     * Static Getter. To get Variable
     */
    public static function classNameGetter()
    {
        return self::$classNames;
    }

}
