<?php

class LifestyleLandingController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new LifestyleLandingFactory();
        $this->setGetter = $this->model->createModel();
        $this->view = MW_VIEW . 'components/lifestyleLandingTpl.php';
    }
}