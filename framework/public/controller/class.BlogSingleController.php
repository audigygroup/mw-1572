<?php

class BlogSingleController extends ControllerBase
{
    public function __construct()
    {
        $this->model = BlogSingleModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/blogSingleTpl.php';
    }
}