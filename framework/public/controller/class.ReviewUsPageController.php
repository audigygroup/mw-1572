<?php

class ReviewUsPageController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new ReviewUsFilterFactory();
        $this->setGetter = (array) $this->model->createModel();
        $this->view = MW_VIEW . 'components/reviewUsTpl.php';
    }
}