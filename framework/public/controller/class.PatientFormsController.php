<?php

class PatientFormsController extends ControllerBase
{
    public function __construct()
    {
        $this->model = new PatientFormsFilterFactory();
        $this->setGetter = (array) $this->model->createModel();
        $this->view = MW_VIEW . 'components/patientFormsTpl.php';
    }
}