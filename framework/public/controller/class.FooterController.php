<?php

class FooterController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  FooterModel::getInstance();
        $this->setGetter = $this->model->getFooterModel();
        $this->view = MW_VIEW . 'components/footerTpl.php';
    }
}