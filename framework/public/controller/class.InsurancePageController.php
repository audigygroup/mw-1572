<?php

class InsurancePageController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  InsuranceModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/insuranceTpl.php';
    }
}