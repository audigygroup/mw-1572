<?php

class QuickNavController extends ControllerBase
{

    public function __construct()
    {
        $this->model =  QuickNavModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();

        if(is_home()){
            $this->view = MW_VIEW . 'components/quickNavTpl.php';
        }else{
            $this->view = MW_VIEW . 'components/quickNavInteriorTpl.php';
        }

        $this->enqueueMWIcons();
    }

    protected function enqueueMWIcons()
    {
        wp_enqueue_style( 'mw-icons', '/wp-content/themes/' .  get_template() . '/framework/includes/merriweather-icons/css/enterprise.min.css' );
    }
}