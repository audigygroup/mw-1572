<?php

class EventsAndSeminarsSingleController extends ControllerBase
{
    public function __construct()
    {
        $this->model = new EventsAndSeminarsSingleFactory();
        $this->setGetter = (array) $this->model->createModel();
        $this->view = MW_VIEW . 'components/eventsAndSeminarsSingleTpl.php';
    }

}