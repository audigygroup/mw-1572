<?php

class ThankYouController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  ThankYouModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredData();
        $this->view = MW_VIEW . 'components/thankYouTpl.php';
    }
}