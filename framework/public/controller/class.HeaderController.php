<?php

class HeaderController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new HeaderFilterFactory();
        $this->setGetter = $this->model->createModel();
        $this->view = MW_VIEW . 'components/headerTpl.php';
    }
}