<?php

class SideBarController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new SideBarFactory();
        $this->setGetter = (array) $this->model->createModel();
        $this->view = MW_VIEW . 'components/sideBarTpl.php';
    }
}