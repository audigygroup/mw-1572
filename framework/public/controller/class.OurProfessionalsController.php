<?php

class OurProfessionalsController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new OurProfessionalsFilterFactory();
        $this->setGetter = (array) $this->model->createModel();
        $this->view = MW_VIEW . 'components/ourProfessionalsTpl.php';
    }
}