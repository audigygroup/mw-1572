<?php

class LocationMapComponentController extends ControllerBase
{
    public function __construct()
    {
        $this->model = LocationMapComponentModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/locationMapComponentTpl.php';
    }
}