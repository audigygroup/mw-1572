<?php

class LocationTwoMapComponentController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  LocationModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/locationTpl.php';
    }
}