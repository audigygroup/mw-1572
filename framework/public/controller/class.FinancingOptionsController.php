<?php

class FinancingOptionsController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  FinancingOptionsModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/financingOptionsTpl.php';
    }
}