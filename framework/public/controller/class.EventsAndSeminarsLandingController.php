<?php

class EventsAndSeminarsLandingController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  EventsAndSeminarsLandingModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/eventsAndSeminarsLandingTpl.php';
    }
}