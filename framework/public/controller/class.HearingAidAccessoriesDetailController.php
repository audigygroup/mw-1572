<?php

class HearingAidAccessoriesDetailController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new HearingAidAccessoriesDetailFactory();
        $this->setGetter = $this->model->createModel();
        $this->view = MW_VIEW . 'components/hearingAidAccessoriesDetailTpl.php';
    }
}