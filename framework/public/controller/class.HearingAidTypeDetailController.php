<?php

class HearingAidTypeDetailController extends ControllerBase
{
    public function __construct()
    {
        $this->model = HearingAidTypeDetailModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/hearingAidTypeDetailTpl.php';
    }
}