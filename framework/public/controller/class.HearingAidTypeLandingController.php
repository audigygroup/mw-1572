<?php

class HearingAidTypeLandingController extends ControllerBase
{
    public function __construct()
    {
        $this->model = new HearingAidTypeLandingFactory();
        $this->setGetter = (array) $this->model->createModel();
        $this->view = MW_VIEW . 'components/hearingAidTypeLandingTpl.php';
    }
}