<?php

class RequestAppointmentController extends ControllerBase
{
    public function __construct()
    {
        $obj = new RequestAppointmentFactory();
        $this->setGetter = (array) $obj->createModel();
        $this->view = MW_VIEW . 'components/requestAppointmentTpl.php';

    }

}