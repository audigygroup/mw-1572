<?php

class CopyrightController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  CopyrightModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredData();
        $this->view = MW_VIEW . 'components/copyrightTpl.php';
    }
}