<?php

class AccordionComponentController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  AccordionComponentModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();


        //logic to set which template to use
        global $post;
        if($post->post_type == 'page' && is_page_template('hearing-aid-types.php')) {
            $this->view = MW_VIEW . 'components/AccordionComponentTpl.php';
        }elseif(is_page_template('our-practice.php')){
            $this->view = MW_VIEW . 'components/AccordionComponentTpl.php';
        }elseif($post->post_type == 'page'){
            $this->view = MW_VIEW . 'components/AccordionPageComponentTpl.php';
        }else{
            $this->view = MW_VIEW . 'components/AccordionComponentTpl.php';
        }
    }
}