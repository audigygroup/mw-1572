<?php

class LifestyleDetailController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  LifestyleDetailModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/lifestyleDetailTpl.php';
    }
}