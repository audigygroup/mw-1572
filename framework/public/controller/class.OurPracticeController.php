<?php

class OurPracticeController extends ControllerBase
{

    public function __construct()
    {
        $obj =  OurPracticeModelFilter::getInstance();
        $this->setGetter = $obj->getFilteredModel();
        $this->view = MW_VIEW . 'components/ourPracticeTpl.php';
    }

}