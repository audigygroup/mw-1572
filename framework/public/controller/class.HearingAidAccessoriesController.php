<?php

class HearingAidAccessoriesController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new HearingAidAccessoriesFilterFactory();
        $this->setGetter = $this->model->createModel();
        $this->view = MW_VIEW . 'components/hearingAidAccessoriesTpl.php';
    }
}