<?php

class EPatientComponentController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  EPatientComponentModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/ePatientTpl.php';
    }
}