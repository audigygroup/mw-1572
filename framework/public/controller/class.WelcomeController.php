<?php

class WelcomeController extends ControllerBase
{
    public function __construct()
    {
        $obj = new WelcomeFactory();
        $this->setGetter = (array) $obj->createModel();
        $this->view = MW_VIEW . 'components/welcomeTpl.php';
    }
}