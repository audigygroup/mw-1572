<?php

class HearingAidTypeController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  HearingAidTypeModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/hearingAidTypeTpl.php';
    }
}