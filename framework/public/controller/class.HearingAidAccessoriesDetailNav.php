<?php

class HearingAidAccessoriesDetailNav extends ControllerBase
{
    public function __construct()
    {
        $this->model = HearingAidAccessoriesCatModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'helpers/accessories-detail-nav.php';
    }
}