<?php

class LocationDetailController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new LocationPageFactory();
        // cast to array
        $this->setGetter = (array) $this->model->createModel();
        $this->view = MW_VIEW . 'components/locationDetailTpl.php';
    }
}