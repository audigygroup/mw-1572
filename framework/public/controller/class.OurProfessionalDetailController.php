<?php
class OurProfessionalDetailController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  OurProfessionalDetailModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/ourProfessionalDetailTpl.php';
    }
}