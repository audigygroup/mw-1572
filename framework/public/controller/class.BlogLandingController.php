<?php

class BlogLandingController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  BlogLandingModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/blogLandingTpl.php';
    }
}