<?php

class HearingAidTypeDetailNav extends ControllerBase
{
    public function __construct()
    {
        $this->model = HearingAidTypeNavModelFilter::getInstance();
        $this->setGetter = (array) $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'helpers/hearing-aid-detail-nav.php';
    }
}