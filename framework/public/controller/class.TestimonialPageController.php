<?php

class TestimonialPageController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  new TestimonialFilterFactory();
        $this->setGetter = (array) $this->model->createModel();
        $this->view = MW_VIEW . 'components/testimonialContentTpl.php';
    }
}