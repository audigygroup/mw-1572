<?php

class GoogleAnalyticsController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  GoogleAnalyticsModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredData();
        $this->view = MW_VIEW . 'components/googleAnalyticsTpl.php';
    }
}