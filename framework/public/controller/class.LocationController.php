<?php

class LocationController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  LocationModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredModel();
        $this->view = MW_VIEW . 'components/location4LocComponentTpl.php';
    }
}