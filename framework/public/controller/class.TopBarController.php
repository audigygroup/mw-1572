<?php

class TopBarController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  AddThisModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredData();
        $this->view = MW_VIEW . 'components/topBarTpl.php';
        $this->enqueueAddThis();
    }

    protected function enqueueAddThis()
    {
        wp_enqueue_script( 'addthis-script', 'http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-554923df7ef040e0', array( 'jquery'), '', true );
    }
}