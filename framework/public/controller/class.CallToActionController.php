<?php

class CallToActionController extends ControllerBase
{
    public function __construct()
    {
        $this->model =  CallToActionModelFilter::getInstance();
        $this->setGetter = $this->model->getFilteredData();
        $this->view = MW_VIEW . 'components/callToActionTpl.php';
    }
}