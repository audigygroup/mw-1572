<?php

class ComponentRenderController
{

    //Master List of controllers
    protected $controllers;

    public function __construct()
    {
        //Master List of controllers
        $this->controllers = MasterController::classNameGetter();
        $this->savePostTitleCallback();

    }

    protected function getModel()
    {
        $model = ComponentRenderFilter::getInstance();
        return $model->getFilteredModel();
    }

    /*
     * Gets Static method to look through array
     */
    protected function getAllControllers()
    {
        $model = $this->getModel();

        array_walk($model, array($this, 'renderCallback'));

    }

    /*
     * Callback for Array Walk
     */
    protected function renderCallback($model)
    {
        if(array_key_exists($model['component'], $this->controllers))
        {
            $comp = new $model['component'];
            $comp->viewOutput();
        }
    }

    /*
     * This method Changes post_title to pretty name of what is in the MasterController Class
     */
    public function savePostTitleCallback()
    {
        $model = $this->getModel();

        foreach($model as $post){
            if(array_key_exists($post['component'], $this->controllers)) {
                $prettyName = $this->controllers[$post['component']];
                $args = array(
                    'ID'         => $post['ID'],
                    'post_title' => $prettyName,
                );
                wp_update_post($args);
            }
        }
    }

    public function getter()
    {
        return $this->getAllControllers();
    }

}