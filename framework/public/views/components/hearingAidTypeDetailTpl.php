<?php
//echo '<pre>'; print_r($output); echo '</pre>';
$hearingAid = $output[0];
?>
<section id="single-hearing-aid-type">
    <div class="hearing-aid-images">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <?php echo wp_get_attachment_image( $hearingAid['image_left'], 'full', '',array('class' => 'img-circle center-block', 'alt' => $hearingAid['hearing_aid_type'] ) ); ?>
                </div>
                <div class="col-xs-12 col-md-4">
                    <?php echo wp_get_attachment_image( $hearingAid['image_center'], 'full', '',array('class' => 'center-block', 'alt' => $hearingAid['hearing_aid_type'] ) ); ?>
                </div>
                <div class="col-xs-12 col-md-4">
                    <?php echo wp_get_attachment_image( $hearingAid['image_right'], 'full', '',array('class' => 'img-circle center-block', 'alt' => $hearingAid['hearing_aid_type'] ) ); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="hearing-aid-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="hearing-aid-title text-center"><?php echo $hearingAid['hearing_aid_type'];?> Hearing Aids</h2>
                    <?php if(!empty($hearingAid['sub_headline'])):?>
                        <p class="sub-headline text-center"><?php echo $hearingAid['sub_headline'];?></p>
                    <?php endif;?>
                </div>
            </div>
            <div class="information-wrapper">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="features-wrap">
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <?php echo wp_get_attachment_image( $hearingAid['hearing_aid_drawing'], 'full', '',array('class' => 'ear-icon center-block', 'alt' => $hearingAid['hearing_aid_type'] ) ); ?>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <?php if(!empty($hearingAid['features_icons'])):?>
                                        <ul class="feature-icons list-unstyled list-inline">
                                            <?php $icons = unserialize($hearingAid['features_icons']) ;?>
                                            <?php foreach($icons as $icon):?>
                                                <li><i class="mw mw-features-<?php echo $icon;?>"></i></li>
                                            <?php endforeach;?>
                                        </ul>
                                    <?php endif;?>
                                    <div class="feature-descriptions">
                                        <?php if(!empty($hearingAid['feature_descriptions'])):?>
                                            <?php echo $hearingAid['feature_descriptions'];?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <?php if(!empty($hearingAid['hearing_aid_description'])):?>
                            <div class="content-wrap">
                                <h2>About this Type:</h2>
                                <p><?php echo $hearingAid['hearing_aid_description'];?></p>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>