<?php
    $lifestyle = $output[0];
//    echo '<pre>'; print_r($lifestyle); echo '</pre>';
?>
<div id="lifestyle-detail">
    <div class="row">
        <?php if( !empty($lifestyle['wistia_id']) && !empty($lifestyle['thumbnail_url']) ):?>
            <div class="col-xs-12">
                <div id="lifestyle-img" class="video-img" data-toggle="modal" data-target="#lifestyle-detail-video">
                    <img src="<?php echo $lifestyle['thumbnail_url']; ?>" alt="<?php echo $lifestyle['post_title']; ?>"/>
                    <div class="watch-button">
                        <h1>watch the video <i class="fa fa-chevron-right"></i></h1>
                    </div>
                </div>
                <div class="modal fade" id="lifestyle-detail-video" tabindex="-1" role="dialog" aria-labelledby="lifestyle-detail-video" aria-hidden="true">
                    <div class="modal-dialog modal-lg"">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            </div>
                            <div class="modal-body" id="lifestyles-detail-modal">
                                <div class="embed-responsive embed-responsive-4by3">
                                    <iframe data-lifestyles-detail="<?php echo $lifestyle['wistia_id'];?>" class="embed-responsive-item"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <div class="col-xs-12">
            <div class="content">
                <?php if(!empty($lifestyle['sub_headline'])):?>
                    <p class="sub-headline"><?php echo $lifestyle['sub_headline'];?></p>
                <?php endif;?>
                <?php if(!empty($lifestyle['content'])):?>
                    <p><?php echo $lifestyle['content'];?></p>
                <?php endif;?>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="post-links">
                <?php previous_post_link('<p class="pull-left"><i class="fa fa-chevron-left"></i> %link</p>'); ?>
                <?php next_post_link('<p class="pull-right">%link <i class="fa fa-chevron-right"></i></p>'); ?>
            </div>
        </div>
    </div>
</div>