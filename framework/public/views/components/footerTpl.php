<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<div id="footer" class="swatch-b">
    <div class="container">
        <div class="row multi-columns-row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="text-center">
                    <a href="<?php echo get_home_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
                        <img src="<?php echo wp_get_attachment_url( $output['options_footer_logo']);?>" alt="<?php bloginfo( 'name' ); ?>" class="footer-logo"/>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="contact-us">
                    <h4>Contact Us</h4>
                    <div class="clearfix">
                        <hr>
                    </div>
                    <?php foreach($output['location'] as $loc):?>
                        <div class="loc-info">
                            <?php if(! empty($loc['location_title'])):?>
                                <h5><?php echo $loc['location_title'];?></h5>
                            <?php else:?>
                                <h5><?php echo $loc['post_title'];?></h5>
                            <?php endif;?>
                            <ul class="list-unstyled">
                                <?php if(! empty($loc['phone_number'])):?>
                                    <li><i class="fa fa-phone"></i> <?php echo $loc['phone_number'];?></li>
                                <?php endif;?>
                            </ul>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="social-media">
                    <h4>Follow Us on Social Media</h4>
                    <div class="clearfix">
                        <hr>
                    </div>
                    <?php if ( file_exists(MW_VIEW . 'helpers/follow-addthis.php') ):?>
                        <ul class="list-unstyled list-inline addthis_toolbox addthis_32x32_style addthis_default_style">
                            <?php include( MW_VIEW . 'helpers/follow-addthis.php' );?>
                        </ul>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="learn-more">
                    <a href="<?php echo $output['options_powered_by_link'];?>" title="<?php echo $output['options_powered_by'];?>" target="_blank"><img src="<?php echo MW_IMAGES ?>/AG-certified-footer.png" alt="Audigy Certified Logo"></a>
                    <?php if(! empty($output['options_audigy_certified_desc'])):?>
                        <p class="cert-desc"><?php echo $output['options_audigy_certified_desc'];?></p>
                    <?php endif;?>
                    <a href="<?php echo $output['options_powered_by_link'];?>" title="<?php echo $output['options_powered_by'];?>" class="btn btn-default" target="_blank">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</div>