<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<div id="events-and-seminars-landing">
    <div class="row">
        <?php foreach($output as $key => $post):?>
            <div class="col-xs-12">
                <div class="post">
                    <div class="image">
                        <a href="<?php echo $post['permalink'];?>" title="<?php echo $post['post_title'] ;?>">
                            <?php if(!empty($post['thumbnail_url'])):?>
                                <img src="<?php echo $post['thumbnail_url'] ;?>" alt="<?php echo $post['post_title'] ;?>"/>
                            <?php else:?>
                                <img src="<?php echo MW_IMAGES;?>/blog_fallback_img.jpg" alt="<?php echo $post['post_title'] ;?>"/>
                            <?php endif;?>
                            <div class="date-fold hidden-xs">
                                <div class="date-wrap">
                                    <img src="<?php echo MW_IMAGES ?>/date-fold.png" alt="Date"/>
                                    <div class="date">
                                        <p class="month">
                                            <?php
                                                $dateFormat = "%B";
                                                $date = strtotime($post['event_date']);
                                                echo strftime($dateFormat, $date);
                                            ?>
                                        </p>
                                        <p class="day">
                                            <?php
                                                $dateFormat = "%d";
                                                echo strftime($dateFormat, $date);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="content-wrap">
                        <h2 class="title"><a href="<?php echo $post['permalink'];?>" title="<?php echo $post['post_title'] ;?>"><?php echo $post['post_title'] ;?></a></h2>
                        <div class="post-info">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="date">
                                        <?php
                                            $dateFormat = "%B %d, %Y";
                                            echo strftime($dateFormat, $date);
                                       ?> &bull; <?php echo $post['event_time'];?>
                                    </p>
                                    <p><?php echo $post['post_title'];?></p>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <?php if(!empty($post['post_excerpt'])):?>
                                <div class="col-xs-12">
                                    <p class="excerpt"><?php echo $post['post_excerpt'];?></p>
                                </div>
                            <?php endif;?>
                            <div class="col-xs-12">
                                <p><a href="<?php echo $post['permalink'];?>" title="Learn More" class="btn btn-default">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div>