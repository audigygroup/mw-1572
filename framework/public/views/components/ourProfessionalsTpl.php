<?php
    $pageContent = $output['ourProfessionalsPage'][0];
?>
<section>
    <div class="main-section-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-wrap">
                        <?php if(! empty($pageContent['our_professionals_headline'])):?>
                            <h2 class="text-center"><?php echo $pageContent['our_professionals_headline'];?></h2>
                        <?php endif;?>
                        <?php if(! empty($pageContent['our_professionals_sub_headline'])):?>
                            <p class="text-center sub-headline"><?php echo $pageContent['our_professionals_sub_headline'];?></p>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="row multi-columns-row">
                <?php foreach($output['professionals'] as $key => $person ): ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                        <div class="type-wrap">
                            <div class="featured-img-wrap">
                                <a href="<?php echo $person['permalink'];?>" title="<?php echo $person['full_name'];?>">
                                    <?php if( array_key_exists('thumbnail_url', $person) && !empty($person['thumbnail_url']) ):?>
                                        <img src="<?php echo $person['thumbnail_url'];?>" alt="<?php echo $person['full_name'];?>" class="img-circle"/>
                                    <?php else:?>
                                        <img src="<?php echo SITE_ROOT ?>framework/assets/images/professional-fallback.jpg" alt="<?php echo $person['full_name'];?>" class="img-circle"/>
                                    <?php endif;?>
                                    <span class="trans-hover"></span>
                                </a>
                            </div>
                            <div class="excerpt text-center">
                                <h3><a href="<?php echo $person['permalink'];?>" title="<?php echo $person['full_name'];?>"><?php echo $person['full_name'];?></a></h3>
                                <?php if(! empty($person['title'])):?>
                                    <p><?php echo $person['title'];?></p>
                                <?php endif;?>
                                <?php if(! empty($person['bio_excerpt'])):?>
                                    <p class="desc-excerpt"><?php echo $person['bio_excerpt'];?></p>
                                <?php endif;?>
                                <a href="<?php echo $person['permalink'];?>" title="Learn More">
                                    <button class="btn btn-default">Learn More</button>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>