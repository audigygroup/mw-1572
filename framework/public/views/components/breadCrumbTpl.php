<section>
    <div id="page-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>
                        <?php
                        if (is_tax()) {
                            echo single_term_title( "", false );
                        } elseif (is_search() && have_posts() ) {
                            printf(__('Search Results for: "%s"', 'merriweather'), get_search_query());
                        }elseif(is_404()) {
                            echo wp_title('404 -');
                        }elseif(is_tag()) {
                            echo wp_title('Tag -');
                        }elseif(is_category()){
                            echo wp_title('Category -');
                        } else {
                            echo $post->post_title;
                        }
                        ?>
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumbs hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ol class="breadcrumb">
                        <?php MWBreadcrumbs(); ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>