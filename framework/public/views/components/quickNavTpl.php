<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="practice-services">
    <div class="container">
        <div class="row multi-columns-row">
            <?php foreach ($output as $key => $qv): ?>
                <?php if ($key < 4): ?>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        <div class="option-wrap text-center">
                            <?php if ( ! empty( $qv['icon_class'] )): ?>
                                <p class="icon">
                                    <a href="<?php echo $qv['quick_nav_link']; ?>" title="<?php echo $qv['title']; ?>" class="active-icon">
                                        <span class="mw-stack">
                                            <i class="mw mw-circle mw-stack-1x"></i>
                                            <i class="mw mw-<?php echo $qv['icon_class']; ?> mw-stack-1x mw-color-flop"></i>
                                        </span>
                                    </a>
                                </p>
                            <?php endif; ?>
                            <?php if ( ! empty( $qv['title'] )): ?>
                                <h4><a href="<?php echo MW_HOME;?><?php echo $qv['quick_nav_link']; ?>" title="<?php echo $qv['title']; ?>"><?php echo $qv['title']; ?></a></h4>
                            <?php endif; ?>
                            <?php if ( ! empty( $qv['quick_nav_excerpt'] )): ?>
                                <p class="excerpt"><?php echo $qv['quick_nav_excerpt']; ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>