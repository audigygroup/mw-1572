<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
$page = $output['testimonialsPage'][0];
?>
<section id="testimonial-page">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-wrap">
                <?php if ( ! empty( $page['testimonials_headline'] )): ?>
                    <h2 class="headline"><?php echo $page['testimonials_headline']; ?></h2>
                <?php endif; ?>
                <?php if ( ! empty( $page['testimonials_sub_headline'] )): ?>
                    <p class="testimonial-sub-headline"><?php echo $page['testimonials_sub_headline']; ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <?php foreach ($output['testimonials'] as $index => $test) : ?>
            <div class="col-xs-12">
                <div class="testimonial-wrap">
                    <p class="presenter-info">
                        <?php if ( ! empty( $test['patient_name'] )): ?>
                            <strong><?php echo $test['patient_name']; ?>,</strong>
                        <?php endif; ?>
                        <?php if ( ! empty( $test['location_name'] )): ?>
                            <?php echo $test['location_name']; ?>
                        <?php endif; ?>
                        <?php if ( ! empty( $test['patient_since'] )): ?>
                            patient since <?php echo $test['patient_since']; ?>
                        <?php endif; ?>
                    </p>
                    <div class="row">
                        <?php if ( ! empty( $test['thumbnail_url'] )): ?>
                            <div class="col-xs-12 col-md-4 col-lg-3">
                                <img src="<?php echo $test['thumbnail_url'];?>" alt="<?php echo $test['patient_name'];?>" class="img-circle"/>
                            </div>
                        <?php endif; ?>
                        <?php if ( ! empty( $test['testimonial_quote'] )): ?>
                            <div class="col-xs-12 <?php if ( ! empty( $test['thumbnail_url'] )): ?>col-md-8 col-lg-9<?php endif; ?>">
                                <p><?php echo $test['testimonial_quote']; ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>