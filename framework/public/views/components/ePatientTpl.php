<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<script>
    var $ePatient = <?php echo json_encode($output);?>;
</script>
<section id="e-patient-videos">
    <div class="container">
        <div class="row">
            <?php foreach($output as $key => $video):?>
                <?php if ($key < 4): ?>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div id="video-container-<?php echo $key;?>" class="video-image" data-toggle="modal" data-target="#video-<?php echo $key;?>">
                            <img src="<?php echo $video['thumbnail_url'];?>" alt="<?php echo $video['post_title'];?>" class="video-thumb"/>
                            <div class="play-btn-icon">
                                <i class="fa fa-play"></i>
                            </div>
                        </div>
                        <div class="modal fade" id="video-<?php echo $key;?>" tabindex="-1" role="dialog" aria-labelledby="video-<?php echo $key;?>" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body" id="modal-<?php echo $key;?>">
                                        <div class="embed-responsive embed-responsive-4by3">
                                            <iframe class="embed-responsive-item"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="title text-center"><?php echo $video['post_title'];?></h4>
                    </div>
                <?php endif;?>
            <?php endforeach;?>
        </div>
    </div>
</section>