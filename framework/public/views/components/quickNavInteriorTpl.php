<?php
//echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="quick-nav-interior">
    <div class="container">
        <div class="row multi-columns-row">
            <?php foreach ($output as $key => $qn): ?>
                <?php if ($key < 4): ?>
                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <div class="qn-wrap text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="<?php echo $qn['quick_nav_link']; ?>" title="<?php echo $qn['title']; ?>" class="active-icon">
                                        <span class="mw-stack">
                                            <i class="mw mw-circle mw-stack-1x"></i>
                                            <i class="mw mw-<?php echo $qn['icon_class']; ?> mw-stack-1x mw-color-flop"></i>
                                        </span>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="<?php echo MW_HOME ?><?php echo $qn['quick_nav_link']; ?>" title="<?php echo $qn['title']; ?>" class="btn btn-default"><?php echo $qn['title']; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>