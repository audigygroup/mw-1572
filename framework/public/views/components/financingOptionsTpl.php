<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
    $page = $output[0];
?>
<div id="financing-options">
    <div class="heading-block">
        <div class="row">
            <div class="col-xs-12">
                <?php if(!empty($page['financing_options_title'])):?>
                    <h3><?php echo $page['financing_options_title'];?></h3>
                <?php endif;?>
                <?php if(!empty($page['financing_options_sub_headline'])):?>
                    <p class="sub-headline"><?php echo $page['financing_options_sub_headline'];?></p>
                <?php endif;?>
                <?php if(!empty($page['financing_options_paragraph'])):?>
                    <p><?php echo $page['financing_options_paragraph'];?></p>
                <?php endif;?>
            </div>
        </div>
    </div>
    <?php if($page['carecredit_section'] == 1):?>
        <div class="care-credit-container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="title">CareCredit® for Hearing Health</h2>
                    <p class="sub-headline">We accept CareCredit, the leading healthcare credit card, to make improving your hearing health easier and more affordable. With CareCredit, you can get the hearing instrument you need and conveniently pay for it over time with monthly payments that easily fit into your budget.</p>
                </div>
            </div>
            <div class="row vertical-align-md">
                <div class="col-xs-12 col-md-3">
                    <a href="http://www.carecredit.com" title="CareCredit" target="_blank"><img src="<?php echo MW_IMAGES; ?>/carecredit.png" alt="CareCredit" class="care-credit-img"/></a>
                </div>
                <div class="col-xs-12 col-md-9">
                    <div class="desc">
                        <h4>Your Healthcare Credit Card.</h4>
                        <p class="content">CareCredit is a healthcare credit card for every member of the family. CareCredit offers special financing on purchase of $200 or more* for healthcare costs not commonly covered by insurance, including hearing aids.</p>
                        <p class="learn-more"><a href="#" title="Learn More">Learn more</a> by visiting <a href="http://www.carecredit.com" title="CareCredit" target="_blank">www.carecredit.com</a> or contacting our office.</p>
                        <p><a href="https://www.carecredit.com/apply/confirm.html?encm=VjcAOQdlAGVWalA0VGAPZgE7UzEGYlhuDmxXb1YzVGg" title="Apply Now" class="btn btn-default">Apply Now</a></p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>
    <?php if($page['wells_fargo_section'] == 1):?>
        <div class="wells-fargo-container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="title">Wells Fargo Health Advantage Credit Card Program</h2>
                    <p class="sub-headline">Wells Fargo Health Advantage credit card brings you exclusive benefits:</p>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="img-container">
                        <a href="https://retailservices.wellsfargo.com/wfha_patient.html" title="Wells Fargo Health Advantage" target="_blank"><img src="<?php echo MW_IMAGES; ?>/wells-fargo.png" alt="Health Advantage" class="wells-fargo-img"/></a>
                        <p class="image-desc text-center">The Wells Fargo Health Advantage credit card is issued with approved credit by Wells Fargo Financial National Bank. Ask for details. </p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-9">
                    <div class="desc">
                        <ul class="content">
                            <li>Convenient monthly payments to fit your budget</li>
                            <li>Open line of credit for all your healthcare needs</li>
                            <li>Special terms promotions available, ask your healthcare provider for details</li>
                            <li>Easy to use online account management and billing payment options</li>
                        </ul>
                        <p><a href="https://retailservices.wellsfargo.com/wfha_patient.html" title="Apply Now" class="btn btn-default">Apply Now</a></p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>
</div>