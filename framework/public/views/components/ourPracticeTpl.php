<?php
//echo '<pre>';print_r($output); echo '</pre>';
$aboutUs = $output[0];
?>
<section id="our-practice">
    <div class="container">
        <div class="row">
            <?php if (!empty($aboutUs['mission_statement'])) : ?>
                <div class="col-xs-12">
                    <div class="mission-statment">
                        <h2>Our Mission</h2>
                        <p class="sub-headline"><?php echo $aboutUs['mission_statement']; ?></p>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (! empty($aboutUs['about_us'])) : ?>
                <div class="col-xs-12">
                    <div class="about-us-content">
                        <h2>About Us</h2>
                        <p><?php echo $aboutUs['about_us']; ?></p>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>