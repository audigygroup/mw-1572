<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
    $accessoryCat = $output['hearingAccessoriesCat'][0];
?>
<section id="accessories-detail">
    <div class="category-img">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img src="<?php echo $accessoryCat['taxonomy_metadata']['taxonomy_image']['img'];?>" alt="<?php echo $accessory['taxonomy_metadata']['taxonomy_image']['alt'];?>"/>
                </div>
            </div>
        </div>
    </div>
    <div class="accessories-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-block text-center">
                        <h2><?php echo $accessoryCat['name'];?></h2>
                        <?php if(!empty( $accessoryCat['taxonomy_metadata']['taxonomy_headline']['sub_headline'] )):?>
                            <p class="sub-headline"><?php echo $accessoryCat['taxonomy_metadata']['taxonomy_headline']['sub_headline'];?></p>
                        <?php endif;?>
                        <?php if(!empty( $accessoryCat['taxonomy_metadata']['taxonomy_headline']['sub_headline_paragraph'] )):?>
                            <p><?php echo $accessoryCat['taxonomy_metadata']['taxonomy_headline']['sub_headline_paragraph'];?></p>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="row multi-columns-row">
                <?php foreach($output['hearingAccessoriesPost'] as $accessory):?>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="accessory-post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="accessory-img">
                                        <img src="<?php echo $accessory['thumbnail_url'];?>" alt="<?php echo $accessory['post_title'];?>"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="accessory-desc">
                                        <h4><?php echo $accessory['post_title'];?></h4>
                                        <?php if(!empty($accessory['description'])):?>
                                            <p><?php echo $accessory['description'];?></p>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>