<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
    $insurance = $output[0];
?>
<div id="insurance-page">
    <div class="insurance-content">
        <div class="row">
            <div class="col-xs-12">
                <?php if(!empty($insurance['thumbnail_url'])):?>
                    <img src="<?php echo $insurance['thumbnail_url']; ?>" alt="<?php echo $insurance['post_title'];?>" class="main-img"/>
                <?php else:?>
                    <img src="<?php echo MW_IMAGES; ?>/insurance-page.jpg" alt="Insurance Image" class="main-img"/>
                <?php endif;?>
            </div>
            <div class="col-xs-12">
                <?php if(!empty($insurance['insurance_headline'])):?>
                    <h2><?php echo $insurance['insurance_headline'];?></h2>
                <?php endif;?>
                <?php if(!empty($insurance['insurance_content'])):?>
                    <p><?php echo $insurance['insurance_content'];?></p>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
