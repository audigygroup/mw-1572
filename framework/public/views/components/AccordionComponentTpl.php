<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="hearing-aid-type-faq">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php if(is_page_template('our-practice.php')):?>
                    <h2>7 Reasons to Visit</h2>
                <?php else:?>
                    <h2>Frequently Asked Questions</h2>
                <?php endif;?>
                <div class="panel-group" id="hearing-aid-type-accordion" role="tablist" aria-multiselectable="true">
                    <?php foreach($output as $key => $faq):?>
                        <?php if(!empty($faq['question']) && !empty($faq['answer']) ):?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading-<?php echo $key;?>">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#hearing-aid-type-accordion" href="#collapse-<?php echo $key;?>" aria-expanded="<?php if($key == 0):?>true <?php else :?>false<?php endif;?>" aria-controls="collapse-<?php echo $key;?>" <?php if($key !== 0):?>class="collapsed"<?php endif;?>>
                                            <span class="expand-box plus-minus"></span><?php echo $faq['question'];?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse-<?php echo $key;?>" class="panel-collapse collapse <?php if($key == 0):?>in<?php endif;?>" role="tabpanel" aria-labelledby="heading-<?php echo $key;?>">
                                    <div class="panel-body">
                                        <?php echo $faq['answer'];?>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
