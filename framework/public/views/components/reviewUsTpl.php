<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
    $page = $output['reviewUsPage'][0];
?>
<div id="review-us">
    <div class="row">
        <div class="col-xs-12">
            <?php if(!empty($page['review_us_headline'])):?>
                <p class="sub-headline"><?php echo $page['review_us_headline']; ?></p>
            <?php endif;?>
            <?php if(! empty($page['review_us_page_description'])):?>
                <p class="desc"><?php echo $page['review_us_page_description']; ?></p>
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="review-tabs" role="tabpanel">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <ul class="nav" role="tablist">
                            <?php foreach( $output['location'] as $index => $loc):?>
                                <li role="presentation" <?php if(!$index):?>class="active"<?php endif;?>><a href="#<?php echo $loc['post_name'];?>" aria-controls="<?php echo $loc['post_name'];?>" role="tab" data-toggle="tab"><?php echo $loc['location_title'];?></a></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-8">
                        <div class="tab-content">
                            <?php foreach( $output['location'] as $index => $loc):?>
                                <div role="tabpanel" class="tab-pane fade in <?php if(!$index):?>active<?php endif;?>" id="<?php echo $loc['post_name'];?>">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="instructions"><?php echo $loc['review_us_instructions'];?></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php if(!empty($loc['review_us_google_plus'])):?>
                                            <div class="col-xs-12 col-md-4 review-btn">
                                                <div class="col-xs-12 col-md-4 padding-0">
                                                    <div class="icon-wrap-google text-center">
                                                        <i class="fa fa-google-plus"></i>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8 padding-0">
                                                    <div class="review-text text-center">
                                                        <p><a href="<?php echo $loc['review_us_google_plus'];?>" title="Review us on Google+" target="_blank">Review us on Google+</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif;?>
                                        <?php if(!empty($loc['review_us_facebook'])):?>
                                            <div class="col-xs-12 col-md-4 review-btn">
                                                <div class="col-xs-12 col-md-4 padding-0">
                                                    <div class="icon-wrap-facebook text-center">
                                                        <i class="fa fa-facebook"></i>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8 padding-0">
                                                    <div class="review-text text-center">
                                                        <p><a href="<?php echo $loc['review_us_facebook'];?>" title="Review us on Facebook" target="_blank">Review us on Facebook</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif;?>
                                        <?php if(!empty($loc['review_us_yelp'])):?>
                                            <div class="col-xs-12 col-md-4 review-btn">
                                                <div class="col-xs-12 col-md-4 padding-0">
                                                    <div class="icon-wrap-yelp text-center">
                                                        <i class="fa fa-yelp"></i>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8 padding-0">
                                                    <div class="review-text text-center">
                                                        <p><a href="<?php echo $loc['review_us_yelp'];?>" title="Review us on Yelp" target="_blank">Review us on Yelp</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if(! empty($page['appreciation_message'])):?>
        <div class="row">
            <div class="col-xs-12">
                <p class="sub-headline"><?php echo $page['appreciation_message'];?></p>
            </div>
        </div>
    <?php endif;?>
    <div class="row">
        <div class="col-xs-12">
            <h2>Instructional videos on leaving an online review</h2>
            <p>Watch the videos below to learn how to post an online review of our practice using different websites.</p>
        </div>
        <div class="col-xs-12 instructional-videos">
            <div class="row">
                <?php if(! empty($page['google_plus_video_id'])):?>
                    <div class="col-xs-12 col-lg-4">
                        <div id="google-vid" class="video-img" data-toggle="modal" data-target="#google-plus">
                            <img src="<?php echo MW_IMAGES; ?>/review-us/review-us-google-plus.png" alt="Google+">
                            <div class="play-button-icon">
                                <i class="fa fa-play"></i>
                            </div>
                        </div>
                        <div class="modal fade" id="google-plus" tabindex="-1" role="dialog" aria-labelledby="google-plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body" id="google-plus-modal">
                                        <div class="embed-responsive embed-responsive-4by3">
                                            <iframe data-google="<?php echo $page['google_plus_video_id'];?>" class="embed-responsive-item"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="video-title">
                            <p class="text-center">Google+</p>
                        </div>
                    </div>
                <?php endif;?>
                <?php if(! empty($page['facebook_video_id'])):?>
                    <div class="col-xs-12 col-lg-4">
                        <div id="facebook-vid" class="video-img" data-toggle="modal" data-target="#facebook">
                            <img src="<?php echo MW_IMAGES; ?>/review-us/review-us-facebook.png" alt="Video">
                            <div class="play-button-icon">
                                <i class="fa fa-play"></i>
                            </div>
                        </div>
                        <div class="modal fade" id="facebook" tabindex="-1" role="dialog" aria-labelledby="facebook" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body" id="facebook-modal">
                                        <div class="embed-responsive embed-responsive-4by3">
                                            <iframe data-facebook="<?php echo $page['facebook_video_id'];?>" class="embed-responsive-item"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="video-title">
                            <p class="text-center">Facebook</p>
                        </div>
                    </div>
                <?php endif;?>
                <?php if(! empty($page['yelp_video_id'])):?>
                    <div class="col-xs-12 col-lg-4">
                        <div id="yelp-vid" class="video-img" data-toggle="modal" data-target="#yelp">
                            <img src="<?php echo MW_IMAGES; ?>/review-us/review-us-yelp.png" alt="Video">
                            <div class="play-button-icon">
                                <i class="fa fa-play"></i>
                            </div>
                        </div>
                        <div class="modal fade" id="yelp" tabindex="-1" role="dialog" aria-labelledby="yelp" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    </div>
                                    <div class="modal-body" id="yelp-modal">
                                        <div class="embed-responsive embed-responsive-4by3">
                                            <iframe data-yelp="<?php echo $page['yelp_video_id'];?>" class="embed-responsive-item" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="video-title">
                            <p class="text-center">Yelp</p>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>