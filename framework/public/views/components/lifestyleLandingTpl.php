<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
    $page = $output['lifestylePage'][0];
?>
<section id="lifestyle-landing">
    <div class="container">
        <div class="row">
            <?php if(!empty($page['lifestyles_landing_wistia_id'])):?>
                <div class="col-xs-12">
                    <div id="lifestyles-video-img" class="video-img" data-toggle="modal" data-target="#lifestyles-video">
                        <?php if(!empty($page['thumbnail_url'])):?>
                            <img src="<?php echo $page['thumbnail_url']; ?>" alt="<?php echo $page['post_title'];?>">
                        <?php else:?>
                            <img src="<?php echo MW_IMAGES; ?>/lifestyleslanding.jpg" alt="Lifestyles">
                        <?php endif;?>
                        <div class="watch-button">
                            <h1>watch the video <i class="fa fa-angle-right"></i></h1>
                        </div>
                    </div>
                    <div class="modal fade" id="lifestyles-video" tabindex="-1" role="dialog" aria-labelledby="lifestyles-video" aria-hidden="true">
                        <div class="modal-dialog modal-lg"">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                </div>
                                <div class="modal-body" id="lifestyles-modal">
                                    <div class="embed-responsive embed-responsive-4by3">
                                        <iframe data-lifestyles-landing="<?php echo $page['lifestyles_landing_wistia_id'];?>" class="embed-responsive-item"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <div class="col-xs-12">
                <div class="page-info">
                    <?php if(!empty($page['lifestyles_landing_sub_headline'])):?>
                        <p class="sub-headline"><?php echo $page['lifestyles_landing_sub_headline'];?></p>
                    <?php endif;?>
                    <?php if(!empty($page['lifestyles_landing_description'])):?>
                        <p class="page-description"><?php echo $page['lifestyles_landing_description'];?></p>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <div class="row multi-columns-row">
            <?php foreach($output['lifestylePost'] as $lifestyle):?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="lifestyle">
                        <?php if(!empty($lifestyle['thumbnail_url'])):?>
                            <div class="lifestyle-img">
                                <a href="<?php echo $lifestyle['permalink'];?>" title="<?php echo $lifestyle['post_title'];?>"><img src="<?php echo $lifestyle['thumbnail_url'];?>" alt="<?php echo $lifestyle['post_title'];?>"/></a>
                            </div>
                        <?php endif;?>
                        <div class="content">
                            <h3><a href="<?php echo $lifestyle['permalink'];?>" title="<?php echo $lifestyle['post_title'];?>"><?php echo $lifestyle['post_title'];?></a></h3>
                            <?php if(!empty($lifestyle['lifestyle_list'])):?>
                                <?php echo $lifestyle['lifestyle_list'];?>
                            <?php endif;?>
                        </div>
                        <p class="text-center"><a href="<?php echo $lifestyle['permalink'];?>" class="btn btn-default">Learn More</a></p>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>