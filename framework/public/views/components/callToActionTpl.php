<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="cta" class="swatch-a">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                    <h1><?php echo $output['options_call_to_action_title'];?></h1>
                    <p><?php echo $output['options_call_to_action_subheadline'];?></p>
                    <hr>
                    <a href="<?php echo MW_HOME;?>/request-an-appointment" title="Request an Appointment" class="btn btn-default cta-btn">Request an Appointment</a>
                </div>
            </div>
        </div>
    </div>
</section>