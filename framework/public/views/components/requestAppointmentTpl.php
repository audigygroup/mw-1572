<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<script>
    var $ra = <?php echo json_encode($output);?>;
</script>
<section id="request-appointment">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="loc-map">
                    <div id="map-canvas-ra"></div>
                    <div class="loc-menu">
                        <ul class="nav" role="tablist">
                            <?php foreach( $output['raLocation'] as $index => $loc):?>
                                <?php if(!empty($loc['latitude']) && !empty($loc['longitude'])):?>
                                    <li id="loc-item-<?php echo $loc['ID'];?>"><a href="#<?php echo $loc['ID'];?>"><?php echo $loc['location_title'];?></a></li>
                                <?php endif;?>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p class="ra-message">
                    <?php echo $output['raPage'][0]['request_an_apointment_message'];?>
                </p>
            </div>
            <div class="col-xs-12">
                <div class="ra-form">
                    <?php gravity_form( 3, true, false, false, '', false );?>
                </div>
            </div>
        </div>
    </div>
</section>