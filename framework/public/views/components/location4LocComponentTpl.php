<?php
//echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="multi-loc-4">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Contact <?php bloginfo('name'); ?></h1>
            </div>
            <?php foreach($output as $key => $loc):?>
                <?php if($key < 4 ):?>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="loc-info">
                            <div class="loc-title">
                                <h3><a href="<?php echo $loc['permalink'];?>" title="<?php echo $loc['location_title'];?>"><?php echo $loc['location_title'];?></a></h3>
                                <p><?php echo $loc['address'];?>, <br> <?php echo $loc['city'];?>, <?php echo $loc['state'];?> <?php echo $loc['zip_code'];?></p>
                            </div>
                            <ul class="list-unstyled">
                                <?php if( !empty( $loc['phone_number'] ) ):?>
                                    <li><i class="fa fa-phone"></i> <?php echo $loc['phone_number'];?></li>
                                <?php endif;?>
                                <?php if( !empty( $loc['email'] ) ):?>
                                    <li><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $loc['email'];?>"><?php echo $loc['email'];?></a></li>
                                <?php endif;?>
                                <?php $rows = get_field('operation', $loc['ID']); if(!empty($rows)):?>
                                    <li>
                                        <i class="fa fa-clock-o"></i> Hours of Operation:<br>
                                        <?php foreach($rows as $r):?>
                                            <time><?php echo $r['day_open'];?><?php if(!empty($r['day_close']) && !empty($r['symbol'])):?>&nbsp;<?php echo $loc['symbol'];?>&nbsp;<?php echo $r['day_close'];?><?php endif;?><?php if(!empty($r['time'])):?>:&nbsp;<?php echo $r['time'];?><br><?php endif;?></time>
                                        <?php endforeach;?>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </div>
                    </div>
                <?php endif;?>
            <?php endforeach;?>
        </div>
    </div>
</section>