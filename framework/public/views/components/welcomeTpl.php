<?php
//echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="welcome-provider">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-lg-3">
                <div class="text-center">
                    <?php foreach ($output['professional'] as $professional): ?>
                        <?php if ($professional['position'] == 'Owner'): ?>
                            <?php if( array_key_exists('thumbnail_url', $professional) && !empty($professional['thumbnail_url']) ): ?>
                                <img src="<?php echo $professional['thumbnail_url']; ?>" alt="<?php echo $professional['full_name'];?>" class="img-circle"/>
                            <?php else:?>
                                <img src="<?php echo SITE_ROOT ?>framework/assets/images/professional-fallback.jpg" alt="<?php echo $professional['full_name'];?>" class="img-circle"/>
                            <?php endif;?>
                            <h4 class="name"><?php echo $professional['full_name']; ?></h4>
                            <p class="caption"><?php echo $professional['title']; ?></p>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-8 col-lg-9">
                <div class="welcome-text">
                    <h1>Welcome to <?php bloginfo( 'name' ); ?></h1>
                    <?php if ( ! empty( $output['practice']['options_welcome_subheadline'] )): ?>
                        <p class="sub-headline"><?php echo $output['practice']['options_welcome_subheadline']; ?></p>
                    <?php endif; ?>
                    <?php if ( ! empty( $output['practice']['options_welcome_statement'] )): ?>
                        <p><?php echo $output['practice']['options_welcome_statement']; ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>