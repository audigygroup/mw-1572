<?php
//echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="multi-loc">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Contact <?php bloginfo('name'); ?></h1>
            </div>
            <?php foreach($output as $key => $loc):?>
                <?php if($key < 2 ):?>
                    <div class="col-xs-12 col-md-6">
                        <?php if( !empty($loc['latitude']) && !empty( $loc['longitude']) ):?>
                            <div class="single-loc">
                                <div id="map-canvas-<?php echo $key;?>" data-latitude="<?php echo $loc['latitude'];?>" data-longitude="<?php echo $loc['longitude'];?>" ></div>
                            </div>
                        <?php endif;?>
                        <div class="loc-info">
                            <div class="loc-title">
                                <h3><a href="<?php echo $loc['permalink'];?>" title="<?php echo $loc['location_title'];?>"><?php echo $loc['location_title'];?></a></h3>
                                <?php if( !empty( $loc['full_address'] ) ):?>
                                    <p><?php echo $loc['full_address'];?></p>
                                <?php else:?>
                                    <p><?php echo $loc['address'];?> &bull; <?php echo $loc['city'];?>, <?php echo $loc['state'];?> <?php echo $loc['zip_code'];?></p>
                                <?php endif;?>
                            </div>
                            <ul class="list-unstyled">
                                <?php if( !empty( $loc['phone_number'] ) ):?>
                                    <li><i class="fa fa-phone"></i> <?php echo $loc['phone_number'];?></li>
                                <?php endif;?>
                                <?php if( !empty( $loc['email'] ) ):?>
                                    <li><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $loc['email'];?>"><?php echo $loc['email'];?></a></li>
                                <?php endif;?>
                                <?php $rows = get_field('operation', $loc['ID']); if(!empty($rows)):?>
                                    <li>
                                        <?php foreach($rows as $index => $r):?>
                                            <time><?php echo $loc['operation_'. $index .'_day_open'];?><?php if(!empty($loc['operation_'. $index .'_day_close']) && !empty($loc['operation_'. $index .'_symbol'])):?>&nbsp;<?php echo $loc['operation_'. $index .'_symbol'];?>&nbsp;<?php echo $loc['operation_'. $index .'_day_close'];?><?php endif;?><?php if(!empty($loc['operation_'. $index .'_time'])):?>:&nbsp;<?php echo $loc['operation_'. $index .'_time'];?><br><?php endif;?></time>
                                        <?php endforeach;?>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </div>
                    </div>
                <?php endif;?>
            <?php endforeach;?>
        </div>
    </div>
</section>