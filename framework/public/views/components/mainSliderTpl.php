<?php
?>
<section>
	<div id="main-img-slider" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<?php foreach ( $output as $key => $slide ):

					/**
					 * Backword compatability to support static button text/link
					 * @TODO: messy, this is not the correct spot to due this
					 */
					if ( $slide['request_appointment_button'] == 1 ) {
						$slide['slider_button_text'] = ( ! empty( $slide['slider_button_text'] ) ) ? $slide['slider_button_text'] : 'Request an Appointment';
						$slide['slider_button_url']  = ( ! empty( $slide['slider_button_url'] ) ) ? $slide['slider_button_url'] : '/contact/request-an-appointment/';
					}
				?>
				<div class="item <?php echo ( empty( $key ) ) ? "active" : ''; ?>">
					<img src="<?php echo $slide['thumbnail_url']; ?>" alt="<?php echo $slide['alt']; ?>"/>

					<div class="carousel-caption hidden-xs hidden-sm">
						<h1><?php echo $slide['title']; ?></h1>
						<?php if ( ! empty( $slide['sub_headline'] ) ): ?>
							<p class="slide-sub-headline"><?php echo $slide['sub_headline']; ?></p>
						<?php endif; ?>
						<?php
						if ( $slide['request_appointment_button'] == 1 && ! empty( $slide['slider_button_text'] ) && ! empty( $slide['slider_button_url'] ) ): ?>
							<p class="ra-btn"><a href="<?php MW_HOME; ?><?php echo $slide['slider_button_url']; ?>" title="<?php echo $slide['slider_button_text']; ?>" class="btn btn-default"><?php echo $slide['slider_button_text']; ?></a></p>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if ( count( $output ) > 1 ): ?>
			<a class="left carousel-control" href="#main-img-slider" role="button" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="right carousel-control" href="#main-img-slider" role="button" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		<?php endif; ?>
	</div>
</section>
