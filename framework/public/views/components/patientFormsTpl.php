<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
    $page = $output['patientFormsPage'][0];
?>
<div id="patient-forms">
    <div class="row">
        <div class="col-xs-12">
            <?php if(! empty($page['patient_forms_headline'])):?>
                <p class="sub-headline"><?php echo $page['patient_forms_headline'];?></p>
            <?php endif;?>
            <p class="desc"><?php echo $page['patient_forms_description'];?></p>
        </div>
    </div>
    <div class="row">
        <?php foreach ($output['forms'] as $key => $form): ?>
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2">
                          <i class="mw mw-forms"></i>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <?php if ( ! empty( $form['form_title'] )): ?>
                                <p class="text-center form-title">
                                    <a href="<?php echo wp_get_attachment_url( $form['form_file'] ); ?>" title="<?php echo $form['form_title']; ?>" target="_blank"><?php echo $form['form_title']; ?></a>
                                </p>
                            <?php endif; ?>
                            <p class="text-center">
                                <a href="<?php echo wp_get_attachment_url( $form['form_file'] ); ?>" target="_blank" title="Download" class="btn btn-default">Download</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
