<?php
//    echo '<pre>'; print_r($output); echo'</pre>';
?>
<header>
    <div class="header-wrap swatch-c">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="main-logo">
                        <a href="<?php echo get_home_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
                            <img src="<?php echo wp_get_attachment_url( $output['headerPractice']['options_header_logo'] );?>" alt="<?php bloginfo( 'name' ); ?>">
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="member-info text-right">
                        <?php foreach($output['headerLocation'] as $key => $loc):?>
                            <h4 class="phone-number">
                                <?php if(! empty($loc['location_title'])):?>
                                    <span><?php echo $loc['location_title'];?></span>
                                <?php endif;?>
                                <?php if(! empty($loc['phone_number'])):?>
                                    <?php echo $loc['phone_number'];?>
                                    <i class="fa fa-phone"></i>
                                <?php endif;?>
                            </h4>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="nav-ex-1 navbar navbar-default swatc" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#header-collapse">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <?php
                wp_nav_menu( array(
                        'menu'            => 'primary',
                        'theme_location'  => 'primary',
                        'container'       => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id'    => 'header-collapse',
                        'menu_class'      => 'nav navbar-nav',
                        'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                        'walker'          => new wp_bootstrap_navwalker(),
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>' . get_search_form(false)
                    )
                );
            ?>
        </div>
    </nav>
</header>