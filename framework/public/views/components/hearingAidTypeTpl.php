<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="hearing-aid-options-v2">
    <div id="hearing-aid-carousel-v2" class="carousel slide" data-ride="carousel">
        <div class="carousel-header-wrap swatch-a-fifty">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1>Hearing Aid Options</h1>
                        <p class="sub-headline">There's a hearing solution that's right for you.</p>
                    </div>
                    <div class="col-xs-12 hidden-xs">
                        <ul class="carousel-indicators">
                            <?php foreach($output as $key => $hearingAid):?>
                                <li data-target="#hearing-aid-carousel-v2" data-slide-to="<?php echo $key;?>" <?php if (!$key) {echo 'class="active"';}; ?>><span class="hearing-aid-text"><?php echo $hearingAid['hearing_aid_type'];?></span></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-wrapper swatch-a-fifty">
            <div class="container contain-ctrl-arrow">
                <div class="carousel-inner">
                    <?php foreach($output as $index => $type):?>
                        <div class="item <?php echo (empty($index)) ? "active" : ''; ?> text-center">
                            <div class="col-xs-12 hidden-xs col-sm-4">
                                <?php echo wp_get_attachment_image( $type['image_left'], 'full', '',array('class' => 'img-circle', 'alt' => $type['hearing_aid_type'] ) ); ?>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <?php echo wp_get_attachment_image( $type['image_center'], 'full', '',array('alt' => $type['hearing_aid_type'] ) ); ?>
                            </div>
                            <div class="col-xs-12 hidden-xs col-sm-4">
                                <?php echo wp_get_attachment_image( $type['image_right'], 'full', '',array('class' => 'img-circle', 'alt' => $type['hearing_aid_type'] ) ); ?>
                            </div>
                            <div class="col-xs-12">
                                <?php if( !empty($type['hearing_aid_type']) ):?>
                                    <h1><?php echo $type['hearing_aid_type'];?></h1>
                                <?php endif;?>
                                <?php if( !empty($type['description_excerpt']) ):?>
                                    <p class="hearing-aid-desc"><?php echo $type['description_excerpt'];?></p>
                                <?php endif;?>
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="text-center">
                                        <div class="col-xs-12 col-md-4">
                                            <p><a href="<?php echo MW_HOME;?>/find-hearing-aids/lifestyles/" title="Choose Your Lifestyle" class="btn btn-default">Choose Your Lifestyle</a></p>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <p><a href="<?php echo $type['permalink'];?>" title="Learn More" class="btn btn-default">Learn More</a></p>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <p><a href="<?php echo MW_HOME;?>/request-an-appointment/" title="Request an Appointment" class="btn btn-default">Request an Appointment</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
                <a class="control-prev" href="#hearing-aid-carousel-v2" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="control-next" href="#hearing-aid-carousel-v2" role="button" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
</section>