<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="accessories">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-wrap">
                    <?php if(!empty($output['hearingAidAccessoriesPage'][0]['hearing_aid_accessories_heading'])):?>
                        <h2 class="text-center"><?php echo $output['hearingAidAccessoriesPage'][0]['hearing_aid_accessories_heading'];?></h2>
                    <?php endif;?>
                    <?php if(!empty($output['hearingAidAccessoriesPage'][0]['hearing_aid_accessories_sub_heading'])):?>
                        <p class="text-center"><?php echo $output['hearingAidAccessoriesPage'][0]['hearing_aid_accessories_sub_heading'];?></p>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <div class="row multi-columns-row">
            <?php foreach($output['hearingAidAccessoriesCat'] as $accessory):?>
                <?php $link = get_term_link( $accessory['slug'], 'accessories_cat' );?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="accessory-wrap">
                        <?php if(!empty($accessory['taxonomy_metadata']['taxonomy_image']['img'])):?>
                            <div class="img-wrap">
                                <a href="<?php echo esc_url($link);?>" title="<?php echo $accessory['name'];?>">
                                    <img src="<?php echo $accessory['taxonomy_metadata']['taxonomy_image']['img'];?>" alt="<?php echo $accessory['taxonomy_metadata']['taxonomy_image']['alt'];?>"/>
                                </a>
                            </div>
                        <?php endif;?>
                        <div class="excerpt">
                            <h3><a href="<?php echo esc_url($link);?>" title="<?php echo $accessory['name'];?>"><?php echo $accessory['name'];?></a></h3>
                            <?php if($accessory['description']):?>
                                <p class="desc-excerpt"><?php echo $accessory['description'];?></p>
                            <?php endif;?>
                            <p><a href="<?php echo esc_url($link);?>" title="Learn More" class="btn btn-default">Learn More</a></p>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>