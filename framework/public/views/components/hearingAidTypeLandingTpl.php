<?php
//echo '<pre>'; print_r($output); echo '</pre>';
$page = $output['hearingAidTypePage'][0];
?>
<section id="hearing-aid-type">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-wrap text-center">
                    <?php if( !empty($page['hearing_aid_type_title']) ):?>
                        <h2><?php echo $page['hearing_aid_type_title'];?></h2>
                    <?php endif;?>
                    <?php if( !empty($page['hearing_aid_type_sub_headline']) ):?>
                        <p class="sub-headline"><?php echo $page['hearing_aid_type_sub_headline'];?></p>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <div class="row multi-columns-row">
            <?php foreach($output['hearingAidType'] as $key => $hearingAid):?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="type-wrap">
                        <div class="featured-img-wrap">
                            <a href="<?php echo $hearingAid['permalink'];?>" title="<?php echo $hearingAid['hearing_aid_type'];?>">
                                <?php echo wp_get_attachment_image( $hearingAid['image_center'], 'full', '',array('alt' => $hearingAid['hearing_aid_type'] ) ); ?>
                            </a>
                        </div>
                        <div class="excerpt text-center">
                            <h3><a href="<?php echo $hearingAid['permalink'];?>" title="<?php echo $hearingAid['hearing_aid_type'];?>"><?php echo $hearingAid['hearing_aid_type'];?></a></h3>
                            <?php if( !empty($hearingAid['description_excerpt']) ):?>
                                <p class="desc-excerpt"><?php echo $hearingAid['description_excerpt'];?></p>
                            <?php endif;?>
                            <p><a href="<?php echo $hearingAid['permalink'];?>" class="btn btn-default" title="Learn More">Learn More</a></p>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>