<?php
$person = $output[0];
//echo "<pre>";  print_r($output); echo "</pre>";
?>
<section id="professional-details">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <div class="pro-details">
                    <div class="professional-img">
                        <?php if( array_key_exists('thumbnail_url', $person) && !empty($person['thumbnail_url']) ): ?>
                            <img src="<?php echo $person['thumbnail_url'];?>" alt="<?php echo $person['full_name'];?>" class="img-circle"/>
                        <?php else:?>
                            <img src="<?php echo SITE_ROOT ?>framework/assets/images/professional-fallback.jpg" alt="<?php echo $person['full_name'];?>" class="img-circle"/>
                        <?php endif;?>
                    </div>
                    <div class="professional-social">
                        <ul class="list-unstyled list-inline social-icons addthis_default_style text-center">
                            <?php if(!empty($person['facebook_id'])):?>
                                <li><a class="addthis_button_facebook_follow" addthis:userid="<?php echo $person['facebook_id'];?>"><i class="fa fa-facebook"></i></a></li>
                            <?php endif;?>
                            <?php if(!empty($person['twitter_id'])):?>
                                <li><a class="addthis_button_twitter_follow" addthis:userid="<?php echo $person['twitter_id'];?>"><i class="fa fa-twitter"></i></a></li>
                            <?php endif;?>
                            <?php if(!empty($person['linkedin_id'])):?>
                                <li><a class="addthis_button_linkedin_follow" addthis:usertype="company" addthis:userid="<?php echo $person['linkedin_id'];?>"><i class="fa fa-linkedin"></i></a></li>
                            <?php endif;?>
                            <?php if(!empty($person['email'])):?>
                                <li><a class="email pull-left" href="mailto:<?php echo $person['email'];?>?Subject=Hello%20<?php echo $person['full_name'];?>" target="_top"><i class="fa fa-envelope"></i></a></li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <?php if(!empty($person['phone_number'])):?>
                        <div class="phone-number">
                            <h2 class="text-center"><?php echo $person['phone_number'];?></h2>
                        </div>
                    <?php endif;?>
                    <?php
                    if($person['request_appointment_button'] == 1):?>
                        <div class="request-appointment">
                            <p class="text-center">
                                <a href="<?php echo MW_HOME;?>/request-an-appointment" title="Request an Appointment" class="btn btn-default">Request Appointment</a>
                            </p>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-xs-12 col-md-8">
                <div class="bio-wrap">
                    <div class="title">
                        <?php if(!empty($person['full_name'])):?>
                            <h2><?php echo $person['full_name'];?></h2>
                        <?php else: ?>
                            <h2><?php echo $person['first_name'];?> <?php echo $person['last_name'];?></h2>
                        <?php endif;?>
                        <?php if(!empty($person['bio'])):?>
                            <h3 class="sub-headline"><?php echo $person['title'];?></h3>
                        <?php endif;?>
                    </div>
                    <?php if(!empty($person['bio'])):?>
                        <p><?php echo $person['bio'];?></p>
                    <?php endif;?>
                </div>
                <?php if(!empty($person['awards_and_certifications'])):?>
                    <div class="awards-certs">
                        <h2>Awards &amp; Certifications</h2>
                        <?php echo $person['awards_and_certifications'];?>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>