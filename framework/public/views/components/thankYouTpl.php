<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="thank-you">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="message">
                    <?php if(!empty($output['options_thank_you_message'])):?>
                        <p class="sub-headline text-center"><?php echo $output['options_thank_you_message'];?></p>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="social-media">
                    <p class="sub-headline text-center">Follow us to stay up-to-date on the latest information from <?php bloginfo( 'name' ); ?>.</p>
                    <ul class="list-unstyled list-inline social-icons addthis_default_style text-center">
                        <?php if( !empty( $output['options_facebook_id'] ) ):?>
                            <li><a class="addthis_button_facebook_follow" addthis:userid="<?php echo $output['options_facebook_id']; ?>"><span>Facebook</span><i class="fa fa-facebook"></i></a></li>
                        <?php endif; ?>
                        <?php if( !empty( $output['options_twitter_id'] ) ):?>
                            <li><a class="addthis_button_twitter_follow" addthis:userid="<?php echo $output['options_twitter_id']; ?>"><span>Twitter</span><i class="fa fa-twitter"></i></a></li>
                        <?php endif; ?>
                        <?php if( !empty( $output['options_youtube_id'] ) ):?>
                            <li><a class="addthis_button_youtube_follow" addthis:userid="<?php echo $output['options_youtube_id']; ?>"><span>Youtube</span><i class="fa fa-youtube"></i></a></li>
                        <?php endif; ?>
                        <?php if( !empty( $output['options_google_plus_id'] ) ):?>
                            <li><a class="addthis_button_google_follow" addthis:userid="<?php echo $output['options_google_plus_id']; ?>"><span>Google+</span><i class="fa fa-google-plus"></i></a></li>
                        <?php endif; ?>
                        <?php if( !empty( $output['options_linkedin_id'] ) ):?>
                            <li><a class="addthis_button_linkedin_follow" addthis:usertype="company" addthis:userid="<?php echo $output['options_linkedin_id']; ?>"><span>Linkedin</span><i class="fa fa-linkedin"></i></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>