<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<footer>
    <div id="copyright" class="swatch-b-forty">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="text-center list-unstyled list-inline">
                        <li>Copyright &copy; <?php echo date("Y") ?>  <?php bloginfo( 'name' ); ?></li>
                        <li>|</li>
                        <li>Powered by <a href="<?php echo $output['options_powered_by_link'];?>" target="_blank" title="<?php echo $output['options_powered_by'];?>"><?php echo $output['options_powered_by'];?></a></li>
                        <li>|</li>
                        <li><a href="/sitemap" title="Sitemap">Sitemap</a></li>
                        <li>|</li>
                        <li><a href="/hipaa-statement" title="HIPAA Statement">HIPAA Statement</a></li>
                        <li>|</li>
                        <li><a href="/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
