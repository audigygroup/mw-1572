<?php
//echo '<pre>'; print_r($output); echo '</pre>';
?>
<script>
    var $loc = <?php echo json_encode($output);?>;
</script>
<section id="location-map">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Contact <?php bloginfo('name'); ?></h1>
            </div>
            <div class="col-xs-12">
                <div class="loc-map">
                    <div id="map-canvas-loc"></div>
                    <div class="location-info-tabs" role="tabpanel">
                        <ul class="loc-menu nav nav-tabs" role="tablist">
                            <li class="dropdown" role="presentation">
                                <a href="#" class="select dropdown-toggle" id="loc-dropdown" data-toggle="dropdown" aria-controls="location-names">
                                    Select Location
                                    <span class="caret"></span>
                                </a>
                                <ul id="location-names" class="dropdown-menu" aria-labelledby="loc-dropdown" role="menu">
                                    <?php foreach($output as $k => $loc):?>
                                        <li role="presentation" id="loc-<?php echo $loc['ID'];?>">
                                            <a href="#<?php echo $loc['post_name'];?>" aria-controls="<?php echo $loc['post_name'];?>" tabindex="-1" role="tab" data-toggle="tab" id="dropdown-<?php echo $loc['post_name'];?>"><?php echo $loc['post_title']?></a>
                                        </li>
                                    <?php endforeach;?>
                                </ul>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <?php foreach($output as $key => $location):?>
                                <div id="<?php echo $location['post_name'];?>" role="tabpanel" class="loc-info tab-pane" aria-labelledby="dropdown-<?php echo $loc['post_name'];?>">
                                    <div class="loc-title">
                                        <h3><a href="<?php echo $location['permalink'];?>" title="title"><?php echo $location['post_title'];?></a></h3>
                                        <p><?php echo $location['address'];?>, <br> <?php echo $location['city'];?>, <?php echo $location['state'];?> <?php echo $location['zip_code'];?></p>
                                    </div>
                                    <ul class="list-unstyled">
                                        <?php if( !empty( $location['phone_number'] ) ):?>
                                            <li><i class="fa fa-phone"></i> <?php echo $location['phone_number'];?></li>
                                        <?php endif;?>
                                        <?php if( !empty( $location['email'] ) ):?>
                                            <li><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $location['email'];?>"><?php echo $location['email'];?></a></li>
                                        <?php endif;?>
                                        <?php $rows = get_field('operation', $location['ID']); if(!empty($rows)):?>
                                            <li>
                                                <i class="fa fa-clock-o"></i> Hours of Operation:<br>
                                                <?php foreach($rows as $r):?>
                                                    <time><?php echo $r['day_open'];?><?php if(!empty($r['day_close']) && !empty($r['symbol'])):?>&nbsp;<?php echo $r['symbol'];?>&nbsp;<?php echo $r['day_close'];?><?php endif;?><?php if(!empty($r['time'])):?>:&nbsp;<?php echo $r['time'];?><br><?php endif;?></time>
                                                <?php endforeach;?>
                                            </li>
                                        <?php endif;?>
                                    </ul>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>