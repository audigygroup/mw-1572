<?php
//echo '<pre>'; print_r($output); echo '</pre>';
?>
<div id="top-bar" class="swatch-b-forty">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <a class="audigy-cert-logo" href="https://www.agxhearing.com/the-agx-difference/" title="This provider is Audigy Certified" target="_blank">
                    <img src="<?php echo MW_IMAGES ?>/AG-certified-top.png" alt="Audigy Certified Logo">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?php if ( file_exists(MW_VIEW . 'helpers/follow-addthis.php') ):?>
                    <ul class="list-unstyled list-inline text-right social-icons addthis_toolbox addthis_32x32_style addthis_default_style">
                        <?php include( MW_VIEW . 'helpers/follow-addthis.php' );?>
                    </ul>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>