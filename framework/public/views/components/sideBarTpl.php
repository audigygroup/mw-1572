<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<div id="side-bar">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-12">
            <div class="loc-info">
                <?php foreach($output['location'] as $index => $loc):?>
                    <div class="single-loc">
                        <h4><a href="<?php echo $loc['permalink'];?>" title="<?php echo $loc['location_title'];?>"><?php echo $loc['location_title'];?></a></h4>
                        <p>
                            <?php echo $loc['address'];?><br>
                            <?php echo $loc['city'];?>, <?php echo $loc['state'];?> <?php echo $loc['zip_code'];?>
                        </p>
                        <?php if(! empty($loc['phone_number'])):?>
                            <p class="h5"><?php echo $loc['phone_number'];?></p>
                        <?php endif;?>
                        <?php if(! empty($loc['email'])):?>
                            <p class="email"><a href="mailto:<?php echo $loc['email'];?>"><?php echo $loc['email'];?></a></p>
                        <?php endif;?>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-12">
            <div class="loc-hours">
                <h4>Office Hours</h4>
                <?php foreach($output['location'] as $index => $loc):?>
                    <h5><?php echo $loc['location_title'];?></h5>
                    <?php $rows = get_field('operation', $loc['ID']); if(!empty($rows)):?>
                        <p>
                            <?php foreach($rows as $key => $r):?>
                                <?php echo $r['day_open'];?><?php if(!empty($r['day_close']) && !empty($r['symbol'])):?>&nbsp;<?php echo $r['symbol'];?>&nbsp;<?php echo $r['day_close'];?><?php endif;?><?php if(!empty($r['time'])):?>:&nbsp;<?php echo $r['time'];?><br><?php endif;?>
                            <?php endforeach;?>
                        </p>
                    <?php endif;?>
                <?php endforeach;?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-12">
            <div class="follow-us">
                <div class="title-wrap">
                    <h4>Follow Us!</h4>
                </div>
                <div class="social-wrap">
                    <ul class="list-unstyled list-inline social-icons addthis_default_style">
                        <?php if( !empty( $output['practice']['options_facebook_id'] ) ):?>
                            <li><a class="addthis_button_facebook_follow" addthis:userid="<?php echo $output['practice']['options_facebook_id']; ?>"><i class="fa fa-facebook"></i></a></li>
                        <?php endif; ?>
                        <?php if( !empty( $output['practice']['options_twitter_id'] ) ):?>
                            <li><a class="addthis_button_twitter_follow" addthis:userid="<?php echo $output['practice']['options_twitter_id']; ?>"><i class="fa fa-twitter"></i></a></li>
                        <?php endif; ?>
                        <?php if( !empty( $output['practice']['options_youtube_id'] ) ):?>
                            <li><a class="addthis_button_youtube_follow" addthis:userid="<?php echo $output['practice']['options_youtube_id']; ?>"><i class="fa fa-youtube"></i></a></li>
                        <?php endif; ?>
                        <?php if( !empty( $output['practice']['options_google_plus_id'] ) ):?>
                            <li><a class="addthis_button_google_follow" addthis:userid="<?php echo $output['practice']['options_google_plus_id']; ?>"><i class="fa fa-google-plus"></i></a></li>
                        <?php endif; ?>
                        <?php if( !empty( $output['practice']['options_linkedin_id'] ) ):?>
                            <li><a class="addthis_button_linkedin_follow" addthis:usertype="company" addthis:userid="<?php echo $output['practice']['options_linkedin_id']; ?>"><i class="fa fa-linkedin"></i></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php if(!is_page('our-blog') && !is_singular('post') && !is_category() && !is_tag() ):?>
            <div class="col-xs-12">
                <div class="form-wrap">
                    <?php gravity_form( 1, true, false, false, '', false );?>
                </div>
            </div>
        <?php endif;?>
        <?php if(is_page_template('blog.php') || is_singular('post') || is_category() || is_tag() || is_archive() ):?>
            <div class="col-xs-12">
                <div class="recent-posts">
                    <h4>Recent Posts</h4>
                    <?php
                        $output['blog'] = array_slice($output['blog'], 0, 3);

                        foreach($output['blog'] as $post):
					?>
                        <div class="post">
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <?php if(!empty($post['thumbnail_url'])):?>
                                        <a href="<?php echo $post['permalink'];?>" title="<?php echo $post['post_title'];?>">
                                            <img src="<?php echo $post['thumbnail_url'];?>" alt="<?php echo $post['post_title'];?>"/></a>
                                        </a>
                                    <?php else: ?>
                                        <a href="<?php echo $post['permalink'];?>" title="<?php echo $post['post_title'];?>">
                                            <img src="<?php echo MW_IMAGES;?>/blog_fallback_img.jpg" alt="<?php echo $post['post_title'] ;?>"/>
                                        </a>
                                    <?php endif ;?>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <h5><a href="<?php echo $post['permalink'];?>" title="<?php echo $post['post_title'];?>"><?php echo $post['post_title'];?></a></h5>
                                    <p><i class="fa fa-clock-o"></i> <?php echo get_the_date('F j, Y', $post['ID']); ?></p>
                                    <p><i class="fa fa-user"></i> By: <?php  $user_info = get_userdata($post['post_author']); echo $user_info->display_name;?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="blog-archive-cats">
                    <div class="title">
                        <h4>Blog Archive</h4>
                    </div>
                    <div class="list">
                        <ul class="list-unstyled">
                            <?php wp_get_archives(array('type' => 'monthly','show_post_count' => true)); ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="blog-archive-cats">
                    <div class="title">
                        <h4>Categories</h4>
                    </div>
                    <div class="list">
                        <?php
                        $queried_object = get_queried_object();
                        $term_id = $queried_object->term_id;
                        $args = array(
                            'order'      => 'ASC',
                            'show_count' => 1,
                            'title_li'   => __( '' ),
                            'current_category'   => $term_id,
                        );
                        ?>
                        <ul class="list-unstyled">
                            <?php echo wp_list_categories($args);?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="tag-cloud">
                    <h4>Tags</h4>
                    <?php
                    $args = array(
                        'smallest' => 14,
                        'largest'  => 14,
                        'unit'     => 'px',
                    );
                    wp_tag_cloud( $args );
                    ?>
                </div>
            </div>
        <?php endif;?>
    </div>
</div>