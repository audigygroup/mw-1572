<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="blog-landing">
    <div class="post-wrap">
        <div class="row">
            <?php foreach($output as $key => $post):?>
                <div class="col-xs-12">
                    <div class="post">
                        <div class="image">
                            <a href="<?php echo $post['permalink'];?>" title="<?php echo $post['post_title'] ;?>">
                                <?php if(!empty($post['thumbnail_url'])):?>
                                    <img src="<?php echo $post['thumbnail_url'] ;?>" alt="<?php echo $post['post_title'] ;?>"/>
                                <?php else:?>
                                    <img src="<?php echo MW_IMAGES;?>/blog_fallback_img.jpg" alt="<?php echo $post['post_title'] ;?>"/>
                                <?php endif;?>
                                <div class="date-fold hidden-xs">
                                    <div class="date-wrap">
                                        <img src="<?php echo MW_IMAGES ?>/date-fold.png" alt="Date"/>
                                        <div class="date">
                                            <p class="month"><?php echo get_the_date('M', $post['ID']); ?></p>
                                            <p class="day"><?php echo get_the_date('d', $post['ID']); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="content-wrap">
                            <h2 class="title"><a href="<?php echo $post['permalink'];?>" title="<?php echo $post['post_title'] ;?>"><?php echo $post['post_title'] ;?></a></h2>
                            <div class="post-info">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <p class="date"><?php echo get_the_date('F j, Y', $post['ID']); ?></p>
                                    </div>
                                    <div class="col-xs-12 col-md-8">
                                        <ul class="specs list-unstyled list-inline">
                                            <li><i class="fa fa-user"></i> Posted By: <?php  $user_info = get_userdata($post['post_author']); echo $user_info->display_name;?></li>
                                            <?php $cats = wp_get_post_categories( $post['ID'] ); if($cats): ?>
                                                <li><i class="fa fa-folder-open"></i> Categories:
                                                    <?php
                                                        foreach($cats as $cat){
                                                            $c = get_category( $cat );
                                                            echo $c->name . ' ';
                                                        }
                                                    ?>
                                                </li>
                                            <?php endif;?>
                                            <?php $tags = get_the_tags($post['ID']); if($tags):?>
                                                <li><i class="fa fa-tags"></i> Tags:
                                                    <?php
                                                        $tags = get_the_tags($post['ID']);
                                                        foreach($tags as $tag) {
                                                            echo $tag->name . ' ';
                                                        }
                                                    ?>
                                                </li>
                                            <?php endif;?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php if(!empty($post['post_excerpt'])):?>
                                    <div class="col-xs-12">
                                        <p class="excerpt"><?php echo $post['post_excerpt'];?></p>
                                    </div>
                                <?php endif;?>
                                <div class="col-xs-12">
                                    <p><a href="<?php echo $post['permalink'];?>" title="Read More" class="btn btn-default">Read More</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php //pagination goes here?>
        </div>
    </div>
</section>