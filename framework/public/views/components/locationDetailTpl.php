<?php
/*
 * @TODO REFACTOR
 */
//DO NOT REMOVE THIS LINE
global $post;
$loc = $output['locationPost'];
foreach($loc as $k => $v){
    if($v['ID'] = $post->ID){
        $loc = $v;
        continue;
    }
}
//echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="location-detail">
    <div class="container">
        <?php if(!empty($loc['latitude']) && $loc['longitude']):?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="loc-map">
                        <div id="map-canvas-0" data-latitude="<?php echo $loc['latitude'];?>" data-longitude="<?php echo $loc['longitude'];?>" ></div>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <div class="details-section">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="location-info">
                        <h2><?php echo $loc['city'];?></h2>
                        <ul class="loc-info list-unstyled">
                            <li><i class="fa fa-location-arrow"></i> <?php echo $loc['address'];?> &bull; <?php echo $loc['city'];?>, <?php echo $loc['state'];?> <?php echo $loc['zip_code'];?></li>
                            <?php if(!empty($loc['phone_number'])):?>
                                <li><i class="fa fa-phone"></i> <?php echo $loc['phone_number'];?></li>
                            <?php endif;?>
                            <?php if(!empty($loc['email'])):?>
                                <li><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $loc['email'];?>"><?php echo $loc['email'];?></a></li>
                            <?php endif;?>
                            <?php
                                $rows = get_field('operation');
                                if(!empty($rows)):
                            ?>
                                <li>
                                    <i class="fa fa-clock-o"></i> Hours of Operation:<br>
                                    <?php foreach($rows as $r):?>
                                        <time><?php echo $r['day_open'];?><?php if(!empty($r['day_close']) && !empty($r['symbol'])):?>&nbsp;<?php echo $r['symbol'];?>&nbsp;<?php echo $r['day_close'];?><?php endif;?><?php if(!empty($r['time'])):?>:&nbsp;<?php echo $r['time'];?><br><?php endif;?></time>
                                    <?php endforeach;?>
                                </li>
                            <?php endif;?>
                        </ul>
                        <p class="review-title">Read Our <?php echo $loc['city'];?> Location's Reviews</p>
                        <ul class="review-links list-unstyled list-inline">
                            <?php if(!empty($loc['review_us_facebook'])):?>
                                <li><a href="<?php echo $loc['review_us_facebook'];?>" title="Review Us on Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <?php endif;?>
                            <?php if(!empty($loc['review_us_google_plus'])):?>
                                <li><a href="<?php echo $loc['review_us_google_plus'];?>" title="Review Us on Google+" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                            <?php endif;?>
                            <?php if(!empty($loc['review_us_yelp'])):?>
                                <li><a href="<?php echo $loc['review_us_yelp'];?>" title="Review Us on Yelp" target="_blank"><i class="fa fa-yelp"></i></a></li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <?php if(!empty($loc['about'])):?>
                        <div class="about-loc">
                            <h2>About Our <?php echo $loc['city'];?>, <?php echo $loc['state'];?> Location</h2>
                            <p><?php echo $loc['about'];?></p>
                        </div>
                    <?php endif;?>
                    <?php if(!empty($output['locationAccordionPost'])):?>
                        <div class="accordion">
                            <h2>Our Practice Specializes In:</h2>
                            <div class="panel-group" id="specializes-accordion" role="tablist" aria-multiselectable="true">
                                <?php foreach($output['locationAccordionPost'] as $key => $special):?>
                                    <?php if(!empty($special['question']) && !empty($special['answer'])):?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading-<?php echo $key;?>">
                                                <h3 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#specializes-accordion" href="#collapse-<?php echo $key;?>" aria-expanded="<?php if($key == 0):?>true <?php else :?>false<?php endif;?>" aria-controls="collapse-<?php echo $key;?>" <?php if($key !== 0):?>class="collapsed"<?php endif;?>>
                                                        <span class="expand-box plus-minus"></span><?php echo $special['question'];?>
                                                    </a>
                                                </h3>
                                            </div>
                                            <div id="collapse-<?php echo $key;?>" class="panel-collapse collapse <?php if($key == 0):?>in<?php endif;?>" role="tabpanel" aria-labelledby="heading-<?php echo $key;?>">
                                                <div class="panel-body">
                                                    <?php echo $special['answer'];?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </div>
                        </div>
                    <?php endif;?>
                    <?php if(!empty($output['locationProfessionalPost'])):?>
                        <div class="professionals">
                            <h2>Our <?php echo $loc['city'];?> Hearing Care Professionals</h2>
                            <div class="row multi-columns-row">
                                <?php foreach($output['locationProfessionalPost'] as $key => $pro):?>
                                    <?php
                                        $professionalAssociation = unserialize($pro['location_association']);
                                        if(!empty($professionalAssociation) && in_array($loc['ID'], $professionalAssociation) ):
                                    ?>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                            <div class="provider-img">
                                                <a href="<?php echo $pro['permalink']; ?>" title="<?php echo $pro['full_name'];?>">
                                                    <?php if( array_key_exists('thumbnail_url', $pro) && !empty($pro['thumbnail_url']) ): ?>
                                                        <img src="<?php echo $pro['thumbnail_url'];?>" alt="<?php echo $pro['full_name'];?>" class="img-circle"/>
                                                    <?php else:?>
                                                        <img src="<?php echo SITE_ROOT ?>framework/assets/images/professional-fallback.jpg" alt="<?php echo $pro['full_name'];?>" class="img-circle"/>
                                                    <?php endif;?>
                                                </a>
                                            </div>
                                            <h5 class="name text-center"><a href="<?php echo $pro['permalink']; ?>" title="<?php echo $pro['full_name'];?>"><?php echo $pro['full_name'];?></a></h5>
                                            <p class="title text-center"><?php echo $pro['title'];?></p>
                                        </div>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="contact-us-form">
                        <?php gravity_form( 2, true, false, false, array('loc_name' => $loc['post_title']), false );?>
                    </div>
                </div>
            </div>
        </div>
        <?php if(!empty($loc['award_1']) || !empty($loc['award_2']) || !empty($loc['award_3']) || !empty($loc['award_4'])):?>
            <div class="col-xs-12">
                <div class="awards">
                    <div class="row">
                        <?php
                        $awards = array(
                            'award_1',
                            'award_2',
                            'award_3',
                            'award_4'
                        );
                        foreach($awards as $award): ?>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <?php echo wp_get_attachment_image( $loc[$award], array( '250', '250' ), '', array( 'class' => 'award-img', 'alt' => 'Award' ) ); ?>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        <?php endif;?>
    </div>
</section>