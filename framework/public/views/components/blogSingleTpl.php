<?php
    global $post;
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
    <div class="img-wrap">
        <?php if(get_post_thumbnail_id($post->ID)):?>
            <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" alt="<?php echo $post->post_title;?>"/>
        <?php else:?>
            <img src="<?php echo MW_IMAGES;?>/blog_fallback_img.jpg" alt="<?php echo $post->post_type ;?>"/>
        <?php endif;?>
        <div class="date-fold hidden-xs">
            <div class="date-wrap">
                <img src="<?php echo MW_IMAGES ?>/date-fold.png" alt="Date"/>
                <div class="date">
                    <p class="month"><?php echo get_the_date('M', $post->ID); ?></p>
                    <p class="day"><?php echo get_the_date('d', $post->ID); ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrap">
        <div class="title">
            <h2><?php the_title();?></h2>
        </div>
        <div class="post-info">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <p class="date"><?php echo get_the_date('F j, Y', $post->ID); ?></p>
                </div>
                <div class="col-xs-12 col-md-8">
                    <ul class="specs list-unstyled list-inline">
                        <li><i class="fa fa-user"></i> Posted By: <?php  $user_info = get_userdata($post->post_author); echo $user_info->display_name;?></li>
                        <?php $cats = wp_get_post_categories( $post->ID ); if($cats): ?>
                            <li><i class="fa fa-folder-open"></i> Categories:
                                <?php
                                foreach($cats as $cat){
                                    $c = get_category( $cat );
                                    echo $c->name . ' ';
                                }
                                ?>
                            </li>
                        <?php endif;?>
                        <?php $tags = get_the_tags($post->ID); if($tags):?>
                            <li><i class="fa fa-tags"></i> Tags:
                                <?php
                                $tags = get_the_tags($post->ID);
                                foreach($tags as $tag) {
                                    echo $tag->name . ' ';
                                }
                                ?>
                            </li>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="content">
                    <?php the_content();?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-3">
                <p class="share-title">Share This Post:</p>
            </div>
            <div class="col-xs-12 col-md-8 col-lg-9">
                <div class="share">
                    <ul class="list-unstyled list-inline addthis_toolbox addthis_default_style">
                        <li><a class="addthis_button_facebook" addthis:title="<?php echo $post->post_title;?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="addthis_button_twitter" addthis:title="<?php echo $post->post_title;?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="addthis_button_google_plusone_share" addthis:title="<?php echo $post->post_title;?>"><i class="fa fa-google-plus"></i></a></li>
                        <li><a class="addthis_button_email" addthis:title="<?php echo $post->post_title;?>"><i class="fa fa-envelope"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="related-posts">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="related-heading text-center">Related Posts</h2>
                </div>
                <?php foreach($output as $key => $relatedPost):?>
                    <div class="col-xs-12 col-lg-4">
                        <div class="post">
                            <div class="related-img">
                                <?php if(!empty($relatedPost['thumbnail_url'])):?>
                                    <a href="<?php echo $relatedPost['permalink'];?>" title="<?php echo $relatedPost['post_title'];?>">
                                        <img src="<?php echo $relatedPost['thumbnail_url'];?>" alt="<?php echo $relatedPost->post_type ;?>"/>
                                    </a>
                                <?php else:?>
                                    <a href="<?php echo $relatedPost['permalink'];?>" title="<?php echo $relatedPost['post_title'];?>">
                                        <img src="<?php echo MW_IMAGES;?>/blog_fallback_img.png" alt="<?php echo $relatedPost['post_title'] ;?>"/>
                                    </a>
                                <?php endif;?>
                            </div>
                            <p class="related-title text-center">
                                <a href="<?php echo $relatedPost['permalink'];?>" title="<?php echo $relatedPost['post_title'];?>"><?php echo $relatedPost['post_title'];?></a>
                            </p>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
<?php endwhile; endif;?>