<?php
    $event = $output['event'][0];
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<div id="events-and-seminar-single">
    <div class="row">
        <div class="col-xs-12">
            <div class="img-wrap">
                <?php if(!empty($event['thumbnail_url'])):?>
                    <img src="<?php echo $event['thumbnail_url']; ?>" alt="<?php echo $event['post_title'];?>"/>
                <?php else:?>
                    <img src="<?php echo MW_IMAGES;?>/blog_fallback_img.jpg" alt="<?php echo $event['post_title'];?>"/>
                <?php endif;?>
                <div class="date-fold hidden-xs">
                    <div class="date-wrap">
                        <img src="<?php echo MW_IMAGES ?>/date-fold.png" alt="Date"/>
                        <div class="date">
                            <p class="month">
                                <?php
                                $dateFormat = "%B";
                                $date = strtotime($event['event_date']);
                                echo strftime($dateFormat, $date);
                                ?>
                            </p>
                            <p class="day">
                                <?php
                                $dateFormat = "%d";
                                echo strftime($dateFormat, $date);
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="title">
                <h2><?php echo $event['post_title'];?></h2>
                <p class="date">
                    <?php
                    $dateFormat = "%B %d, %Y";
                    echo strftime($dateFormat, $date);
                    ?> &bull; <?php echo $event['event_time'];?>
                </p>
            </div>
            <div class="desc">
                <p><?php echo $event['event_description']; ?></p>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="rsvp-form">
                <h3>RSVP Today!</h3>
                <?php if(!empty($event['rsvp_number'])):?>
                    <p>Call <?php echo $event['rsvp_number'];?> or fill out the form below</p>
                <?php endif;?>
                <?php gravity_form( 4, true, false, false, array('event_name' => $event['post_title']), false );?>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="location">
                <h3><?php echo $event['location_name'];?></h3>
                <p><?php echo $event['location_address'];?></p>
                <div class="loc-map">
                    <div id="map-canvas-0" data-latitude="<?php echo $event['location_latitude'];?>" data-longitude="<?php echo $event['location_longitude'];?>"></div>
                </div>
            </div>
        </div>
        <?php if(!empty($output['presenter'])):?>
            <div class="col-xs-12">
                <div class="presenters">
                    <h4 class="text-center">Presenters</h4>
                    <div class="row"></div>
                    <?php foreach($output['presenter'] as $presenter):?>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="provider-img">
                                <a href="<?php echo $presenter['permalink']; ?>" title="<?php echo $presenter['full_name'];?>">
                                    <?php if( !empty($presenter['thumbnail_url']) ): ?>
                                        <img src="<?php echo $presenter['thumbnail_url'];?>" alt="<?php echo $presenter['full_name'];?>" class="img-circle"/>
                                    <?php else:?>
                                        <img src="<?php echo SITE_ROOT ?>framework/assets/images/professional-fallback.jpg" alt="<?php echo $presenter['full_name'];?>" class="img-circle"/>
                                    <?php endif;?>
                                </a>
                            </div>
                            <h5 class="name text-center"><a href="<?php echo $presenter['permalink']; ?>" title="<?php echo $presenter['full_name'];?>"><?php echo $presenter['full_name'];?></a></h5>
                            <p class="title text-center"><?php echo $presenter['title'];?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif;?>
    </div>
</div>