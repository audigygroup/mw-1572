<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<section id="testimonials-v2" class="swatch-c">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Testimonials</h1>
                <p class="text-center sub-headline">See What People are saying about  <?php bloginfo( 'name' ); ?></p>
            </div>
            <div class="col-xs-12">
                <div id="testimonial-v2-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php foreach($output as $index => $testimonial): ?>
                            <div class="item <?php echo (empty($index)) ? "active" : ''; ?>">
                                <div class="row">
                                    <?php if(!empty($testimonial['thumbnail_url'])):?>
                                        <div class="col-xs-12 col-md-3 text-center">
                                            <img src="<?php echo $testimonial['thumbnail_url'];?>" alt="<?php echo $testimonial['patient_name'];?>" class="img-circle swatch-b-border"/>
                                        </div>
                                    <?php endif ;?>
                                    <div class="col-xs-12 <?php if(!empty($testimonial['thumbnail_url'])):?>col-md-9<?php endif ;?>">
                                        <?php if(!empty($testimonial['testimonial_quote'])):?>
                                            <p><?php echo $testimonial['testimonial_quote'];?></p>
                                        <?php endif ;?>
                                        <h4>
                                            <?php if(!empty($testimonial['patient_name'])):?>
                                                <b><?php echo $testimonial['patient_name'];?></b>
                                            <?php endif ;?>
                                            <?php if(!empty($testimonial['patient_since'])):?>
                                                <span><?php echo $testimonial['patient_since'];?></span>
                                            <?php endif ;?>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <a class="pull-left control-prev swatch-b" href="#testimonial-v2-carousel" role="button" data-slide="prev" title="Previous">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="pull-right control-next swatch-b" href="#testimonial-v2-carousel" role="button" data-slide="next" title="Next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>