<?php
//    echo '<pre>'; print_r($output); echo '</pre>';
?>
<nav class="single-nav">
    <div class="container">
        <ul class="nav nav-tabs">
            <?php foreach($output as $key => $catName):?>
                <?php $link = get_term_link( $catName['slug'], 'accessories_cat' );?>
                <li role="presentation" <?php if (is_tax('accessories_cat', $catName['term_id'])){echo 'class="active"';}; ?>>
                    <a href="<?php echo $link;?>" title="<?php echo $catName['name'];?>" class="accessory"><?php echo $catName['name'];?></a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
</nav>