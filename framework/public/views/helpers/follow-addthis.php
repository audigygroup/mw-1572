<?php if( !empty( $output['options_facebook_id'] ) ):?>
    <li><a class="addthis_button_facebook_follow" addthis:userid="<?php echo $output['options_facebook_id']; ?>"><i class="fa fa-facebook"></i></a></li>
<?php endif; ?>
<?php if( !empty( $output['options_twitter_id'] ) ):?>
    <li><a class="addthis_button_twitter_follow" addthis:userid="<?php echo $output['options_twitter_id']; ?>"><i class="fa fa-twitter"></i></a></li>
<?php endif; ?>
<?php if( !empty( $output['options_youtube_id'] ) ):?>
    <li><a class="addthis_button_youtube_follow" addthis:userid="<?php echo $output['options_youtube_id']; ?>"><i class="fa fa-youtube"></i></a></li>
<?php endif; ?>
<?php if( !empty( $output['options_google_plus_id'] ) ):?>
    <li><a class="addthis_button_google_follow" addthis:userid="<?php echo $output['options_google_plus_id']; ?>"><i class="fa fa-google-plus"></i></a></li>
<?php endif; ?>
<?php if( !empty( $output['options_linkedin_id'] ) ):?>
    <li><a class="addthis_button_linkedin_follow" addthis:usertype="company" addthis:userid="<?php echo $output['options_linkedin_id']; ?>"><i class="fa fa-linkedin"></i></a></li>
<?php endif; ?>