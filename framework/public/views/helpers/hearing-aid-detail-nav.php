<?php
//echo '<pre>'; print_r($output); echo '</pre>';
?>
<nav class="single-nav">
    <div class="container">
        <ul class="nav nav-tabs">
            <?php foreach($output as $key => $aid):?>
                <li role="presentation" <?php if (is_single($aid['ID'])){echo 'class="active"';}; ?>>
                    <a href="<?php echo $aid['permalink'];?>" title="<?php echo $aid['post_title'];?>"><?php echo $aid['post_title'];?></a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
</nav>