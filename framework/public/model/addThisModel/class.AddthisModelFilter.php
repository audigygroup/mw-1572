<?php

class AddthisModelFilter extends WpOptionDataFilterBase
{

    private static $instance;

    private function __construct()
    {
        //Helper Class PracticePostData
        $this->model =  PracticePostData::getInstance();
        $this->requiredData = array(
            'options_facebook_id',
            'options_twitter_id',
            'options_youtube_id',
            'options_google_plus_id',
            'options_linkedin_id'
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}