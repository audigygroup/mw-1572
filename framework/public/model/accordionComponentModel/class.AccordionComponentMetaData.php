<?php

class AccordionComponentMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $contentObj = HearingAidTypeDetailModelFilter::getInstance();
        $postId = AccordionComponentPostData::getInstance($contentObj);
        $this->postObject = $postId->getPosts();
    }
}