<?php

class AccordionComponentFactory
{
    protected $contentObj;

    public function createModel($contentObj)
    {
        $this->contentObj = $contentObj;

        $postObj = AccordionComponentPostData::getInstance($contentObj);
        $postData = $postObj->getPosts();

        $postMetaObj = new AccordionComponentMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $hearingAidTypeModel = new AccordionComponentModel($postData, $metaData);

        return $hearingAidTypeModel;
    }
}