<?php

class AccordionComponentPostData extends GetPosts
{
    private static $instance;
    protected $getDetailObj;

    public function __construct($getDetailObj)
    {
        $this->getDetailObj = $getDetailObj;

        $this->args = array(
            'post_type' => 'accordion',
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'accordion_cat', //Taxonomy name here
                    'terms' => $this->getCatId() //set ID here
                )
            )
        );
    }

    /*
     * This Method Gets an instance of Detail Model Filter Class so we can use the Term Id for related posts.
     */
    protected function getCatId()
    {
        $obj = $this->getDetailObj;
        $hat = $obj->getFilteredModel();

        return $hat[0]['faq'];
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance($getDetailObj)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($getDetailObj);
        }
        return self::$instance;
    }
}