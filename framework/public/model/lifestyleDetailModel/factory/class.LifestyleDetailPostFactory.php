<?php

class LifestyleDetailPostFactory implements factory
{
    public function createModel()
    {
        $postObj = new LifestylePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new LifestyleMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new LifestyleDetailModel($postData, $metaData);

        return $model;
    }
}