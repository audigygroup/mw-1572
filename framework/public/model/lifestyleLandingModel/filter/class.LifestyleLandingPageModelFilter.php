<?php

class LifestyleLandingPageModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {

        $dataSet = new LifestyleLandingPageFactory();
        $this->dataSet =  $dataSet->createModel();
        $this->postDataSet = array(
            'post_title',
            'thumbnail_url'
        );
        $this->metaDataSet = array(
            'lifestyles_landing_sub_headline',
            'lifestyles_landing_description',
            'lifestyles_landing_wistia_id'
        );
    }

    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }
}