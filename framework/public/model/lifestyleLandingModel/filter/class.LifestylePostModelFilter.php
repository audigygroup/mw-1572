<?php

class LifestylePostModelFilter  extends FilterBase
{
    private static $instance;

    private function __construct()
    {

        $dataSet = new LifestylePostFactory();
        $this->dataSet =  $dataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
            'thumbnail_url',
            'permalink'
        );
        $this->metaDataSet = array(
            'lifestyle_list'
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }
}