<?php

class LifestyleLandingPageFactory implements factory
{
    public function createModel()
    {
        $objOne  = new PagePostData();
        $post = $objOne->getPosts();

        $objTwo = new PageMetaData();
        $meta  = $objTwo->getPostMeta();

        $model = new LifestyleLandingPageModel( $post, $meta );

        return $model;
    }
}