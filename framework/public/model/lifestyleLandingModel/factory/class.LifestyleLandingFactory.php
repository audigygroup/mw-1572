<?php

class LifestyleLandingFactory implements factory
{
    public function createModel()
    {
        $objOne = LifestylePostModelFilter::getInstance();
        $objTwo  = LifestyleLandingPageModelFilter::getInstance();

        $model = new LifestyleLandingModel( $objOne, $objTwo );

        return (array) $model;
    }
}