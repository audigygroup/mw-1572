<?php

class LifestylePostFactory implements factory
{
    public function createModel()
    {
        $postObj = new LifestylePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new LifestyleMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new LifestylesPostModel($postData, $metaData);

        return $model;
    }
}