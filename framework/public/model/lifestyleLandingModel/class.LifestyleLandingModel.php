<?php

class LifestyleLandingModel
{
    //name of keys
    public $lifestylePost;
    public $lifestylePage;

    public function __construct(LifestylePostModelFilter $lifestylePost, LifestyleLandingPageModelFilter $lifestylePage)
    {
        $this->lifestylePost = $lifestylePost->getFilteredModel();
        $this->lifestylePage = $lifestylePage->getFilteredModel();
    }
}