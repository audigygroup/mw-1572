<?php

class RequestAppointmentModel
{

    //name of keys
    public $raLocation;
    public $raPage;

    public function __construct(RequestAppointmentLocationModelFilter $location, RequestAppointmentPageModelFilter $practice)
    {
        $this->raLocation = $location->getFilteredModel();
        $this->raPage = $practice->getFilteredModel();
    }


}