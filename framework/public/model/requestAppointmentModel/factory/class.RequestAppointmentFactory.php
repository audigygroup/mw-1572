<?php
/*
 * @TODO Refactor
 */


class RequestAppointmentFactory implements factory
{
    public function createModel()
    {
        $raLocation = RequestAppointmentLocationModelFilter::getInstance();
        $raPractice    = RequestAppointmentPageModelFilter::getInstance();

        $model = new RequestAppointmentModel( $raLocation, $raPractice );

        return $model;
    }
}