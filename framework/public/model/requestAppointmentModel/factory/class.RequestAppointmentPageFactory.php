<?php

class RequestAppointmentPageFactory implements factory
{
    public function createModel()
    {
        $postObj  = new PagePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PageMetaData();
        $metaData    = $postMetaObj->getPostMeta();

        $model = new RequestAppointmentPageModel( $postData, $metaData );

        return $model;
    }
}