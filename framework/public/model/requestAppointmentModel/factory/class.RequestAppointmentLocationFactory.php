<?php

class RequestAppointmentLocationFactory implements factory
{
    public function createModel()
    {
        $postObj  = new LocationPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new LocationMetaData();
        $metaData    = $postMetaObj->getPostMeta();

        $requestAppointmentModel = new RequestAppointmentLocationModel( $postData, $metaData );

        return $requestAppointmentModel;
    }
}