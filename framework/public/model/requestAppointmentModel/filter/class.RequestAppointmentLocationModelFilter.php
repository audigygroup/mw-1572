<?php

class RequestAppointmentLocationModelFilter extends FilterBase{

    private static $instance;

    private function __construct()
    {
        $obj = new RequestAppointmentLocationFactory();
        $this->dataSet =  $obj->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'permalink',
            'thumbnail_url'
        );
        $this->metaDataSet = array(
            'location_title',
            'full_address',
            'address',
            'city',
            'state',
            'zip_code',
            'phone_number',
            'email',
            'latitude',
            'longitude',
            'days_of_operation',
            'hours_of_operation',
            'loc_image'
        );

    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}