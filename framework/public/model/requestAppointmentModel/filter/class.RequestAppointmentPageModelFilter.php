<?php

class RequestAppointmentPageModelFilter extends FilterBase
{

    private static $instance;

    private function __construct()
    {
        $obj = new RequestAppointmentPageFactory();
        $this->dataSet =  $obj->createModel();
        $this->postDataSet = array();
        $this->metaDataSet = array(
            'request_an_apointment_message'
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}