<?php

class InsuranceModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {

        $dataSet = new InsuranceFactory();
        $this->dataSet =  $dataSet->createModel();
        $this->postDataSet = array(
            'post_title',
            'thumbnail_url'
        );
        $this->metaDataSet = array(
            'insurance_headline',
            'insurance_content'
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }
}