<?php

class InsuranceFactory implements factory
{
    public function createModel()
    {
        $postObj = new PagePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PageMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new InsuranceModel($postData, $metaData);

        return $model;
    }
}