<?php

class HearingAidAccessoriesPostModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $dataObj= new HearingAidAccessoriesPostFactory();
        $this->dataSet = $dataObj->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
            'thumbnail_url'
        );
        $this->metaDataSet = array(
            'description',
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}