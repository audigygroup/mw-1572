<?php

class HearingAidAccessoriesCatDetailModelFilter extends TaxonomyFilterBase
{
    private static $instance;

    private function __construct()
    {
        $hearingAidAccessories = new HearingAidAccessoriesCatDetailFactory();
        $this->dataSet = $hearingAidAccessories->createModel();
        $this->filteredFields = array(
            'name',
            'term_id',
            'taxonomy_metadata',
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
