<?php

class HearingAidAccessoriesMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $hearingAidAccessories = new HearingAidAccessoriesPostData();
        $this->postObject = $hearingAidAccessories->getPosts();
    }
}