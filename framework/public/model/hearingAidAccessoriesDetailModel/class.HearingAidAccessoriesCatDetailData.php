<?php

/*
 * @TODO Refactor into base Class to be used for Single Taxonomy pages
 */

class HearingAidAccessoriesCatDetailData
{
    //Set Taxonomy
    protected $taxonomy;

    public function getCatList()
    {
        // Set global post var to use ID for get_the_terms() function
        global $post;

        $this->taxonomy = 'accessories_cat';
        $taxonomy  = $this->taxonomy;
        $tax = get_the_terms($post->ID, $taxonomy);

        return $tax;
    }
}