<?php
/*
 * Gets Post for Accessories
 */
class HearingAidAccessoriesPostData extends GetPosts
{
    //Get Filtered Obj to use term_id
    protected $taxonomyObj;

    public function __construct()
    {
        //Sets Obj to be used by getTermId() method
        $this->taxonomyObj = HearingAidAccessoriesCatDetailModelFilter::getInstance();

        $this->args = array(
            'post_type' => 'accessories',
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'accessories_cat', //Taxonomy name here
                    'terms' => $this->getTermId() //set ID here
                )
            )
        );
    }

    /*
     * Gets Term ID to Filter Posts by Term in Taxonomy. Returns Term ID
     */
    protected function getTermId()
    {
        $taxonomyObj = $this->taxonomyObj;
        $tax = $taxonomyObj->getFilteredModel();

        return $tax[0]['term_id'];
    }
}