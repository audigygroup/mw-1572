<?php

class HearingAidAccessoriesPostFactory implements factory
{
    public function createModel()
    {
        $postObj = new HearingAidAccessoriesPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new HearingAidAccessoriesMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $hearingAidTypeModel = new HearingAidAccessoriesPostModel($postData, $metaData);


        return $hearingAidTypeModel;
    }
}