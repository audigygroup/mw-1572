<?php

class HearingAidAccessoriesDetailFactory implements factory
{
    /*
    * This Factory Merges filtered Data
    */
    public function createModel()
    {
        $post = HearingAidAccessoriesPostModelFilter::getInstance();
        $cat  = HearingAidAccessoriesCatDetailModelFilter::getInstance();

        $model = new HearingAidAccessoriesDetailModel( $post, $cat );

        return (array) $model;
    }
}