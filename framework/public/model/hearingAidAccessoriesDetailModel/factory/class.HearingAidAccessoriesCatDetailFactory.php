<?php

class HearingAidAccessoriesCatDetailFactory implements factory
{
    public function createModel()
    {
        $tax = new HearingAidAccessoriesCatDetailData();
        $taxImg = new HearingAidAccessoriesCatMetaData();

        $hearingAidAccessoriesCatModel = new HearingAidAccessoriesCatDetailModel($tax, $taxImg);

        return $hearingAidAccessoriesCatModel;
    }
}