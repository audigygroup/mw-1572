<?php

class HearingAidAccessoriesDetailModel
{
    //name of keys
    public $hearingAccessoriesPost;
    public $hearingAccessoriesCat;

    public function __construct(HearingAidAccessoriesPostModelFilter $hearingAccessoriesPost, HearingAidAccessoriesCatDetailModelFilter $hearingAccessoriesCat)
    {
        $this->hearingAccessoriesPost = $hearingAccessoriesPost->getFilteredModel();
        $this->hearingAccessoriesCat = $hearingAccessoriesCat->getFilteredModel();
    }
}