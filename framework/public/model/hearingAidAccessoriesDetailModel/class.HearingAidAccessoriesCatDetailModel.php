<?php

class HearingAidAccessoriesCatDetailModel
{
    public $tax;
    public $taxImg;

    public function __construct(HearingAidAccessoriesCatDetailData $tax, HearingAidAccessoriesCatMetaData $taxImg)
    {
        $this->tax = $tax->getCatList();
        $this->taxImg = $taxImg->getCatMeta();
    }
}