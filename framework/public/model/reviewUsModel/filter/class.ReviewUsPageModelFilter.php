<?php

class ReviewUsPageModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $dataSet = new ReviewUsPageFactory();
        $this->dataSet = $dataSet->createModel();
        $this->postDataSet = array();

        $this->metaDataSet = array(
            'review_us_headline',
            'review_us_page_description',
            'appreciation_message',
            'google_plus_video_id',
            'facebook_video_id',
            'yelp_video_id'
        );
    }


    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
