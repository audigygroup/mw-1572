<?php

class ReviewUsModel
{
    public $location;
    public $reviewUsPage;

    public function __construct(ReviewUsLocationModelFilter $location, ReviewUsPageModelFilter $reviewUsPage)
    {
        $this->location = $location->getFilteredModel();
        $this->reviewUsPage = $reviewUsPage->getFilteredModel();
    }
}
