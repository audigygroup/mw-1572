<?php

class ReviewUsFilterFactory implements factory
{
    public function createModel()
    {
        $loc = ReviewUsLocationModelFilter::getInstance();
        $page = ReviewUsPageModelFilter::getInstance();

        $model = new ReviewUsModel($loc, $page);

        return $model;
    }
}