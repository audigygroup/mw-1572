<?php

class ReviewUsLocationFactory implements factory
{
    /*
    * Method to create data Object
    */
    public function createModel()
    {
        $postObj = new LocationPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new LocationMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new ReviewUsLocationModel($postData, $metaData);

        return $model;
    }
}