<?php

class EPatientComponentMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $contentObj = PageModelFilter::getInstance();
        $postId = EPatientComponentPostData::getInstance($contentObj);
        $this->postObject = $postId->getPosts();
    }
}