<?php

class EPatientComponentPostData  extends GetPosts
{
    private static $instance;
    protected $getDetailObj;

    public function __construct($getDetailObj)
    {
        $this->getDetailObj = $getDetailObj;

        $this->args = array(
            'post_type' => 'e-patient-video',
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'e_patient_cat', //Taxonomy name here
                    'terms' => $this->getCatId() //set ID here
                )
            )
        );
    }

    /*
     * This Method Gets an instance of Detail Model Filter Class so we can use the Term Id for related posts.
     */
    protected function getCatId()
    {
        $obj = $this->getDetailObj;
        $page = $obj->getFilteredModel();

        if(is_tax()){
            return $page[0]['taxonomy_metadata']['e_patient_videos'];
        }else{
            return $page[0]['e_patient_videos'];
        }
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance($getDetailObj)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($getDetailObj);
        }
        return self::$instance;
    }
}