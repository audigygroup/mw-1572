<?php

class EPatientComponentModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        // Pass in filter to be used with dependency injection
        global $post;
        if($post->post_type == 'page'){
            $contentObj = PageModelFilter::getInstance();
        }elseif($post->post_type == 'hearing-aid-type'){
            $contentObj = HearingAidTypeDetailModelFilter::getInstance();
        }elseif(is_tax('accessories_cat')){
            $contentObj = HearingAidAccessoriesCatDetailModelFilter::getInstance();
        }

        $dataObj= EPatientComponentFactory::getInstance();
        $this->dataSet = $dataObj->createModel($contentObj);
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
            'thumbnail_url'
        );
        $this->metaDataSet = array(
            'wistia_video_id'
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}