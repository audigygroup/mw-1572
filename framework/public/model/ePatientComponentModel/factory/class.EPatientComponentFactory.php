<?php

class EPatientComponentFactory
{

    private static $instance;
    protected $contentObj;

    public function createModel($contentObj)
    {
        $this->contentObj = $contentObj;

        $postObj = EPatientComponentPostData::getInstance($contentObj);
        $postData = $postObj->getPosts();

        $postMetaObj = new EPatientComponentMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new EPatientComponentModel($postData, $metaData);

        return $model;
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}