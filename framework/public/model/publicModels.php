<?php
/*
      ___           ___           ___           ___                       ___           ___           ___                       ___           ___           ___
     /__/\         /  /\         /  /\         /  /\        ___          /__/\         /  /\         /  /\          ___        /__/\         /  /\         /  /\
    |  |::\       /  /:/_       /  /::\       /  /::\      /  /\        _\_ \:\       /  /:/_       /  /::\        /  /\       \  \:\       /  /:/_       /  /::\
    |  |:|:\     /  /:/ /\     /  /:/\:\     /  /:/\:\    /  /:/       /__/\ \:\     /  /:/ /\     /  /:/\:\      /  /:/        \__\:\     /  /:/ /\     /  /:/\:\
  __|__|:|\:\   /  /:/ /:/_   /  /:/~/:/    /  /:/~/:/   /__/::\      _\_ \:\ \:\   /  /:/ /:/_   /  /:/~/::\    /  /:/     ___ /  /::\   /  /:/ /:/_   /  /:/~/:/
 /__/::::| \:\ /__/:/ /:/ /\ /__/:/ /:/___ /__/:/ /:/___ \__\/\:\__  /__/\ \:\ \:\ /__/:/ /:/ /\ /__/:/ /:/\:\  /  /::\    /__/\  /:/\:\ /__/:/ /:/ /\ /__/:/ /:/___
 \  \:\~~\__\/ \  \:\/:/ /:/ \  \:\/:::::/ \  \:\/:::::/    \  \:\/\ \  \:\ \:\/:/ \  \:\/:/ /:/ \  \:\/:/__\/ /__/:/\:\   \  \:\/:/__\/ \  \:\/:/ /:/ \  \:\/:::::/
  \  \:\        \  \::/ /:/   \  \::/~~~~   \  \::/~~~~      \__\::/  \  \:\ \::/   \  \::/ /:/   \  \::/      \__\/  \:\   \  \::/       \  \::/ /:/   \  \::/~~~~
   \  \:\        \  \:\/:/     \  \:\        \  \:\          /__/:/    \  \:\/:/     \  \:\/:/     \  \:\           \  \:\   \  \:\        \  \:\/:/     \  \:\
    \  \:\        \  \::/       \  \:\        \  \:\         \__\/      \  \::/       \  \::/       \  \:\           \__\/    \  \:\        \  \::/       \  \:\
     \__\/         \__\/         \__\/         \__\/                     \__\/         \__\/         \__\/                     \__\/         \__\/         \__\/

 */
// Base Classes Located in Includes
require_once(MW_INC . 'model/baseClasses/class.WpOptionDataBase.php');
require_once(MW_INC . 'model/baseClasses/class.WpOptionDataFilterBase.php');
require_once(MW_INC . 'model/baseClasses/class.MWPost.php');
require_once(MW_INC . 'model/baseClasses/class.MWMetaPost.php');
require_once(MW_INC . 'model/baseClasses/class.GetPosts.php');
require_once(MW_INC . 'model/baseClasses/class.GetPostsMeta.php');
require_once(MW_INC . 'model/baseClasses/class.BaseModel.php');
require_once(MW_INC . 'model/baseClasses/class.FilterBase.php');
require_once(MW_INC . 'model/baseClasses/class.GetCatList.php');
require_once(MW_INC . 'model/baseClasses/class.GetCatListMeta.php');
require_once(MW_INC . 'model/baseClasses/class.TaxonomyFilterBase.php');
/*
 * interface
 */
require_once(MW_INC . 'model/interface/interface.factory.php');

/*
 * Helpers Get Post and Post Meta
 */
require_once(MW_INC . 'model/helper/class.PracticePostData.php');
//posts
require_once(MW_INC . 'model/helper/class.TestimonialPostData.php');
require_once(MW_INC . 'model/helper/class.ProfessionalPostData.php');
require_once(MW_INC . 'model/helper/class.LocationPostData.php');
require_once(MW_INC . 'model/helper/class.HearingAidTypePostData.php');
require_once(MW_INC . 'model/helper/class.LifestylePostData.php');
require_once(MW_INC . 'model/helper/class.MainSliderPostData.php');
require_once(MW_INC . 'model/helper/class.EventsAndSeminarsPostData.php');
require_once(MW_INC . 'model/helper/class.PatientFormsFormPostData.php');
//meta
require_once(MW_INC . 'model/helper/getPostMeta/class.TestimonialPostMetaData.php');
require_once(MW_INC . 'model/helper/getPostMeta/class.ProfessionalMetaData.php');
require_once(MW_INC . 'model/helper/getPostMeta/class.LocationMetaData.php');
require_once(MW_INC . 'model/helper/getPostMeta/class.HearingAidTypeMetaData.php');
require_once(MW_INC . 'model/helper/getPostMeta/class.LifestyleMetaData.php');
require_once(MW_INC . 'model/helper/getPostMeta/class.MainSliderMetaData.php');
require_once(MW_INC . 'model/helper/getPostMeta/class.EventsAndSeminarsMetaData.php');
require_once(MW_INC . 'model/helper/getPostMeta/class.PatientFormsFormMetaData.php');

/*
 * Header
 */
require_once(MW_MODEL . 'headerModel/class.HeaderLocationModel.php');
require_once(MW_MODEL . 'headerModel/class.HeaderModel.php');
//factory
require_once(MW_MODEL . 'headerModel/factory/class.HeaderLocationFactory.php');
require_once(MW_MODEL . 'headerModel/factory/class.HeaderFilterFactory.php');
//filter
require_once(MW_MODEL . 'headerModel/filter/class.HeaderLocationModelFilter.php');
require_once(MW_MODEL . 'headerModel/filter/class.HeaderPracticeModelFilter.php');


/*
 * MainSlider
 */
require_once(MW_MODEL . 'mainSliderModel/class.MainSliderModel.php');
//factory
require_once(MW_MODEL . 'mainSliderModel/factory/class.MainSliderFactory.php');
//filter
require_once(MW_MODEL . 'mainSliderModel/filter/class.MainSliderModelFilter.php');

/*
 * AddThis
 */
require_once(MW_MODEL . 'addThisModel/class.AddthisModelFilter.php');

/*
 * Testimonial
 */
require_once(MW_MODEL . 'testimonialModel/class.TestimonialModel.php');
require_once(MW_MODEL . 'testimonialModel/factory/class.TestimonialFactory.php');
require_once(MW_MODEL . 'testimonialModel/filter/class.TestimonialModelFilter.php');

/*
 * Testimonial Page
 */
require_once(MW_MODEL . 'testimonialPageModel/class.TestimonialPostModel.php');
require_once(MW_MODEL . 'testimonialPageModel/class.TestimonialPageModel.php');
require_once(MW_MODEL . 'testimonialPageModel/class.TestimonialsModel.php');

//factory
require_once(MW_MODEL . 'testimonialPageModel/factory/class.TestimonialPostFactory.php');
require_once(MW_MODEL . 'testimonialPageModel/factory/class.TestimonialPageFactory.php');
require_once(MW_MODEL . 'testimonialPageModel/factory/class.TestimonialFilterFactory.php');
//filter
require_once(MW_MODEL . 'testimonialPageModel/filter/class.TestimonialPostModelFilter.php');
require_once(MW_MODEL . 'testimonialPageModel/filter/class.TestimonialPageModelFilter.php');

/*
 * Welcome
 */
require_once(MW_MODEL . 'welcomeModel/class.WelcomeProfessionalModel.php');
require_once(MW_MODEL . 'welcomeModel/class.WelcomeModel.php');
//Filters
require_once(MW_MODEL . 'welcomeModel/filter/class.WelcomeProfessionalFilter.php');
require_once(MW_MODEL . 'welcomeModel/filter/class.WelcomePracticeDataFilter.php');
//Factories
require_once(MW_MODEL . 'welcomeModel/factory/class.WelcomeProfessionalFactory.php');
require_once(MW_MODEL . 'welcomeModel/factory/class.WelcomeFactory.php');

/*
 * Location home page Component
 */
require_once(MW_MODEL . 'locationModel/class.LocationModel.php');
require_once(MW_MODEL . 'locationModel/factory/class.LocationFactory.php');
require_once(MW_MODEL . 'locationModel/filter/class.LocationModelFilter.php');
/*
 * Location Homepage Map Component
 */
require_once(MW_MODEL . 'locationMapComponent/filter/class.LocationMapComponentModelFilter.php');


// Quick Nav
require_once(MW_MODEL . 'quickNavModel/class.QuickNavPostData.php');
require_once(MW_MODEL . 'quickNavModel/class.QuickNavMetaData.php');
require_once(MW_MODEL . 'quickNavModel/class.QuickNavModel.php');
//Factory
require_once(MW_MODEL . 'quickNavModel/factory/class.QuickNavFactory.php');
//Filter
require_once(MW_MODEL . 'quickNavModel/filter/class.QuickNavModelFilter.php');

/*
 * Hearing Aid Types
 */
require_once(MW_MODEL . 'hearingAidTypesModel/class.HearingAidTypeModel.php');
//factory
require_once(MW_MODEL . 'hearingAidTypesModel/factory/class.HearingAidTypeFactory.php');
//filter
require_once(MW_MODEL . 'hearingAidTypesModel/filter/class.HearingAidTypeModelFilter.php');

//Call To Action
require_once(MW_MODEL . 'callToActionModel/class.CallToActionModelFilter.php');

/*
 * Footer
 */

require_once(MW_MODEL . 'footerModel/class.FooterModel.php');
//filter
require_once(MW_MODEL . 'footerModel/filter/class.FooterPracticeDataModelFilter.php');
require_once(MW_MODEL . 'footerModel/filter/class.FooterLocationDataModelFilter.php');

/*
 * Copyright Bar
 */
require_once(MW_MODEL . 'copyrightModel/class.CopyrightModelFilter.php');

/*
* Our Practice
*/
require_once(MW_MODEL . 'ourPracticeModel/class.OurPracticeModel.php');
//filter
require_once(MW_MODEL . 'ourPracticeModel/filter/class.OurPracticeModelFilter.php');
//factory
require_once(MW_MODEL . 'ourPracticeModel/factory/class.OurPracticeFactory.php');

/*
 * Our Professionals
 */
require_once(MW_MODEL . 'ourProfessionalsModel/class.ProfessionalsModel.php');
require_once(MW_MODEL . 'ourProfessionalsModel/class.OurProfessionalsModel.php');
require_once(MW_MODEL . 'ourProfessionalsModel/class.OurProfessionalsPageModel.php');
// filter
require_once(MW_MODEL . 'ourProfessionalsModel/filter/class.ProfessionalsModelFilter.php');
require_once(MW_MODEL . 'ourProfessionalsModel/filter/class.OurProfessionalsPageModelFilter.php');

// factory
require_once(MW_MODEL . 'ourProfessionalsModel/factory/class.ProfessionalsFactory.php');
require_once(MW_MODEL . 'ourProfessionalsModel/factory/class.OurProfessionalsPageFactory.php');
require_once(MW_MODEL . 'ourProfessionalsModel/factory/class.OurProfessionalsFilterFactory.php');

/*
 * Professionals Detail Page
 */
require_once(MW_MODEL . 'OurProfessionalDetailModel/class.OurProfessionalDetailModel.php');
//factory
require_once(MW_MODEL . 'OurProfessionalDetailModel/factory/class.OurProfessionalDetailFactory.php');
//filter
require_once(MW_MODEL . 'OurProfessionalDetailModel/filter/class.OurProfessionalDetailModelFilter.php');

/*
 * Side Bar
 */
require_once(MW_MODEL . 'sideBarModel/class.SideBarModel.php');
require_once(MW_MODEL . 'sideBarModel/class.SideBarLocationModel.php');
//factory
require_once(MW_MODEL . 'sideBarModel/factory/class.SideBarLocationFactory.php');
require_once(MW_MODEL . 'sideBarModel/factory/class.SideBarFactory.php');
//filter
require_once(MW_MODEL . 'sideBarModel/filter/class.SideBarPracticeModelFilter.php');
require_once(MW_MODEL . 'sideBarModel/filter/class.SideBarLocationModelFilter.php');

/*
 * Insurance
 */
require_once(MW_MODEL . 'insuranceModel/class.InsuranceModel.php');
//factory
require_once(MW_MODEL . 'insuranceModel/factory/class.InsuranceFactory.php');
//filter
require_once(MW_MODEL . 'insuranceModel/filter/class.InsuranceModelFilter.php');

/*
 * Patient Forms
 */
require_once(MW_MODEL . 'patientFormsPageModel/class.PatientFormsFormModel.php');
require_once(MW_MODEL . 'patientFormsPageModel/class.PatientFormsPageModel.php');
require_once(MW_MODEL . 'patientFormsPageModel/class.PatientFormsModel.php');
//factory
require_once(MW_MODEL . 'patientFormsPageModel/factory/class.PatientFormsFormFactory.php');
require_once(MW_MODEL . 'patientFormsPageModel/factory/class.PatientFormsFilterFactory.php');
require_once(MW_MODEL . 'patientFormsPageModel/factory/class.PatientFormsPageFactory.php');
//filter
require_once(MW_MODEL . 'patientFormsPageModel/filter/class.PatientFormsFormModelFilter.php');
require_once(MW_MODEL . 'patientFormsPageModel/filter/class.PatientFormsPageModelFilter.php');

/*
 * Review Us
 */
require_once(MW_MODEL . 'reviewUsModel/class.ReviewUsLocationModel.php');
require_once(MW_MODEL . 'reviewUsModel/class.ReviewUsPageModel.php');
require_once(MW_MODEL . 'reviewUsModel/class.ReviewUsModel.php');
//factory
require_once(MW_MODEL . 'reviewUsModel/factory/class.ReviewUsLocationFactory.php');
require_once(MW_MODEL . 'reviewUsModel/factory/class.ReviewUsPageFactory.php');
require_once(MW_MODEL . 'reviewUsModel/factory/class.ReviewUsFilterFactory.php');
//filter
require_once(MW_MODEL . 'reviewUsModel/filter/class.ReviewUsLocationModelFilter.php');
require_once(MW_MODEL . 'reviewUsModel/filter/class.ReviewUsPageModelFilter.php');

/*
* Location Detail Page
*/
require_once(MW_MODEL . 'locationDetailModel/class.LocationDetailModel.php');
require_once(MW_MODEL . 'locationDetailModel/class.LocationProfessionalModel.php');
require_once(MW_MODEL . 'locationDetailModel/class.LocationPageModel.php');
require_once(MW_MODEL . 'locationDetailModel/class.LocationAccordionPostData.php');
require_once(MW_MODEL . 'locationDetailModel/class.LocationAccordionMetaData.php');
require_once(MW_MODEL . 'locationDetailModel/class.LocationAccordionModel.php');
//Factories
require_once(MW_MODEL . 'locationDetailModel/factory/class.LocationDetailFactory.php');
require_once(MW_MODEL . 'locationDetailModel/factory/class.LocationProfessionalFactory.php');
require_once(MW_MODEL . 'locationDetailModel/factory/class.LocationAccordionFactory.php');
require_once(MW_MODEL . 'locationDetailModel/factory/class.LocationPageFactory.php');
//Filters
require_once(MW_MODEL . 'locationDetailModel/filter/class.LocationProfessionalModelFilter.php');
require_once(MW_MODEL . 'locationDetailModel/filter/class.LocationAccordionModelFilter.php');
require_once(MW_MODEL . 'locationDetailModel/filter/class.LocationDetailModelFilter.php');

/*
* Thank You
*/
require_once(MW_MODEL . 'thankYouModel/class.ThankYouModelFilter.php');

/*
* Request Appointment
*/
require_once(MW_MODEL . 'requestAppointmentModel/class.RequestAppointmentLocationModel.php');
require_once(MW_MODEL . 'requestAppointmentModel/class.RequestAppointmentPageModel.php');
require_once(MW_MODEL . 'requestAppointmentModel/class.RequestAppointmentModel.php');
//factory
require_once(MW_MODEL . 'requestAppointmentModel/factory/class.RequestAppointmentLocationFactory.php');
require_once(MW_MODEL . 'requestAppointmentModel/factory/class.RequestAppointmentPageFactory.php');
require_once(MW_MODEL . 'requestAppointmentModel/factory/class.RequestAppointmentFactory.php');
//filters
require_once(MW_MODEL . 'requestAppointmentModel/filter/class.RequestAppointmentLocationModelFilter.php');
require_once(MW_MODEL . 'requestAppointmentModel/filter/class.RequestAppointmentPageModelFilter.php');

/*
 * Hearing Aid Types Landing
 */
require_once(MW_MODEL . 'hearingAidTypeLandingModel/class.HearingAidTypeLandingModel.php');
require_once(MW_MODEL . 'hearingAidTypeLandingModel/class.HearingAidTypeLandingPageModel.php');
//require_once(MW_MODEL . 'hearingAidTypeLandingModel/class.HearingAidTypePostModel.php');
//Factory
require_once(MW_MODEL . 'hearingAidTypeLandingModel/factory/class.HearingAidTypeLandingFactory.php');
//require_once(MW_MODEL . 'hearingAidTypeLandingModel/factory/class.HearingAidTypePostFactory.php');
require_once(MW_MODEL . 'hearingAidTypeLandingModel/factory/class.HearingAidTypeLandingPageFactory.php');
//Filter
require_once(MW_MODEL . 'hearingAidTypeLandingModel/filter/class.HearingAidTypeLandingModelFilter.php');
require_once(MW_MODEL . 'hearingAidTypeLandingModel/filter/class.HearingAidTypeLandingPageModelFilter.php');

/*
 * Hearing Aid Type Nav
 */
require_once(MW_MODEL . 'hearingAidTypeDetailNavModel/class.HearingAidTypeNavPostData.php');
require_once(MW_MODEL . 'hearingAidTypeDetailNavModel/class.HearingAidTypeNavModel.php');
//Factory
require_once(MW_MODEL . 'hearingAidTypeDetailNavModel/factory/class.HearingAidTypeNavFactory.php');
//filter
require_once(MW_MODEL . 'hearingAidTypeDetailNavModel/filter/class.HearingAidTypeNavModelFilter.php');

/*
* Hearing Aid Type Detail
*/
require_once(MW_MODEL . 'hearingAidTypeDetailModel/class.HearingAidTypeDetailModel.php');
//Factory
require_once(MW_MODEL . 'hearingAidTypeDetailModel/factory/class.HearingAidTypeDetailPostFactory.php');
//Filter
require_once(MW_MODEL . 'hearingAidTypeDetailModel/filter/class.HearingAidTypeDetailModelFilter.php');

/*
* Hearing Aid Accessories Category
*/
require_once(MW_MODEL . 'hearingAidAccessoriesModel/class.HearingAidAccessoriesCatData.php');
require_once(MW_MODEL . 'hearingAidAccessoriesModel/class.HearingAidAccessoriesCatMetaData.php');
require_once(MW_MODEL . 'hearingAidAccessoriesModel/class.HearingAidAccessoriesCatModel.php');
require_once(MW_MODEL . 'hearingAidAccessoriesModel/class.HearingAidAccessoriesPageModel.php');
require_once(MW_MODEL . 'hearingAidAccessoriesModel/class.HearingAidAccessoriesModel.php');
//Factory
require_once(MW_MODEL . 'hearingAidAccessoriesModel/factory/class.HearingAidAccessoriesCatFactory.php');
require_once(MW_MODEL . 'hearingAidAccessoriesModel/factory/class.HearingAidAccessoriesPageFactory.php');
require_once(MW_MODEL . 'hearingAidAccessoriesModel/factory/class.HearingAidAccessoriesFilterFactory.php');
//Filter
require_once(MW_MODEL . 'hearingAidAccessoriesModel/filter/class.HearingAidAccessoriesCatModelFilter.php');
require_once(MW_MODEL . 'hearingAidAccessoriesModel/filter/class.HearingAidAccessoriesPageModelFilter.php');


/*
 * Hearing Aid Accessories Detail Page
 */
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/class.HearingAidAccessoriesCatDetailData.php');
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/class.HearingAidAccessoriesCatDetailModel.php');
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/class.HearingAidAccessoriesPostData.php');
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/class.HearingAidAccessoriesMetaData.php');
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/class.HearingAidAccessoriesPostModel.php');
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/class.HearingAidAccessoriesDetailModel.php');
//Factory
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/factory/class.HearingAidAccessoriesCatDetailFactory.php');
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/factory/class.HearingAidAccessoriesPostFactory.php');
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/factory/class.HearingAidAccessoriesDetailFactory.php');
//Filter
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/filter/class.HearingAidAccessoriesCatDetailModelFilter.php');
require_once(MW_MODEL . 'hearingAidAccessoriesDetailModel/filter/class.HearingAidAccessoriesPostModelFilter.php');

/*
 * Financing Options Page
 */
require_once(MW_MODEL . 'financingOptionsModel/class.FinancingOptionsModel.php');
//filter
require_once(MW_MODEL . 'financingOptionsModel/filter/class.FinancingOptionsModelFilter.php');
//factory
require_once(MW_MODEL . 'financingOptionsModel/factory/class.FinancingOptionsFactory.php');

/*
 * Lifestyle Landing Page
 */
require_once(MW_MODEL . 'lifestyleLandingModel/class.LifestylesPostModel.php');
require_once(MW_MODEL . 'lifestyleLandingModel/class.LifestyleLandingModel.php');
require_once(MW_MODEL . 'lifestyleLandingModel/class.LifestyleLandingPageModel.php');
//Factory
require_once(MW_MODEL . 'lifestyleLandingModel/factory/class.LifestylePostFactory.php');
require_once(MW_MODEL . 'lifestyleLandingModel/factory/class.LifestyleLandingPageFactory.php');
require_once(MW_MODEL . 'lifestyleLandingModel/factory/class.LifestyleLandingFactory.php');
//Filters
require_once(MW_MODEL . 'lifestyleLandingModel/filter/class.LifestylePostModelFilter.php');
require_once(MW_MODEL . 'lifestyleLandingModel/filter/class.LifestyleLandingPageModelFilter.php');


/*
 * Lifestyle Details page
 */
require_once(MW_MODEL . 'lifestyleDetailModel/class.LifestyleDetailModel.php');
//Factory
require_once(MW_MODEL . 'lifestyleDetailModel/factory/class.LifestyleDetailPostFactory.php');
//Filter
require_once(MW_MODEL . 'lifestyleDetailModel/filter/class.LifestyleDetailModelFilter.php');

/*
 * Accordion Component
 */
require_once(MW_MODEL . 'accordionComponentModel/class.AccordionComponentPostData.php');
require_once(MW_MODEL . 'accordionComponentModel/class.AccordionComponentMetaData.php');
require_once(MW_MODEL . 'accordionComponentModel/class.AccordionComponentModel.php');
//factory
require_once(MW_MODEL . 'accordionComponentModel/factory/class.AccordionComponentFactory.php');
//filter
require_once(MW_MODEL . 'accordionComponentModel/filter/class.AccordionComponentModelFilter.php');

/*
 * E-Patient Component
 */
require_once(MW_MODEL . 'ePatientComponentModel/class.EPatientComponentPostData.php');
require_once(MW_MODEL . 'ePatientComponentModel/class.EPatientComponentMetaData.php');
require_once(MW_MODEL . 'ePatientComponentModel/class.EPatientComponentModel.php');
//factory
require_once(MW_MODEL . 'ePatientComponentModel/factory/class.EPatientComponentFactory.php');
//filter
require_once(MW_MODEL . 'ePatientComponentModel/filter/class.EPatientComponentModelFilter.php');

/*
 * Page
 */
require_once(MW_MODEL . 'helperModels/PagePostModel/class.PagePostData.php');
require_once(MW_MODEL . 'helperModels/PagePostModel/class.PageMetaData.php');
require_once(MW_MODEL . 'helperModels/PagePostModel/class.PageModel.php');
//factory
require_once(MW_MODEL . 'helperModels/PagePostModel/factory/class.PageFactory.php');
//filter
require_once(MW_MODEL . 'helperModels/PagePostModel/filter/class.PageModelFilter.php');

/*
 * Google Analytics
 */
require_once(MW_MODEL . 'googleAnalyticsModel/filter/class.GoogleAnalyticsModelFilter.php');

/*
 * Component Render
 */
require_once(MW_MODEL . 'componentRenderModel/class.ComponentRenderPostData.php');
require_once(MW_MODEL . 'componentRenderModel/class.ComponentRenderMetaData.php');
require_once(MW_MODEL . 'componentRenderModel/class.ComponentRenderModel.php');
require_once(MW_MODEL . 'componentRenderModel/factory/class.ComponentRenderFactory.php');
require_once(MW_MODEL . 'componentRenderModel/filter/class.ComponentRenderFilter.php');

/*
 * Blog Landing
 */
require_once(MW_MODEL . 'blogLandingModel/class.BlogPostData.php');
require_once(MW_MODEL . 'blogLandingModel/class.BlogMetaData.php');
require_once(MW_MODEL . 'blogLandingModel/class.BlogLandingModel.php');
//Filter
require_once(MW_MODEL . 'blogLandingModel/filter/class.BlogLandingModelFilter.php');
//Factory
require_once(MW_MODEL . 'blogLandingModel/factory/class.BlogLandingFactory.php');

/*
 * Blog Single
 */
require_once(MW_MODEL . 'blogSingleModel/class.RelatedPostData.php');
require_once(MW_MODEL . 'blogSingleModel/class.RelatedMetaData.php');
require_once(MW_MODEL . 'blogSingleModel/class.BlogSingleModel.php');
require_once(MW_MODEL . 'blogSingleModel/factory/class.RelatedPostFactory.php');
require_once(MW_MODEL . 'blogSingleModel/filter/class.BlogSingleModelFilter.php');

/*
 * Events And Seminars Landing
 */
require_once(MW_MODEL . 'eventsAndSeminarsLandingModel/class.EventsAndSeminarsLandingModel.php');
require_once(MW_MODEL . 'eventsAndSeminarsLandingModel/factory/class.EventsAndSeminarsLandingFactory.php');
require_once(MW_MODEL . 'eventsAndSeminarsLandingModel/filter/class.EventsAndSeminarsLandingModelFilter.php');

/*
 * Events And Seminars Single
 */
require_once(MW_MODEL . 'eventsAndSeminarsSingleModel/class.EventsAndSeminarsEventModel.php');
require_once(MW_MODEL . 'eventsAndSeminarsSingleModel/class.EventsAndSeminarsPresenterModel.php');
require_once(MW_MODEL . 'eventsAndSeminarsSingleModel/class.EventsAndSeminarsSingleModel.php');
//factory
require_once(MW_MODEL . 'eventsAndSeminarsSingleModel/factory/class.EventsAndSeminarsEventFactory.php');
require_once(MW_MODEL . 'eventsAndSeminarsSingleModel/factory/class.EventsAndSeminarsPresenterFactory.php');
require_once(MW_MODEL . 'eventsAndSeminarsSingleModel/factory/class.EventsAndSeminarsSingleFactory.php');
//filter
require_once(MW_MODEL . 'eventsAndSeminarsSingleModel/filter/class.EventsAndSeminarsPresenterModelFilter.php');
require_once(MW_MODEL . 'eventsAndSeminarsSingleModel/filter/class.EventsAndSeminarsEventModelFilter.php');

