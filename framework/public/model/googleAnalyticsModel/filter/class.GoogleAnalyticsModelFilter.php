<?php

class GoogleAnalyticsModelFilter extends WpOptionDataFilterBase
{

    private static $instance;

    private function __construct()
    {
        $this->model =  PracticePostData::getInstance();
        $this->requiredData = array(
            'options_google_analytics',
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}