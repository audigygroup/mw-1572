<?php

class CopyrightModelFilter extends WpOptionDataFilterBase
{

    private static $instance;

    private function __construct()
    {
        $this->model =  PracticePostData::getInstance();
        $this->requiredData = array(
            'options_powered_by',
            'options_powered_by_link'
        );
    }

    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }

}