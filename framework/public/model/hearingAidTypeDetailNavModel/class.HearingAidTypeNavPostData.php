<?php

class HearingAidTypeNavPostData extends GetPosts
{
    public function __construct()
    {
        $this->args = array(
            'post_type' => 'hearing-aid-type',
            'orderby'   => 'menu_order',
            'order'     => 'ASC'
        );
    }
}