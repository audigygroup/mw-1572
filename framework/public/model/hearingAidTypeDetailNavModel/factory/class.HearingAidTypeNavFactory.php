<?php

class HearingAidTypeNavFactory implements factory
{
    public function createModel()
    {
        $postObj = new HearingAidTypeNavPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new HearingAidTypeMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $hearingAidTypeNavModel = new HearingAidTypeNavModel($postData, $metaData);

        return $hearingAidTypeNavModel;
    }
}