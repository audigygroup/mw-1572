<?php

class RelatedPostData extends GetPosts
{
    public function __construct()
    {
        global $post;
        $this->args = array(
            'post_type'       => 'post',
            'orderby'         => 'post_date',
            'order'           => 'DESC',
            'posts_per_page'  => 3,
            'tax_query' => array(
                array(
                    'taxonomy' => 'category', //Taxonomy name here
                    'terms' => wp_get_post_categories( $post->ID ) //set ID here
                )
            )
        );
    }
}