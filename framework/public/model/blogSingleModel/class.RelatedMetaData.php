<?php

class RelatedMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $post = new RelatedPostData();
        $this->postObject = $post->getPosts();
    }
}