<?php

class RelatedPostFactory implements factory
{
    public function createModel()
    {
        $postObj = new RelatedPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new RelatedMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new BlogSingleModel($postData, $metaData);

        return $model;
    }
}