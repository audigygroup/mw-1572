<?php

class HearingAidTypeModelFilter extends FilterBase{

    private static $instance;


    private function __construct()
    {

        $hearingAidTypeDataSet = new HearingAidTypeFactory();
        $this->dataSet =  $hearingAidTypeDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
            'permalink'
        );
        $this->metaDataSet = array(
            'hearing_aid_type',
            'description_excerpt',
            'image_left',
            'image_center',
            'image_right'
        );

    }


    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }


}