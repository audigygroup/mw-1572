<?php

class HearingAidTypeFactory implements factory
{
    public function createModel()
    {
        $postObj = new HearingAidTypePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new HearingAidTypeMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new HearingAidTypeModel($postData, $metaData);

        return $model;
    }
}