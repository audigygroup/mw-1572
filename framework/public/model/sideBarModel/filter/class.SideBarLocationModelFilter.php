<?php

class SideBarLocationModelFilter extends FilterBase
{
    private static $instance;

    protected $openDays = array();
    protected $hours = array();
    protected $symbol = array();
    protected $dayClose = array();

    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
    {
        $locationDataSet =  new SideBarLocationFactory();
        $this->dataSet =  $locationDataSet->createModel();
        $this->getRepeaterRows();

        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
            'permalink'
        );

        $this->metaDataSet = array(
            'location_title',
            'address',
            'city',
            'state',
            'zip_code',
            'phone_number',
            'email',
        );

        $this->addKeysRepeaterMetaData();
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /*
     * Builds Key used by ACF Pro repeater to add to filter
     */
    protected function getRepeaterRows()
    {
        //Use Data Obj to get Id to filter by ID
        $locationObj = $this->dataSet;
        $rows = array();

        foreach($locationObj->post as $loc){
            $row = get_field('operation', $loc->ID);
            $rows[] = $row;
        }


        foreach($rows as $key => $r){
            $day = 'operation_'. $key .'_day_open';
            $symbol = 'operation_'. $key .'_symbol';
            $dayClose = 'operation_'. $key .'_day_close';
            $hours = 'operation_'. $key .'_time';
            $this->openDays[] = $day;
            $this->symbol[] = $symbol;
            $this->dayClose[] = $dayClose;
            $this->hours[] = $hours;
        }
    }

    /*
     * Insert Data into meta filter
     */
    protected function addKeysRepeaterMetaData()
    {
        //Array of properties to loop through
        $propertyArray = array(
            $this->openDays,
            $this->hours,
            $this->symbol,
            $this->dayClose
        );
        foreach($propertyArray as $prop){
            //Adds keys to metaDataSet Property dynamically
            foreach($prop as $field)
            {
                $this->metaDataSet[$field] = $field;
            }
        }
    }
}