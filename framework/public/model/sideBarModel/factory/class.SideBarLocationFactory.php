<?php

class SideBarLocationFactory implements factory
{
    /*
    * Method to create data Object
    */
    public function createModel()
    {
        $postObj = new LocationPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new LocationMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $LocationModel = new SideBarLocationModel($postData, $metaData);

        return $LocationModel;
    }
}