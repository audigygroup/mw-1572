<?php

class SideBarFactory implements factory
{
    public function createModel()
    {
        $location = SideBarLocationModelFilter::getInstance();
        $practice = SideBarPracticeModelFilter::getInstance();
        $blog = BlogLandingModelFilter::getInstance();

        $model = new SideBarModel($location, $practice, $blog);

        return $model;
    }
}