<?php

class SideBarModel
{
    public $location;
    public $practice;
    public $blog;

    public function __construct(SideBarLocationModelFilter $location, SideBarPracticeModelFilter $practice, BlogLandingModelFilter $blog)
    {
        $this->location = $location->getFilteredModel();
        $this->practice = $practice->getFilteredData();
        $this->blog = $blog->getFilteredModel();
    }
}
