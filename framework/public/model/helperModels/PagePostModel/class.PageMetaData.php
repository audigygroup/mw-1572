<?php

class PageMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $postId = new PagePostData();
        $this->postObject = $postId->getPosts();
    }
}