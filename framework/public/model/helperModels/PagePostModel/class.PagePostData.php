<?php

class PagePostData extends GetPosts
{
    public function __construct()
    {
        global $post;

        $this->args = array(
            'post_type' => 'page',
        );

        # filter to a single Hearing Aid Type
        if ($post->post_type == 'page') {
            $this->args['name'] = $post->post_name;
        }
    }
}