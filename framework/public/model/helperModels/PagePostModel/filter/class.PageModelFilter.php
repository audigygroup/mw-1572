<?php

class PageModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $pageDataSet = new PageFactory();
        $this->dataSet = $pageDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
        );
        $this->metaDataSet = array(
            'faq',
            'quick_nav_term_id',
            'e_patient_videos',
            'component_page_id'
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}