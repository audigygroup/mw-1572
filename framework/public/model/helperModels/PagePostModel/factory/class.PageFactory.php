<?php

class PageFactory implements factory
{
    public function createModel()
    {
        $postObj = new PagePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PageMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new PageModel($postData, $metaData);

        return $model;
    }
}