<?php

class TestimonialModelFilter extends FilterBase{

    private static $instance;

    private function __construct()
    {

        $testimonialDataSet = new TestimonialFactory();
        $this->dataSet =  $testimonialDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'guid',
            'post_type',
            'post_status',
            'thumbnail_url'
        );
        $this->metaDataSet = array(
            'patient_name',
            'patient_since',
            'testimonial_quote',
            'testimonial_image'
        );

    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }

}