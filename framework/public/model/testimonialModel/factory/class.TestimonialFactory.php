<?php
/*
 * @TODO Refactor
 */
class TestimonialFactory implements factory
{

    public function createModel()
    {
        $postObj = new TestimonialPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new TestimonialPostMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $testimonialModel = new TestimonialModel($postData, $metaData);


        return $testimonialModel;
    }

}