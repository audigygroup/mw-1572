<?php

class OurPracticeModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $pageDataSet = new OurPracticeFactory();
        $this->dataSet = $pageDataSet->createModel();
        $this->postDataSet = array(
            'post_title',
        );
        $this->metaDataSet = array(
            'mission_statement',
            'about_us',
            'youtube_video_id',
            'wistia_video_id',
            'faq',
            'quick_nav_term_id',
            'e_patient_videos',
            'component_page_id'
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}