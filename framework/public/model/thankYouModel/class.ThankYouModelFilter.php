<?php

class ThankYouModelFilter extends WpOptionDataFilterBase
{

    private static $instance;

    private function __construct()
    {
        $this->model =  PracticePostData::getInstance();
        $this->requiredData = array(
            'options_facebook_id',
            'options_twitter_id',
            'options_youtube_id',
            'options_google_plus_id',
            'options_linkedin_id',
            'options_thank_you_message',
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}