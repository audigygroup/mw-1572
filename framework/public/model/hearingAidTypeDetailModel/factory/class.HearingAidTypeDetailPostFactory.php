<?php

class HearingAidTypeDetailPostFactory implements factory
{
    public function createModel()
    {
        $postObj = new HearingAidTypePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new HearingAidTypeMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $hearingAidTypeModel = new HearingAidTypeDetailModel($postData, $metaData);

        return $hearingAidTypeModel;
    }
}