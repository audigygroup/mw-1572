<?php

class HearingAidTypeDetailModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $hearingAidTypeDataSet = new HearingAidTypeDetailPostFactory();
        $this->dataSet = $hearingAidTypeDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
        );
        $this->metaDataSet = array(
            'hearing_aid_type',
            'description_excerpt',
            'image_left',
            'image_center',
            'image_right',
            'sub_headline',
            'hearing_aid_description',
            'hearing_aid_drawing',
            'features_icons',
            'feature_descriptions',
            'faq',
            'e_patient_videos',
            'quick_nav_term_id',
            'component_page_id'
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}