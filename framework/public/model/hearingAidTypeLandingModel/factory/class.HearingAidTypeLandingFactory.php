<?php

class HearingAidTypeLandingFactory implements factory
{
    /*
    * This Factory Merges filtered Data
    */
    public function createModel()
    {
        $objOne = HearingAidTypeLandingModelFilter::getInstance();
        $objTwo  = HearingAidTypeLandingPageModelFilter::getInstance();

        $model = new HearingAidTypeLandingModel( $objOne, $objTwo );

        return $model;
    }
}