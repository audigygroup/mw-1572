<?php

class HearingAidTypeLandingModel
{
    //name of keys
    public $hearingAidType;
    public $hearingAidTypePage;

    public function __construct( HearingAidTypeLandingModelFilter $hearingAidType,  HearingAidTypeLandingPageModelFilter $hearingAidTypePage)
    {
        $this->hearingAidType = $hearingAidType->getFilteredModel();
        $this->hearingAidTypePage = $hearingAidTypePage->getFilteredModel();
    }
}