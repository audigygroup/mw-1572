<?php

class HearingAidTypeLandingPageModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $obj = new HearingAidTypeLandingPageFactory();
        $this->dataSet = $obj->createModel();
        $this->postDataSet = array();
        $this->metaDataSet = array(
            'hearing_aid_type_title',
            'hearing_aid_type_sub_headline'
        );
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}