<?php

class EventsAndSeminarsLandingFactory implements factory
{
    public function createModel()
    {
        $postObj = new EventsAndSeminarsPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new EventsAndSeminarsMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new EventsAndSeminarsLandingModel($postData, $metaData);

        return $model;
    }
}