<?php

class EventsAndSeminarsLandingModelFilter extends FilterBase
{

    private static $instance;


    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
    {
        $model =  new EventsAndSeminarsLandingFactory();
        $this->dataSet =  $model->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_excerpt',
            'thumbnail_url',
            'permalink'
        );
        $this->metaDataSet = array(
            'event_date',
            'event_time',
            'location_name',
            'location_address',
            'location_latitude',
            'location_longitude'
        );
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }

}