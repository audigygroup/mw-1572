<?php

class TestimonialsModel
{
    public $testimonials;
    public $testimonialsPage;

    public function __construct(TestimonialPostModelFilter $testimonials, TestimonialPageModelFilter $testimonialsPage)
    {
        $this->testimonials = $testimonials->getFilteredModel();
        $this->testimonialsPage = $testimonialsPage->getFilteredModel();
    }
}