<?php

class TestimonialPageFactory implements factory
{
    public function createModel()
    {
        $postObj = new PagePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PageMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new TestimonialPageModel($postData, $metaData);

        return $model;
    }
}