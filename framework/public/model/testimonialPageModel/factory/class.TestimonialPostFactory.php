<?php

class TestimonialPostFactory implements factory
{
    public function createModel()
    {
        $postObj = new TestimonialPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new TestimonialPostMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $testimonialModel = new TestimonialPostModel($postData, $metaData);

        return $testimonialModel;
    }
}