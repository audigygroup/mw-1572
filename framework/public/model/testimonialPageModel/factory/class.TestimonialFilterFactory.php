<?php

class TestimonialFilterFactory implements factory
{
    public function createModel()
    {
        $postsData = TestimonialPostModelFilter::getInstance();
        $pageData = TestimonialPageModelFilter::getInstance();

        $model = new TestimonialsModel($postsData, $pageData);

        return $model;
    }
}