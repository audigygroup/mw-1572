<?php

class TestimonialPostModelFilter extends FilterBase
{

    private static $instance;

    private function __construct()
    {

        $testimonialDataSet = new TestimonialPostFactory();
        $this->dataSet =  $testimonialDataSet->createModel();
        $this->postDataSet = array(
            'thumbnail_url'
        );
        $this->metaDataSet = array(
            'patient_name',
            'patient_since',
            'location_name',
            'testimonial_quote',
            'testimonial_image'
        );

    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }

}