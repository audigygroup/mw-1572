<?php

class TestimonialPageModelFilter extends FilterBase
{

    private static $instance;

    private function __construct()
    {

        $obj = new TestimonialPageFactory();
        $this->dataSet =  $obj->createModel();
        $this->postDataSet = array();
        $this->metaDataSet = array(
            'testimonials_headline',
            'testimonials_sub_headline',
        );

    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }

}