<?php

class LocationFactory implements factory
{
    /*
     * Method to create data Object
     */
    public function createModel()
    {
        $postObj = new LocationPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new LocationMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $locationModel = new LocationModel($postData, $metaData);

        return $locationModel;
    }
}