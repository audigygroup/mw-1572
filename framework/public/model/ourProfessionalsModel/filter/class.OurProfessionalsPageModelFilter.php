<?php

class OurProfessionalsPageModelFilter extends FilterBase
{

    private static $instance;


    private function __construct()
    {

        $dataSet = new OurProfessionalsPageFactory();
        $this->dataSet = $dataSet->createModel();
        $this->postDataSet = array(
            'post_title'
        );
        $this->metaDataSet = array(
            'our_professionals_headline',
            'our_professionals_sub_headline'
        );

    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}