<?php

class ProfessionalsModelFilter extends FilterBase
{

    private static $instance;


    private function __construct()
    {

        $dataSet = new ProfessionalsFactory();
        $this->dataSet = $dataSet->createModel();
        $this->postDataSet = array(
            'post_title',
            'thumbnail_url',
            'permalink'
        );
        $this->metaDataSet = array(
            'full_name',
            'first_name',
            'last_name',
            'title',
            'bio_excerpt',
            'position',
            'provider_image'
        );

    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}