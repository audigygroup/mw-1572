<?php

class OurProfessionalsPageFactory implements factory
{
    public function createModel()
    {
        $postObj = new PagePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PageMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new OurProfessionalsPageModel( $postData, $metaData );

        return $model;
    }
}