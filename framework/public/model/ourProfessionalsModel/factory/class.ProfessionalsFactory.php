<?php

class ProfessionalsFactory implements factory
{

    public function createModel()
    {
        $postObj = new ProfessionalPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new ProfessionalMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new ProfessionalsModel($postData, $metaData);

        return $model;
    }

}