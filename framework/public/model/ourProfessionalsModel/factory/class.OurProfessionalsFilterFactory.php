<?php

class OurProfessionalsFilterFactory implements factory
{
    public function createModel()
    {
        $pro = ProfessionalsModelFilter::getInstance();
        $pageData = OurProfessionalsPageModelFilter::getInstance();

        $model = new OurProfessionalsModel($pro, $pageData);

        return $model;
    }
}