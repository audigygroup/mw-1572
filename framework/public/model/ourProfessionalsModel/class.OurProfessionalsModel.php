<?php

class OurProfessionalsModel
{
//name of keys
    public $professionals;
    public $ourProfessionalsPage;

    public function __construct(ProfessionalsModelFilter $professionals, OurProfessionalsPageModelFilter $ourProfessionalsPage)
    {
        $this->professionals = $professionals->getFilteredModel();
        $this->ourProfessionalsPage = $ourProfessionalsPage->getFilteredModel();
    }

}