<?php

class CallToActionModelFilter extends WpOptionDataFilterBase
{

    private static $instance;

    private function __construct()
    {
        $this->model =  PracticePostData::getInstance();
        $this->requiredData = array(
            'options_call_to_action_title',
            'options_call_to_action_subheadline'
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}