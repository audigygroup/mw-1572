<?php

class EventsAndSeminarsEventFactory implements factory
{
    public function createModel()
    {
        $postObj = new EventsAndSeminarsPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new EventsAndSeminarsMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new EventsAndSeminarsEventModel($postData, $metaData);

        return $model;
    }
}