<?php

class EventsAndSeminarsPresenterFactory implements factory
{

    //post Ids for presenters
    protected $postIds;

    public function createModel()
    {
        $this->setPostIdIncludeArg();

        $postObj = new ProfessionalPostData($this->postIds);
        $postData = $postObj->getPosts();

        $postMetaObj = new ProfessionalMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new EventsAndSeminarsPresenterModel($postData, $metaData);

        return $model;
    }


    /*
     * Method to set property to post ids of presenters
     */
    protected function setPostIdIncludeArg()
    {
        $postIds = EventsAndSeminarsEventModelFilter::getInstance();
        $postInclude = $postIds->getFilteredModel();

        $post = unserialize($postInclude[0]['presenters']);

        $this->postIds = $post;
    }
}