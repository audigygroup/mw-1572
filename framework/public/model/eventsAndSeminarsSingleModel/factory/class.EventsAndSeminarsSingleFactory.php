<?php

class EventsAndSeminarsSingleFactory implements factory
{
    public function createModel()
    {
        $event = EventsAndSeminarsEventModelFilter::getInstance();
        $presenter = EventsAndSeminarsPresenterModelFilter::getInstance();

        $model = new EventsAndSeminarsSingleModel($event, $presenter);

        return $model;
    }
}