<?php

class EventsAndSeminarsPresenterModelFilter extends FilterBase
{
    private static $instance;



    private function __construct()
    {
        $model = new EventsAndSeminarsPresenterFactory();
        $this->dataSet = $model->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'thumbnail_url',
            'permalink'
        );
        $this->metaDataSet = array(
            'full_name',
            'title',
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}