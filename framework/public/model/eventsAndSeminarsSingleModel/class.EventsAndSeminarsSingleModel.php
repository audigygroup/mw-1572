<?php

class EventsAndSeminarsSingleModel{

    public $event;
    public $presenter;

    public function __construct(EventsAndSeminarsEventModelFilter $event, EventsAndSeminarsPresenterModelFilter  $presenter)
    {
        $this->event = $event->getFilteredModel();
        $this->presenter = $presenter->getFilteredModel();
    }
}