<?php

class HearingAidAccessoriesCatMetaData extends GetCatListMeta
{
    public function __construct()
    {
        $accessoriesId = new HearingAidAccessoriesCatData();
        $this->taxObj = $accessoriesId->getCatList();
        $this->taxonomy = 'accessories_cat';
    }
}