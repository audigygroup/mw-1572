<?php

class HearingAidAccessoriesModel
{
    //name of keys
    public $hearingAidAccessoriesCat;
    public $hearingAidAccessoriesPage;

    public function __construct(HearingAidAccessoriesCatModelFilter $hearingAidAccessoriesCat, HearingAidAccessoriesPageModelFilter $hearingAidAccessoriesPage)
    {
        $this->hearingAidAccessoriesCat = $hearingAidAccessoriesCat->getFilteredModel();
        $this->hearingAidAccessoriesPage = $hearingAidAccessoriesPage->getFilteredModel();
    }
}