<?php

class HearingAidAccessoriesCatModel
{
    public $tax;
    public $taxImg;

    public function __construct(HearingAidAccessoriesCatData $tax, HearingAidAccessoriesCatMetaData $taxImg)
    {
        $this->tax = $tax->getCatList();
        $this->taxImg = $taxImg->getCatMeta();
    }
}