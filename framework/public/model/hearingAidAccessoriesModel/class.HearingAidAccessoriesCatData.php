<?php

class HearingAidAccessoriesCatData extends GetCatList
{
    public function __construct()
    {
        $this->tax = 'accessories_cat';
        $this->args = array(
            'orderby'    => 'name',
            'order'      => 'ASC',
            'hide_empty' => 0
        );
    }
}