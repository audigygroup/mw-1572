<?php

class HearingAidAccessoriesFilterFactory implements factory
{
    /*
    * This Factory Merges filtered Data
    */
    public function createModel()
    {
        $cat = HearingAidAccessoriesCatModelFilter::getInstance();
        $page  = HearingAidAccessoriesPageModelFilter::getInstance();

        $model = new HearingAidAccessoriesModel( $cat, $page );

        return (array) $model;
    }
}