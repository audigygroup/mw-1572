<?php

class HearingAidAccessoriesPageFactory implements factory
{
    public function createModel()
    {
        $postObj = new PagePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PageMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new HearingAidAccessoriesPageModel($postData, $metaData);

        return $model;
    }
}