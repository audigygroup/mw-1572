<?php

class HearingAidAccessoriesCatFactory implements factory
{
    public function createModel()
    {
        $tax = new HearingAidAccessoriesCatData();
        $taxImg = new HearingAidAccessoriesCatMetaData();

        $hearingAidAccessoriesCatModel = new HearingAidAccessoriesCatModel($tax, $taxImg);

        return $hearingAidAccessoriesCatModel;
    }
}