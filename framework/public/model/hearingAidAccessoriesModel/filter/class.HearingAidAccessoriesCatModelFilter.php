<?php

class HearingAidAccessoriesCatModelFilter extends TaxonomyFilterBase
{
    private static $instance;

    private function __construct()
    {
        $hearingAidAccessories = new HearingAidAccessoriesCatFactory();
        $this->dataSet = $hearingAidAccessories->createModel();
        $this->filteredFields = array(
            'name',
            'description',
            'slug',
            'taxonomy_metadata',
            'term_id',
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}