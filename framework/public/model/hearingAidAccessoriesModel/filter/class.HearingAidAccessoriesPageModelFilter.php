<?php

class HearingAidAccessoriesPageModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $obj = new HearingAidAccessoriesPageFactory();
        $this->dataSet = $obj->createModel();
        $this->postDataSet = array();
        $this->metaDataSet = array(
            'hearing_aid_accessories_heading',
            'hearing_aid_accessories_sub_heading'
        );
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}