<?php


class WelcomePracticeDataFilter extends WpOptionDataFilterBase
{

    private static $instance;

    private function __construct()
    {
        //Helper Class PracticePostData
        $this->model =  PracticePostData::getInstance();
        $this->requiredData = array(
            'options_welcome_subheadline',
            'options_welcome_statement'
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}