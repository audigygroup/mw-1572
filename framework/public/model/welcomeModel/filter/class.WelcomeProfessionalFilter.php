<?php

class WelcomeProfessionalFilter extends FilterBase
{

    private static $instance;

    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
    {
        $dataSet = new WelcomeProfessionalFactory();
        $this->dataSet = $dataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_name',
            'post_type',
            'post_status',
            'thumbnail_url'
        );

        $this->metaDataSet = array(
            'full_name',
            'first_name',
            'last_name',
            'title',
            'position'
        );
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}