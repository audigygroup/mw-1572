<?php

class WelcomeProfessionalFactory implements factory
{
    public function createModel()
    {
        $postObj = new ProfessionalPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new ProfessionalMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new WelcomeProfessionalModel($postData, $metaData);

        return $model;
    }
}