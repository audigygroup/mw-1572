<?php

class WelcomeFactory implements factory
{
    /*
    * This Factory Merges filtered Data
    */
    public function createModel()
    {
        $getterOne = WelcomePracticeDataFilter::getInstance();
        $getterTwo  = WelcomeProfessionalFilter::getInstance();

        $model = new WelcomeModel( $getterOne, $getterTwo );

        return $model;
    }
}