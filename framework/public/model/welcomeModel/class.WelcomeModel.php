<?php

class WelcomeModel
{
    //name of keys
    public $practice;
    public $professional;

    public function __construct(WelcomePracticeDataFilter $practice, WelcomeProfessionalFilter $professional)
    {
        $this->practice = $practice->getFilteredData();
        $this->professional = $professional->getFilteredModel();
    }
}