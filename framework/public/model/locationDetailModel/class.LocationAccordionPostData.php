<?php

class LocationAccordionPostData extends GetPosts
{
    protected $getDetailObj;

    public function __construct()
    {
        $this->getDetailObj = LocationDetailModelFilter::getInstance();

        $this->args = array(
            'post_type' => 'accordion',
            'tax_query' => array(
                array(
                    'taxonomy' => 'accordion_cat', //Taxonomy name here
                    'terms' => $this->getCatId() //set ID here
                )
            )
        );
    }

    /*
     * This Method Gets an instance of Detail Model Filter Class so we can use the Term Id for related posts.
     */
    protected function getCatId()
    {
        $locationObj = $this->getDetailObj;
        $loc = $locationObj->getFilteredModel();

        return $loc[0]['location_specialties'];
    }
}