<?php

class LocationAccordionMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $accordionId = new LocationAccordionPostData();
        $this->postObject = $accordionId->getPosts();
    }
}