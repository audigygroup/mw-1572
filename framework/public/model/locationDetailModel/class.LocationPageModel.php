<?php

class LocationPageModel
{
    public $locationPost;
    public $locationProfessionalPost;
    public $locationAccordionPost;

    public function __construct(LocationDetailModelFilter $locationPost, LocationProfessionalModelFilter $locationProfessionalPost, LocationAccordionModelFilter $locationAccordionPost)
    {
        $this->locationPost = $locationPost->getFilteredModel();
        $this->locationProfessionalPost = $locationProfessionalPost->getFilteredModel();
        $this->locationAccordionPost = $locationAccordionPost->getFilteredModel();
    }
}