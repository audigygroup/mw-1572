<?php
/*
 * @TODO Refactor getRepeaterRows()
 */
class LocationDetailModelFilter extends FilterBase
{
    private static $instance;

    protected $openDays = array();
    protected $hours = array();
    protected $symbol = array();
    protected $dayClose = array();


    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
    {
        $locationDataSet =  new LocationDetailFactory();
        $this->dataSet =  $locationDataSet->createModel();

        $this->getRepeaterRows();

        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status'
        );

        $this->metaDataSet = array(
            'location_title',
            'full_address',
            'address',
            'city',
            'state',
            'zip_code',
            'phone_number',
            'email',
            'latitude',
            'longitude',
            'days_of_operation',
            'hours_of_operation',
            'review_us_facebook',
            'review_us_google_plus',
            'review_us_yelp',
            'about',
            'location_specialties',
            'award_1',
            'award_2',
            'award_3',
            'award_4',
            'component_page_id'
        );

        $this->addKeysRepeaterMetaData();
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /*
     * Builds Key used by ACF Pro repeater to add to filter
     */
    protected function getRepeaterRows()
    {
        $rows = get_field('operation');
        if(empty($rows)){
            return;
        }

        foreach($rows as $key => $r){
            $day = 'operation_'. $key .'_day_open';
            $symbol = 'operation_'. $key .'_symbol';
            $dayClose = 'operation_'. $key .'_day_close';
            $hours = 'operation_'. $key .'_time';
            $this->openDays[] = $day;
            $this->symbol[] = $symbol;
            $this->dayClose[] = $dayClose;
            $this->hours[] = $hours;
        }
    }

    protected function addKeysRepeaterMetaData()
    {
        //Array of properties to loop through
        $propertyArray = array(
            $this->openDays,
            $this->hours,
            $this->symbol,
            $this->dayClose
        );
        foreach($propertyArray as $prop){
            //Adds keys to metaDataSet Property dynamically
            foreach($prop as $field)
            {
                $this->metaDataSet[$field] = $field;
            }
        }
    }
}