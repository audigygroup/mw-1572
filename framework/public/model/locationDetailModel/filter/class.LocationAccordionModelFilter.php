<?php

class LocationAccordionModelFilter extends FilterBase
{
    private static $instance;

    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
{
    $locationDataSet =  new LocationAccordionFactory();
    $this->dataSet =  $locationDataSet->createModel();
    $this->postDataSet = array(
        'ID',
        'post_title',
        'post_name',
        'post_type',
        'post_status'
    );

    $this->metaDataSet = array(
        'question',
        'answer'
    );
}
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
{

    if (is_null( self::$instance )) {
        self::$instance = new self();
    }

    return self::$instance;

}
}