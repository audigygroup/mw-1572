<?php

class LocationProfessionalFactory implements factory
{
    /*
    * Method to create data Object
    */
    public function createModel()
    {
        $postObj = new ProfessionalPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new ProfessionalMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $locationModel = new LocationProfessionalModel($postData, $metaData);

        return $locationModel;
    }
}