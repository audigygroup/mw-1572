<?php

class LocationDetailFactory implements factory
{
    /*
     * Method to create data Object
     */
    public function createModel()
    {
        $single = array(
            'single' => ''
        );
        $postObj = new LocationPostData($single);
        $postData = $postObj->getPosts();

        $postMetaObj = new LocationMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $locationModel = new LocationDetailModel($postData, $metaData);

        return $locationModel;
    }
}