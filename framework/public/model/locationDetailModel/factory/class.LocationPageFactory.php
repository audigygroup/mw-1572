<?php

class LocationPageFactory implements factory
{

    /*
    * Method to create LocationPageModel data Object
    */
    public function createModel()
    {
        $locationPost = LocationDetailModelFilter::getInstance();
        $locationProfessionalPost = LocationProfessionalModelFilter::getInstance();
        $locationAccordionPost = LocationAccordionModelFilter::getInstance();

        $locationPageModel = new LocationPageModel($locationPost, $locationProfessionalPost, $locationAccordionPost);

        return $locationPageModel;
    }
}