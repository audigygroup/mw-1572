<?php

class LocationAccordionFactory implements factory
{
    /*
     * Method to create data Object
     */
    public function createModel()
    {
        $postObj = new LocationAccordionPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new LocationAccordionMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $locationAccordionModel = new LocationAccordionModel($postData, $metaData);

        return $locationAccordionModel;
    }
}