<?php

class PatientFormsPageFactory implements factory
{
    public function createModel()
    {
        $postObj = new PagePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PageMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new PatientFormsPageModel($postData, $metaData);

        return $model;
    }
}