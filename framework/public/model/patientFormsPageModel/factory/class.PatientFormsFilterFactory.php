<?php

class PatientFormsFilterFactory implements factory
{
    public function createModel()
    {
        $post = PatientFormsFormModelFilter::getInstance();
        $page = PatientFormsPageModelFilter::getInstance();

        $model = new PatientFormsModel($post, $page);

        return $model;
    }
}