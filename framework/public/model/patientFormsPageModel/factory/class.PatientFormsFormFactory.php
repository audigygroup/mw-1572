<?php

class PatientFormsFormFactory implements factory
{
    public function createModel()
    {
        $postObj = new PatientFormsFormPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PatientFormsFormMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new PatientFormsFormModel($postData, $metaData);

        return $model;
    }
}