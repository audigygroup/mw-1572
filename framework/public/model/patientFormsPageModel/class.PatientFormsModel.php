<?php

class PatientFormsModel
{
    public $forms;
    public $patientFormsPage;

    public function __construct(PatientFormsFormModelFilter $forms, PatientFormsPageModelFilter $patientFormsPage)
    {
        $this->forms = $forms->getFilteredModel();
        $this->patientFormsPage = $patientFormsPage->getFilteredModel();
    }
}