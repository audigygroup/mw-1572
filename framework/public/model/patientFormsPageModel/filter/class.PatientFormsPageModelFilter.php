<?php

class PatientFormsPageModelFilter  extends FilterBase
{
    private static $instance;


    private function __construct()
    {
        $dataSet =  new PatientFormsPageFactory();
        $this->dataSet =  $dataSet->createModel();
        $this->postDataSet = array();

        $this->metaDataSet = array(
            'patient_forms_headline',
            'patient_forms_description'
        );
    }


    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}