<?php

class PatientFormsFormModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $dataSet =  new PatientFormsFormFactory();
        $this->dataSet =  $dataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status'
        );

        $this->metaDataSet = array(
            'form_title',
            'form_file'
        );
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }
}