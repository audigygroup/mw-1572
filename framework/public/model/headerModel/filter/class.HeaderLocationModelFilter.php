<?php

class HeaderLocationModelFilter extends FilterBase
{
    private static $instance;


    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
    {
        $locationDataSet =  new HeaderLocationFactory();
        $this->dataSet =  $locationDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status'
        );

        $this->metaDataSet = array(
            'location_title',
            'phone_number',
        );
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }
}