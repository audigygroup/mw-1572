<?php

class HeaderPracticeModelFilter extends WpOptionDataFilterBase
{

    private static $instance;

    private function __construct()
    {
        //Helper Class PracticePostData
        $this->model =  PracticePostData::getInstance();
        $this->requiredData = array(
            'options_header_logo',
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}