<?php

class HeaderModel
{
    //name of keys
    public $headerLocation;
    public $headerPractice;

    public function __construct(HeaderLocationModelFilter $headerLocation, HeaderPracticeModelFilter $headerPractice)
    {
        $this->headerLocation = $headerLocation->getFilteredModel();
        $this->headerPractice = $headerPractice->getFilteredData();
    }
}