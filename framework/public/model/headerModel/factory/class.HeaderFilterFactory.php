<?php

class HeaderFilterFactory implements factory
{
    public function createModel()
    {
        $location = HeaderLocationModelFilter::getInstance();
        $practice = HeaderPracticeModelFilter::getInstance();

        $headerLocationModel = new HeaderModel($location, $practice);

        return (array) $headerLocationModel;
    }
}