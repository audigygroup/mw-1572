<?php

class HeaderLocationFactory implements factory
{
    /*
      * Method to create data Object
      */
    public function createModel()
    {
        $postObj = new LocationPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new LocationMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $headerLocationModel = new HeaderLocationModel($postData, $metaData);

        return $headerLocationModel;
    }
}