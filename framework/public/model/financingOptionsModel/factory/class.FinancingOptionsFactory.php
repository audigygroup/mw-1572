<?php

class FinancingOptionsFactory implements factory
{
    public function createModel()
    {
        $postObj = new PagePostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new PageMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new FinancingOptionsModel($postData, $metaData);

        return $model;
    }
}