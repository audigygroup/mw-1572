<?php

class FinancingOptionsModelFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        $model = new FinancingOptionsFactory();
        $this->dataSet = $model->createModel();
        $this->postDataSet = array();

        $this->metaDataSet = array(
            'financing_options_title',
            'financing_options_sub_headline',
            'financing_options_paragraph',
            'carecredit_section',
            'wells_fargo_section'
        );
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}