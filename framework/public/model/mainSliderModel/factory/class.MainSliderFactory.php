<?php

class MainSliderFactory implements factory
{
    public function createModel()
    {
        $postObj = new MainSliderPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new MainSliderMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new MainSliderModel($postData, $metaData);

        return $model;
    }
}