<?php

class MainSliderModelFilter extends FilterBase
{

    private static $instance;

    private function __construct()
    {
        $mainSliderDataSet =  new MainSliderFactory();
        $this->dataSet =  $mainSliderDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'guid',
            'post_type',
            'post_status',
            'thumbnail_url'
        );

        $this->metaDataSet = array(
            'slide_caption',
            'alt',
            'title',
            'sub_headline',
            'request_appointment_button',
	        'slider_button_text',
	        'slider_button_url'
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}