<?php

class ComponentRenderPostData extends GetPosts
{
    private static $instance;
    protected $getDetailObj;

    public function __construct($getDetailObj)
    {
        $this->getDetailObj = $getDetailObj;

        $this->args = array(
            'post_type' => 'component',
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'comp_cat', //Taxonomy name here
                    'terms' => $this->getCatId() //set ID here
                )
            )
        );
    }

    /*
     * This Method Gets an instance of Detail Model Filter Class so we can use the Term Id for related posts.
     */
    protected function getCatId()
    {
        $obj = $this->getDetailObj;
        $page = $obj->getFilteredModel();

        if(is_tax()){
            return $page[0]['taxonomy_metadata']['component_page_id'];
        }else{
            return $page[0]['component_page_id'];
        }
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance($getDetailObj)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($getDetailObj);
        }
        return self::$instance;
    }
}