<?php

class ComponentRenderFilter extends FilterBase
{
    private static $instance;

    private function __construct()
    {
        global $post;
        // Pass in filter to be used with dependency injection
        if($post->post_type == 'page') {
            $contentObj = PageModelFilter::getInstance();
        }elseif($post->post_type == 'professional') {
            $contentObj = OurProfessionalDetailModelFilter::getInstance();
        }elseif($post->post_type == 'hearing-aid-type'){
            $contentObj = HearingAidTypeDetailModelFilter::getInstance();
        }elseif($post->post_type == 'lifestyle'){
            $contentObj = LifestyleDetailModelFilter::getInstance();
        }elseif($post->post_type == 'location'){
            $contentObj = LocationDetailModelFilter::getInstance();
        }elseif(is_tax('accessories_cat')){
            $contentObj = HearingAidAccessoriesCatDetailModelFilter::getInstance();
        }

        $dataObj = new ComponentRenderFactory();
        $this->dataSet = $dataObj->createModel($contentObj);
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
        );
        $this->metaDataSet = array(
            'component'
        );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}