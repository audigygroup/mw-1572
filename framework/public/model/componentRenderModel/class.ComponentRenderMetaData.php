<?php

class ComponentRenderMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $contentObj = PageModelFilter::getInstance();
        $postId = ComponentRenderPostData::getInstance($contentObj);
        $this->postObject = $postId->getPosts();
    }
}