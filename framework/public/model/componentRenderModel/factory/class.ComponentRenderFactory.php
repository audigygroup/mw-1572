<?php

class ComponentRenderFactory
{
    private static $instance;
    protected $contentObj;

    public function createModel($contentObj)
    {
        $this->contentObj = $contentObj;

        $postObj = ComponentRenderPostData::getInstance($contentObj);
        $postData = $postObj->getPosts();

        $postMetaObj = new ComponentRenderMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new ComponentRenderModel($postData, $metaData);

        return $model;
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}