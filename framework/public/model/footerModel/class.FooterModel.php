<?php

class FooterModel
{
    private static $instance;

    protected $footerPracticeData;
    protected $footerLocationData;

    private function __construct()
    {
        $this->footerPracticeData = FooterPracticeDataModelFilter::getInstance();
        $this->footerLocationData = FooterLocationDataModelFilter::getInstance();
    }

    public static function getInstance()
    {

        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;

    }


    /*
    * Gets Filtered Practice Data
    */
    protected function getPracticeData()
    {
        $filteredPracticeData = $this->footerPracticeData->getFilteredData();
        return $filteredPracticeData;
    }

    /*
     * gets Filtered Location Data
     */
    protected function getLocationData()
    {
        $locationData = $this->footerLocationData->getFilteredModel();
        return $locationData;
    }

    /*
     * Merges Filtered Data Together into one Array
     */
    protected function mergePracticeAndLocation()
    {
        $practiceData = $this->getPracticeData();
        $locationData = $this->getLocationData();

        $tempLoc = array(
            'location' => $locationData
        );

        $mergedArray = array_merge(  $practiceData, $tempLoc);

        return $mergedArray;

    }

    /*
     * Getter
     */
    public function getFooterModel()
    {
        return $this->mergePracticeAndLocation();
    }

}