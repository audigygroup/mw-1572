<?php

class FooterPracticeDataModelFilter extends WpOptionDataFilterBase
{

    private static $instance;

    private function __construct()
    {
        $this->model =  PracticePostData::getInstance();
        $this->requiredData = array(
            'options_footer_logo',
            'options_powered_by',
            'options_powered_by_link',
            'options_facebook_id',
            'options_twitter_id',
            'options_youtube_id',
            'options_google_plus_id',
            'options_linkedin_id',
            'options_audigy_certified_desc'
        );
    }

    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}