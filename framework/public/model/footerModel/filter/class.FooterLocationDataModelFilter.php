<?php

class FooterLocationDataModelFilter extends FilterBase
{
    private static $instance;


    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
    {
        //Uses Header Location Factory to populate location data
        $locationDataSet =  new HeaderLocationFactory();
        $this->dataSet =  $locationDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status'
        );

        $this->metaDataSet = array(
            'location_title',
            'phone_number',
            'email',
            'days_of_operation',
            'hours_of_operation',
        );
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}