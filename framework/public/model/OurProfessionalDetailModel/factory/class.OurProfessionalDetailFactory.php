<?php

class OurProfessionalDetailFactory implements factory
{
    public function createModel()
    {
        $postObj  = new ProfessionalPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new ProfessionalMetaData();
        $metaData    = $postMetaObj->getPostMeta();

        $providerModel = new OurProfessionalDetailModel( $postData, $metaData );

        return $providerModel;
    }
}