<?php

class OurProfessionalDetailModelFilter extends FilterBase
{
    private static $instance;

    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
    {
        $providerDataSet =  new OurProfessionalDetailFactory();
        $this->dataSet =  $providerDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'guid',
            'post_type',
            'post_status',
            'thumbnail_url'
        );

        $this->metaDataSet = array(
            'full_name',
            'first_name',
            'last_name',
            'title',
            'request_appointment_button',
            'bio',
            'email',
            'facebook_id',
            'linkedin_id',
            'twitter_id',
            'phone_number',
            'awards_and_certifications',
            'quick_nav_term_id',
            'component_page_id'
        );
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        if (is_null( self::$instance )) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}