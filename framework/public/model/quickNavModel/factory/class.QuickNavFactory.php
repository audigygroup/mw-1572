<?php

class QuickNavFactory implements factory
{
    public function createModel()
    {
        $postObj  = new QuickNavPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new QuickNavMetaData();
        $metaData    = $postMetaObj->getPostMeta();

        $testimonialModel = new QuickNavModel( $postData, $metaData );

        return $testimonialModel;
    }
}