<?php

class QuickNavModelFilter extends FilterBase{

    private static $instance;


    private function __construct()
    {

        $quickNavDataSet = new QuickNavFactory();
        $this->dataSet =  $quickNavDataSet->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
        );
        $this->metaDataSet = array(
            'icon_class',
            'title',
            'quick_nav_excerpt',
            'quick_nav_link',
        );

    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }


}