<?php

class QuickNavMetaData extends GetPostsMeta
{

    public function __construct()
    {
        $quickNavId = new QuickNavPostData();
        $this->postObject = $quickNavId->getPosts();
    }

}