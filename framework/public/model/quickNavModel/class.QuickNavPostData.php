<?php
/*
 * @TODO Refactor this class
 */
class QuickNavPostData extends GetPosts
{

    protected $getPageObj;
    protected $getHearingAidSingleObj;
    protected $getProfessionalDetailObj;
    protected $getAccessoriesCatDetailObj;


    public function __construct()
    {
        $this->getPageObj = PageModelFilter::getInstance();
        $this->getHearingAidSingleObj = HearingAidTypeDetailModelFilter::getInstance();
        $this->getProfessionalDetailObj = OurProfessionalDetailModelFilter::getInstance();
        $this->getAccessoriesCatDetailObj = HearingAidAccessoriesCatDetailModelFilter::getInstance();

        $this->args = array(
            'post_type' => 'quick-nav',
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'quick_nav_cat', //Taxonomy name here
                    'terms' => $this->getCatId() //set ID here
                )
            )
        );
    }

    /*
     * This Method Gets an instance of Model Filter Class so we can use the Term Id for related posts.
     */
    protected function getCatId()
    {
        //set post global so we can check for posts
        global $post;

        //check for post type to get the category
       if($post->post_type == 'hearing-aid-type'){
            $obj = $this->getHearingAidSingleObj;
       }elseif($post->post_type == 'professional') {
           $obj = $this->getProfessionalDetailObj;
       }elseif(is_tax('accessories_cat')){
           $obj = $this->getAccessoriesCatDetailObj;
       }else{
           $obj = $this->getPageObj;
       }

        //set getter
        $page = $obj->getFilteredModel();

        //check if a cat page
        if(is_tax('accessories_cat')){
            return $page[0]['taxonomy_metadata']['taxonomy_quick_nav'];
        }else{
            return $page[0]['quick_nav_term_id'];
        }
    }

}