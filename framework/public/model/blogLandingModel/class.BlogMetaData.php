<?php

class BlogMetaData extends GetPostsMeta
{
    public function __construct()
    {
        $post = new BlogPostData();
        $this->postObject = $post->getPosts();
    }
}