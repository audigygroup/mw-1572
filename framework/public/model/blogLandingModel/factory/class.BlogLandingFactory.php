<?php

class BlogLandingFactory implements factory
{
    public function createModel()
    {
        $postObj = new BlogPostData();
        $postData = $postObj->getPosts();

        $postMetaObj = new BlogMetaData();
        $metaData = $postMetaObj->getPostMeta();

        $model = new BlogLandingtModel($postData, $metaData);

        return $model;
    }
}