<?php

class BlogPostData extends GetPosts
{
    public function __construct()
    {

        $this->args = array(
            'post_type'       => 'post',
            'orderby'         => 'post_date',
            'order'           => 'DESC',
            'posts_per_page'  => 20,
        );

        //set Taxonomy
        $this->setTerm();

    }

    protected function setTerm()
    {
        if(!is_category() && !is_tag()){
            return;
        }
        //get Term ID
        $queried_object = get_queried_object();
        $term_id = $queried_object->term_id;

        $this->args['tax_query'] = array(
            array(
                'taxonomy' => $queried_object->taxonomy, //Taxonomy name here
                'terms' => $term_id //set ID here
            )
        );
    }

}