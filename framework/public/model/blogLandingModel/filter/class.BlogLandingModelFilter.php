<?php

class BlogLandingModelFilter extends FilterBase
{

    private static $instance;


    /**
     * Set Variables that are in FilterBase Class
     *
     * @since     1.0.0
     *
     */
    private function __construct()
    {
        $model =  new BlogLandingFactory();
        $this->dataSet =  $model->createModel();
        $this->postDataSet = array(
            'ID',
            'post_title',
            'post_name',
            'post_type',
            'post_status',
            'post_content',
            'post_author',
            'post_date',
            'post_excerpt',
            'thumbnail_url',
            'permalink'
        );
        $this->metaDataSet = array();
    }
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        if (is_null( self::$instance )) {
            self::$instance = new self();
        }

        return self::$instance;

    }

}