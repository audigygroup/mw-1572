<?php

class MWPublic
{
	private static $instance;
	public $MWFontCombos;

	private function __construct()
	{
		$this->MWFontCombos = new MWFontCombos();
		add_action('wp_enqueue_scripts', array($this, 'mw_customizer_public') );
	}

	public static function getInstance()
	{
		if ( is_null( self::$instance ) )
		{
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function mw_customizer_public()
	{
        $fontOption = get_theme_mod('mw_combo_font');

        $MWFontOptions = MWFontOptions::getInstance();
        $fontOptions = $MWFontOptions->getLessValues($fontOption);

		if (!empty($fontOptions)) {
            if (!empty($fontOptions['link_urls'])) {
                foreach ($fontOptions['link_urls'] as $url) {
                    wp_enqueue_style(md5($url), $url, '', true);
                }
            }
		}
	}
}
