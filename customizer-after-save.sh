#!/bin/bash

# Local Setup
# In Terminal, type which node, then create a symlink to that location in /usr/bin.
# For example, if node lives in /usr/local/bin, create the symlink like so:
# sudo ln -s /usr/local/bin/node /usr/bin/node
#
#
# web server needs execute access to grunt and node
# and write permissions to dynamic less file

# Set the path to the local grunt install. For OSX it's here:
/usr/local/bin/grunt