<?php
/*
Template Name: Review Us
*/
get_header();?>
    <div class="main-section-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-lg-9">
                    <?php
                        // Review Us Content
                        $reviewUsPage = new ReviewUsPageController();
                        $reviewUsPage->viewOutput();
                    ?>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-3">
                    <?php
                        // SideBar
                        $sideBar = new SideBarController();
                        $sideBar->viewOutput();
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
$comps = new ComponentRenderController();
$comps->getter();

get_footer();
?>