<?php get_header();?>
<section class="main-header-image">
    <div id="<?php echo $post->post_name;?>">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>
</section>
<section id="generic-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-9">
                <div class="page-content">
                    <?php if ( have_posts() ) : ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <?php
                    $faq = new AccordionComponentController();
                    $faq->viewOutput();
                ?>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-3">
                <?php
                // SideBar
                $sideBar = new SideBarController();
                $sideBar->viewOutput();
                ?>
            </div>
        </div>
    </div>
</section>
<?php
$comps = new ComponentRenderController();
$comps->getter();
?>
<?php get_footer();?>