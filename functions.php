<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
// Define directories
define('MW_FRAMEWORK', get_template_directory() . '/framework/');

//Includes Merriweather Framework
include_once( MW_FRAMEWORK . 'merriweather.php' );