<?php
/**
 * Template Name: Search Page
**/
?>
<form action="/" method="get" class="navbar-form" role="search">
	<div class="form-group">
        <div class="form-wrap">
            <input type="text" class="form-control search-input" placeholder="Search" value="<?php echo get_search_query() ?>" name="s">
            <button type="submit" class="search-btn glyphicon glyphicon-search"></button>
        </div>
	</div>
</form>