<?php get_header();?>
<section class="main-section-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-9">
                <?php
                $lifestyle = new LifestyleDetailController();
                $lifestyle->viewOutput();
                ?>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-3">
                <?php
                // SideBar
                $sideBar = new SideBarController();
                $sideBar->viewOutput();
                ?>
            </div>
        </div>
    </div>
</section>
<?php

$comps = new ComponentRenderController();
$comps->getter();

get_footer();?>