<?php
/**
 * @package
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="page-header">
        <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>


        <?php if ( is_search() ) : // Only display Excerpts for Search ?>
            <div class="entry-summary">
                <?php the_excerpt(); ?>
            </div>
        <?php endif; ?>

        <a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
            <?php
                $link = get_permalink('','',FALSE);
                echo substr($link, 0, 40);
                if (strlen($link) > 40) echo " ...";
             ?>
        </a>
    </header>
</article>