<?php
/**
 * Template Name: Icon Style Guide Template
 *
 * This page template is intended to display the icons available for us in Merriweather.
 * To add a new font icon, just add the unique portion of the class name. Leave off the mw- and the -solid or -outline
 */

/**
 *  This version represents only the icons defined in the comps for Dhals build out7
 */
$icons = array(
    'people',
    'forms',
    'tinnitus',
    'calendar',
    'reviews',
    'umbrella',
    'about',
    'accessories',
    'anesthesia',
    'apple',
    'batteries',
    'calendar',
    'crowns',
    'dental-implants',
    'dentistry-tools',
    'finance',
    'floss',
    'invisalign',
    'kite',
    'laptop',
    'lifestyles',
    'lips',
    'location',
    'news',
    'quick-crowns',
    'reviews',
    'sedation',
    'six-month-smile',
    'smile',
    'tinnitus',
    'tooth-health',
    'tooth-shield',
    'tooth-sparkle',
    'toothbrush-toothpaste',
    'types',
    'umbrella',
    'veneers'
);

$hatIcons = array(
    'active',
    'aid-with-ear',
    'battery',
    'bluetooth',
    'breahtable',
    'buttons',
    'buzzing',
    'ear-mold',
    'invisible',
    'levels',
    'lightweight',
    'location',
    'natural-sound',
    'popular',
    'powerful',
    'search',
);

get_header(); ?>
<?php
/*
* load stylesheet is only temporary
*/
?>
    <link rel="stylesheet" href="/wp-content/themes/<?php echo get_template(); ?>/framework/includes/merriweather-icons/css/enterprise.min.css">
    <section id="icon-style-guide">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h1>Regular</h1>

                    <div class="row">
                        <?php foreach ($icons as $icon) : ?>
                            <div class="col-xs-12">
                                <div class="icon-wrapper">
                                    <i class="mw mw-<?php echo $icon; ?>-solid"></i>
                                    <code>&lt;i class="mw mw-<?php echo $icon; ?>-solid"&gt;&lt;/i&gt;</code>
                                </div>
                                <h4>Usage in Quick Nav Admin Panel:</h4>
                                <div class="well well-sm">
                                    <p><?php echo $icon; ?>-solid</p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h1>Outline</h1>

                        <div class="row">
                            <?php foreach ($icons as $icon) : ?>
                                <div class="col-xs-12">
                                    <div class="icon-wrapper">
                                        <i class="mw mw-<?php echo $icon; ?>-outline"></i>
                                        <code>&lt;i class="mw mw-<?php echo $icon; ?>-outline"&gt;&lt;/i&gt;</code>
                                    </div>
                                    <h4>Usage in Quick Nav Admin Panel:</h4>
                                    <div class="well well-sm">
                                        <p><?php echo $icon; ?>-outline</p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <h1>Hearing Aid Type Features Icons</h1>
                    <div class="col-xs-12 col-sm-6">
                        <div class="row">
                            <?php foreach ($hatIcons as $icon) : ?>
                                <div class="col-xs-12">
                                    <div class="icon-wrapper">
                                        <i class="mw mw-features-<?php echo $icon; ?>"></i>
                                        <code>&lt;i class="mw mw-features-<?php echo $icon; ?>"&gt;&lt;/i&gt;</code>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
    </section>
<?php get_footer(); ?>