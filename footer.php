<?php
  $footer = new FooterController();
  $footer->viewOutput();
  $copyright = new CopyrightController();
  $copyright->viewOutput();
?>
<script  src="/wp-content/themes/<?php echo get_template(); ?>/framework/assets/js/jquery-1.11.2.min.js"></script>
<!--[if lt IE 9]>
  <script src="/wp-content/themes/<?php echo get_template(); ?>/framework/assets/js/respond.min.js"></script>
<![endif]-->
<script async src="/wp-content/themes/<?php echo get_template() ?>/framework/assets/js/main.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>